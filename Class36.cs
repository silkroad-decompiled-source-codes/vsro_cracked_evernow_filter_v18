﻿// Decompiled with JetBrains decompiler
// Type: Class36
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.IO;
using System.Reflection;

internal class Class36
{
  public static void smethod_0()
  {
    TextReader textReader = (TextReader) new StreamReader(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Setting(s)\\TeleportByTime.ini");
    string str1;
    while ((str1 = textReader.ReadLine()) != null)
    {
      string str2 = str1;
      if (str2 == "[Service System]")
        Class32.bool_129 = Convert.ToBoolean(textReader.ReadLine().Split('=')[1]);
      else if (str2 == "[Time Setting]")
      {
        Class32.dictionary_9.Clear();
        int int32 = Convert.ToInt32(textReader.ReadLine().Split('=')[1]);
        for (int index = 0; index < int32; ++index)
        {
          uint key = uint.Parse(textReader.ReadLine().Split('=')[1]);
          string str3 = textReader.ReadLine().Split('=')[1];
          string str4 = textReader.ReadLine().Split('=')[1];
          string str5 = textReader.ReadLine().Split('=')[1];
          if (!Class32.dictionary_9.ContainsKey(key))
          {
            Class32.dictionary_9.Add(key, 0);
          }
          else
          {
            Class32.dictionary_9.Remove(key);
            Class32.dictionary_9.Add(key, 0);
          }
        }
      }
    }
    textReader.Close();
  }
}
