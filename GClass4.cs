﻿// Decompiled with JetBrains decompiler
// Type: GClass4
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

public class GClass4
{
  private static uint[] uint_0 = GClass4.smethod_3();
  private static Random random_0 = new Random();
  private uint uint_1;
  private uint uint_2;
  private uint uint_3;
  private uint uint_4;
  private uint uint_5;
  private uint uint_6;
  private uint uint_7;
  private uint uint_8;
  private ulong ulong_0;
  private ulong ulong_1;
  private byte[] byte_0;
  private ulong ulong_2;
  private ulong ulong_3;
  private bool bool_0;
  private byte byte_1;
  private GClass4.Class7 class7_0;
  private bool bool_1;
  private bool bool_2;
  private byte byte_2;
  private string string_0;
  private List<GClass3> list_0;
  private List<GClass3> list_1;
  private List<ushort> list_2;
  private GClass0 gclass0_0;
  private GClass5 gclass5_0;
  private GClass5 gclass5_1;
  private ushort ushort_0;
  private GClass3 gclass3_0;
  private object object_0;

  private static GClass4.Class7 smethod_0(GClass4.Class7 class7_1)
  {
    return new GClass4.Class7()
    {
      byte_0 = class7_1.byte_0,
      byte_1 = class7_1.byte_1,
      byte_2 = class7_1.byte_2,
      byte_3 = class7_1.byte_3,
      byte_4 = class7_1.byte_4,
      byte_5 = class7_1.byte_5,
      byte_6 = class7_1.byte_6,
      byte_7 = class7_1.byte_7
    };
  }

  private static byte smethod_1(GClass4.Class7 class7_1)
  {
    return (byte) ((int) class7_1.byte_0 | (int) class7_1.byte_1 << 1 | (int) class7_1.byte_2 << 2 | (int) class7_1.byte_3 << 3 | (int) class7_1.byte_4 << 4 | (int) class7_1.byte_5 << 5 | (int) class7_1.byte_6 << 6 | (int) class7_1.byte_7 << 7);
  }

  private static GClass4.Class7 smethod_2(byte byte_3)
  {
    GClass4.Class7 class7 = new GClass4.Class7();
    class7.byte_0 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_1 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_2 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_3 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_4 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_5 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_6 = (byte) ((uint) byte_3 & 1U);
    byte_3 >>= 1;
    class7.byte_7 = (byte) ((uint) byte_3 & 1U);
    byte_3 = (byte) 0;
    return class7;
  }

  private static uint[] smethod_3()
  {
    uint[] numArray = new uint[65536];
    using (MemoryStream memoryStream = new MemoryStream(new byte[1024]
    {
      (byte) 177,
      (byte) 214,
      (byte) 139,
      (byte) 150,
      (byte) 150,
      (byte) 48,
      (byte) 7,
      (byte) 119,
      (byte) 44,
      (byte) 97,
      (byte) 14,
      (byte) 238,
      (byte) 186,
      (byte) 81,
      (byte) 9,
      (byte) 153,
      (byte) 25,
      (byte) 196,
      (byte) 109,
      (byte) 7,
      (byte) 143,
      (byte) 244,
      (byte) 106,
      (byte) 112,
      (byte) 53,
      (byte) 165,
      (byte) 99,
      (byte) 233,
      (byte) 163,
      (byte) 149,
      (byte) 100,
      (byte) 158,
      (byte) 50,
      (byte) 136,
      (byte) 219,
      (byte) 14,
      (byte) 164,
      (byte) 184,
      (byte) 220,
      (byte) 121,
      (byte) 30,
      (byte) 233,
      (byte) 213,
      (byte) 224,
      (byte) 136,
      (byte) 217,
      (byte) 210,
      (byte) 151,
      (byte) 43,
      (byte) 76,
      (byte) 182,
      (byte) 9,
      (byte) 189,
      (byte) 124,
      (byte) 177,
      (byte) 126,
      (byte) 7,
      (byte) 45,
      (byte) 184,
      (byte) 231,
      (byte) 145,
      (byte) 29,
      (byte) 191,
      (byte) 144,
      (byte) 100,
      (byte) 16,
      (byte) 183,
      (byte) 29,
      (byte) 242,
      (byte) 32,
      (byte) 176,
      (byte) 106,
      (byte) 72,
      (byte) 113,
      (byte) 177,
      (byte) 243,
      (byte) 222,
      (byte) 65,
      (byte) 190,
      (byte) 140,
      (byte) 125,
      (byte) 212,
      (byte) 218,
      (byte) 26,
      (byte) 235,
      (byte) 228,
      (byte) 221,
      (byte) 109,
      (byte) 81,
      (byte) 181,
      (byte) 212,
      (byte) 244,
      (byte) 199,
      (byte) 133,
      (byte) 211,
      (byte) 131,
      (byte) 86,
      (byte) 152,
      (byte) 108,
      (byte) 19,
      (byte) 192,
      (byte) 168,
      (byte) 107,
      (byte) 100,
      (byte) 122,
      (byte) 249,
      (byte) 98,
      (byte) 253,
      (byte) 236,
      (byte) 201,
      (byte) 101,
      (byte) 138,
      (byte) 79,
      (byte) 92,
      (byte) 1,
      (byte) 20,
      (byte) 217,
      (byte) 108,
      (byte) 6,
      (byte) 99,
      (byte) 99,
      (byte) 61,
      (byte) 15,
      (byte) 250,
      (byte) 245,
      (byte) 13,
      (byte) 8,
      (byte) 141,
      (byte) 200,
      (byte) 32,
      (byte) 110,
      (byte) 59,
      (byte) 94,
      (byte) 16,
      (byte) 105,
      (byte) 76,
      (byte) 228,
      (byte) 65,
      (byte) 96,
      (byte) 213,
      (byte) 114,
      (byte) 113,
      (byte) 103,
      (byte) 162,
      (byte) 209,
      (byte) 228,
      (byte) 3,
      (byte) 60,
      (byte) 71,
      (byte) 212,
      (byte) 4,
      (byte) 75,
      (byte) 253,
      (byte) 133,
      (byte) 13,
      (byte) 210,
      (byte) 107,
      (byte) 181,
      (byte) 10,
      (byte) 165,
      (byte) 250,
      (byte) 168,
      (byte) 181,
      (byte) 53,
      (byte) 108,
      (byte) 152,
      (byte) 178,
      (byte) 66,
      (byte) 214,
      (byte) 201,
      (byte) 187,
      (byte) 219,
      (byte) 64,
      (byte) 249,
      (byte) 188,
      (byte) 172,
      (byte) 227,
      (byte) 108,
      (byte) 216,
      (byte) 50,
      (byte) 117,
      (byte) 92,
      (byte) 223,
      (byte) 69,
      (byte) 207,
      (byte) 13,
      (byte) 214,
      (byte) 220,
      (byte) 89,
      (byte) 61,
      (byte) 209,
      (byte) 171,
      (byte) 172,
      (byte) 48,
      (byte) 217,
      (byte) 38,
      (byte) 58,
      (byte) 0,
      (byte) 222,
      (byte) 81,
      (byte) 128,
      (byte) 81,
      (byte) 215,
      (byte) 200,
      (byte) 22,
      (byte) 97,
      (byte) 208,
      (byte) 191,
      (byte) 181,
      (byte) 244,
      (byte) 180,
      (byte) 33,
      (byte) 35,
      (byte) 196,
      (byte) 179,
      (byte) 86,
      (byte) 153,
      (byte) 149,
      (byte) 186,
      (byte) 207,
      (byte) 15,
      (byte) 165,
      (byte) 183,
      (byte) 184,
      (byte) 158,
      (byte) 184,
      (byte) 2,
      (byte) 40,
      (byte) 8,
      (byte) 136,
      (byte) 5,
      (byte) 95,
      (byte) 178,
      (byte) 217,
      (byte) 236,
      (byte) 198,
      (byte) 36,
      (byte) 233,
      (byte) 11,
      (byte) 177,
      (byte) 135,
      (byte) 124,
      (byte) 111,
      (byte) 47,
      (byte) 17,
      (byte) 76,
      (byte) 104,
      (byte) 88,
      (byte) 171,
      (byte) 29,
      (byte) 97,
      (byte) 193,
      (byte) 61,
      (byte) 45,
      (byte) 102,
      (byte) 182,
      (byte) 144,
      (byte) 65,
      (byte) 220,
      (byte) 118,
      (byte) 6,
      (byte) 113,
      (byte) 219,
      (byte) 1,
      (byte) 188,
      (byte) 32,
      (byte) 210,
      (byte) 152,
      (byte) 42,
      (byte) 16,
      (byte) 213,
      (byte) 239,
      (byte) 137,
      (byte) 133,
      (byte) 177,
      (byte) 113,
      (byte) 31,
      (byte) 181,
      (byte) 182,
      (byte) 6,
      (byte) 165,
      (byte) 228,
      (byte) 191,
      (byte) 159,
      (byte) 51,
      (byte) 212,
      (byte) 184,
      (byte) 232,
      (byte) 162,
      (byte) 201,
      (byte) 7,
      (byte) 120,
      (byte) 52,
      (byte) 249,
      (byte) 160,
      (byte) 15,
      (byte) 142,
      (byte) 168,
      (byte) 9,
      (byte) 150,
      (byte) 24,
      (byte) 152,
      (byte) 14,
      (byte) 225,
      (byte) 187,
      (byte) 13,
      (byte) 106,
      (byte) 127,
      (byte) 45,
      (byte) 61,
      (byte) 109,
      (byte) 8,
      (byte) 151,
      (byte) 108,
      (byte) 100,
      (byte) 145,
      (byte) 1,
      (byte) 92,
      (byte) 99,
      (byte) 230,
      (byte) 244,
      (byte) 81,
      (byte) 107,
      (byte) 107,
      (byte) 98,
      (byte) 97,
      (byte) 108,
      (byte) 28,
      (byte) 216,
      (byte) 48,
      (byte) 101,
      (byte) 133,
      (byte) 78,
      (byte) 0,
      (byte) 98,
      (byte) 242,
      (byte) 237,
      (byte) 149,
      (byte) 6,
      (byte) 108,
      (byte) 123,
      (byte) 165,
      (byte) 1,
      (byte) 27,
      (byte) 193,
      (byte) 244,
      (byte) 8,
      (byte) 130,
      (byte) 87,
      (byte) 196,
      (byte) 15,
      (byte) 245,
      (byte) 198,
      (byte) 217,
      (byte) 176,
      (byte) 99,
      (byte) 80,
      (byte) 233,
      (byte) 183,
      (byte) 18,
      (byte) 234,
      (byte) 184,
      (byte) 190,
      (byte) 139,
      (byte) 124,
      (byte) 136,
      (byte) 185,
      (byte) 252,
      (byte) 223,
      (byte) 29,
      (byte) 221,
      (byte) 98,
      (byte) 73,
      (byte) 45,
      (byte) 218,
      (byte) 21,
      (byte) 243,
      (byte) 124,
      (byte) 211,
      (byte) 140,
      (byte) 101,
      (byte) 76,
      (byte) 212,
      (byte) 251,
      (byte) 88,
      (byte) 97,
      (byte) 178,
      (byte) 77,
      (byte) 206,
      (byte) 81,
      (byte) 181,
      (byte) 58,
      (byte) 116,
      (byte) 0,
      (byte) 188,
      (byte) 163,
      (byte) 226,
      (byte) 48,
      (byte) 187,
      (byte) 212,
      (byte) 65,
      (byte) 165,
      (byte) 223,
      (byte) 74,
      (byte) 215,
      (byte) 149,
      (byte) 216,
      (byte) 61,
      (byte) 109,
      (byte) 196,
      (byte) 209,
      (byte) 164,
      (byte) 251,
      (byte) 244,
      (byte) 214,
      (byte) 211,
      (byte) 106,
      (byte) 233,
      (byte) 105,
      (byte) 67,
      (byte) 252,
      (byte) 217,
      (byte) 110,
      (byte) 52,
      (byte) 70,
      (byte) 136,
      (byte) 103,
      (byte) 173,
      (byte) 208,
      (byte) 184,
      (byte) 96,
      (byte) 218,
      (byte) 115,
      (byte) 45,
      (byte) 4,
      (byte) 68,
      (byte) 229,
      (byte) 29,
      (byte) 3,
      (byte) 51,
      (byte) 95,
      (byte) 76,
      (byte) 10,
      (byte) 170,
      (byte) 201,
      (byte) 124,
      (byte) 13,
      (byte) 221,
      (byte) 60,
      (byte) 113,
      (byte) 5,
      (byte) 80,
      (byte) 170,
      (byte) 65,
      (byte) 2,
      (byte) 39,
      (byte) 16,
      (byte) 16,
      (byte) 11,
      (byte) 190,
      (byte) 134,
      (byte) 32,
      (byte) 12,
      (byte) 201,
      (byte) 37,
      (byte) 181,
      (byte) 104,
      (byte) 87,
      (byte) 179,
      (byte) 133,
      (byte) 111,
      (byte) 32,
      (byte) 9,
      (byte) 212,
      (byte) 102,
      (byte) 185,
      (byte) 159,
      (byte) 228,
      (byte) 97,
      (byte) 206,
      (byte) 14,
      (byte) 249,
      (byte) 222,
      (byte) 94,
      (byte) 8,
      (byte) 201,
      (byte) 217,
      (byte) 41,
      (byte) 34,
      (byte) 152,
      (byte) 208,
      (byte) 176,
      (byte) 180,
      (byte) 168,
      (byte) 87,
      (byte) 199,
      (byte) 23,
      (byte) 61,
      (byte) 179,
      (byte) 89,
      (byte) 129,
      (byte) 13,
      (byte) 180,
      (byte) 62,
      (byte) 59,
      (byte) 92,
      (byte) 189,
      (byte) 183,
      (byte) 173,
      (byte) 108,
      (byte) 186,
      (byte) 192,
      (byte) 32,
      (byte) 131,
      (byte) 184,
      (byte) 237,
      (byte) 182,
      (byte) 179,
      (byte) 191,
      (byte) 154,
      (byte) 12,
      (byte) 226,
      (byte) 182,
      (byte) 3,
      (byte) 154,
      (byte) 210,
      (byte) 177,
      (byte) 116,
      (byte) 57,
      (byte) 71,
      (byte) 213,
      (byte) 234,
      (byte) 175,
      (byte) 119,
      (byte) 210,
      (byte) 157,
      (byte) 21,
      (byte) 38,
      (byte) 219,
      (byte) 4,
      (byte) 131,
      (byte) 22,
      (byte) 220,
      (byte) 115,
      (byte) 18,
      (byte) 11,
      (byte) 99,
      (byte) 227,
      (byte) 132,
      (byte) 59,
      (byte) 100,
      (byte) 148,
      (byte) 62,
      (byte) 106,
      (byte) 109,
      (byte) 13,
      (byte) 168,
      (byte) 90,
      (byte) 106,
      (byte) 122,
      (byte) 11,
      (byte) 207,
      (byte) 14,
      (byte) 228,
      (byte) 157,
      byte.MaxValue,
      (byte) 9,
      (byte) 147,
      (byte) 39,
      (byte) 174,
      (byte) 0,
      (byte) 10,
      (byte) 177,
      (byte) 158,
      (byte) 7,
      (byte) 125,
      (byte) 68,
      (byte) 147,
      (byte) 15,
      (byte) 240,
      (byte) 210,
      (byte) 162,
      (byte) 8,
      (byte) 135,
      (byte) 104,
      (byte) 242,
      (byte) 1,
      (byte) 30,
      (byte) 254,
      (byte) 194,
      (byte) 6,
      (byte) 105,
      (byte) 93,
      (byte) 87,
      (byte) 98,
      (byte) 247,
      (byte) 203,
      (byte) 103,
      (byte) 101,
      (byte) 128,
      (byte) 113,
      (byte) 54,
      (byte) 108,
      (byte) 25,
      (byte) 231,
      (byte) 6,
      (byte) 107,
      (byte) 110,
      (byte) 118,
      (byte) 27,
      (byte) 212,
      (byte) 254,
      (byte) 224,
      (byte) 43,
      (byte) 211,
      (byte) 137,
      (byte) 90,
      (byte) 122,
      (byte) 218,
      (byte) 16,
      (byte) 204,
      (byte) 74,
      (byte) 221,
      (byte) 103,
      (byte) 111,
      (byte) 223,
      (byte) 185,
      (byte) 249,
      (byte) 249,
      (byte) 239,
      (byte) 190,
      (byte) 142,
      (byte) 67,
      (byte) 190,
      (byte) 183,
      (byte) 23,
      (byte) 213,
      (byte) 142,
      (byte) 176,
      (byte) 96,
      (byte) 232,
      (byte) 163,
      (byte) 214,
      (byte) 214,
      (byte) 126,
      (byte) 147,
      (byte) 209,
      (byte) 161,
      (byte) 196,
      (byte) 194,
      (byte) 216,
      (byte) 56,
      (byte) 82,
      (byte) 242,
      (byte) 223,
      (byte) 79,
      (byte) 241,
      (byte) 103,
      (byte) 187,
      (byte) 209,
      (byte) 103,
      (byte) 87,
      (byte) 188,
      (byte) 166,
      (byte) 221,
      (byte) 6,
      (byte) 181,
      (byte) 63,
      (byte) 75,
      (byte) 54,
      (byte) 178,
      (byte) 72,
      (byte) 218,
      (byte) 43,
      (byte) 13,
      (byte) 216,
      (byte) 76,
      (byte) 27,
      (byte) 10,
      (byte) 175,
      (byte) 246,
      (byte) 74,
      (byte) 3,
      (byte) 54,
      (byte) 96,
      (byte) 122,
      (byte) 4,
      (byte) 65,
      (byte) 195,
      (byte) 239,
      (byte) 96,
      (byte) 223,
      (byte) 85,
      (byte) 223,
      (byte) 103,
      (byte) 168,
      (byte) 239,
      (byte) 142,
      (byte) 110,
      (byte) 49,
      (byte) 121,
      (byte) 14,
      (byte) 105,
      (byte) 70,
      (byte) 140,
      (byte) 179,
      (byte) 81,
      (byte) 203,
      (byte) 26,
      (byte) 131,
      (byte) 99,
      (byte) 188,
      (byte) 160,
      (byte) 210,
      (byte) 111,
      (byte) 37,
      (byte) 54,
      (byte) 226,
      (byte) 104,
      (byte) 82,
      (byte) 149,
      (byte) 119,
      (byte) 12,
      (byte) 204,
      (byte) 3,
      (byte) 71,
      (byte) 11,
      (byte) 187,
      (byte) 185,
      (byte) 20,
      (byte) 2,
      (byte) 34,
      (byte) 47,
      (byte) 38,
      (byte) 5,
      (byte) 85,
      (byte) 190,
      (byte) 59,
      (byte) 182,
      (byte) 197,
      (byte) 40,
      (byte) 11,
      (byte) 189,
      (byte) 178,
      (byte) 146,
      (byte) 90,
      (byte) 180,
      (byte) 43,
      (byte) 4,
      (byte) 106,
      (byte) 179,
      (byte) 92,
      (byte) 167,
      byte.MaxValue,
      (byte) 215,
      (byte) 194,
      (byte) 49,
      (byte) 207,
      (byte) 208,
      (byte) 181,
      (byte) 139,
      (byte) 158,
      (byte) 217,
      (byte) 44,
      (byte) 29,
      (byte) 174,
      (byte) 222,
      (byte) 91,
      (byte) 176,
      (byte) 114,
      (byte) 100,
      (byte) 155,
      (byte) 38,
      (byte) 242,
      (byte) 227,
      (byte) 236,
      (byte) 156,
      (byte) 163,
      (byte) 106,
      (byte) 117,
      (byte) 10,
      (byte) 147,
      (byte) 109,
      (byte) 2,
      (byte) 169,
      (byte) 6,
      (byte) 9,
      (byte) 156,
      (byte) 63,
      (byte) 54,
      (byte) 14,
      (byte) 235,
      (byte) 133,
      (byte) 104,
      (byte) 7,
      (byte) 114,
      (byte) 19,
      (byte) 7,
      (byte) 0,
      (byte) 5,
      (byte) 130,
      (byte) 72,
      (byte) 191,
      (byte) 149,
      (byte) 20,
      (byte) 122,
      (byte) 184,
      (byte) 226,
      (byte) 174,
      (byte) 43,
      (byte) 177,
      (byte) 123,
      (byte) 56,
      (byte) 27,
      (byte) 182,
      (byte) 12,
      (byte) 155,
      (byte) 142,
      (byte) 210,
      (byte) 146,
      (byte) 13,
      (byte) 190,
      (byte) 213,
      (byte) 229,
      (byte) 183,
      (byte) 239,
      (byte) 220,
      (byte) 124,
      (byte) 33,
      (byte) 223,
      (byte) 219,
      (byte) 11,
      (byte) 148,
      (byte) 210,
      (byte) 211,
      (byte) 134,
      (byte) 66,
      (byte) 226,
      (byte) 212,
      (byte) 241,
      (byte) 248,
      (byte) 179,
      (byte) 221,
      (byte) 104,
      (byte) 110,
      (byte) 131,
      (byte) 218,
      (byte) 31,
      (byte) 205,
      (byte) 22,
      (byte) 190,
      (byte) 129,
      (byte) 91,
      (byte) 38,
      (byte) 185,
      (byte) 246,
      (byte) 225,
      (byte) 119,
      (byte) 176,
      (byte) 111,
      (byte) 119,
      (byte) 71,
      (byte) 183,
      (byte) 24,
      (byte) 224,
      (byte) 90,
      (byte) 8,
      (byte) 136,
      (byte) 112,
      (byte) 106,
      (byte) 15,
      (byte) 241,
      (byte) 202,
      (byte) 59,
      (byte) 6,
      (byte) 102,
      (byte) 92,
      (byte) 11,
      (byte) 1,
      (byte) 17,
      byte.MaxValue,
      (byte) 158,
      (byte) 101,
      (byte) 143,
      (byte) 105,
      (byte) 174,
      (byte) 98,
      (byte) 248,
      (byte) 211,
      byte.MaxValue,
      (byte) 107,
      (byte) 97,
      (byte) 69,
      (byte) 207,
      (byte) 108,
      (byte) 22,
      (byte) 120,
      (byte) 226,
      (byte) 10,
      (byte) 160,
      (byte) 238,
      (byte) 210,
      (byte) 13,
      (byte) 215,
      (byte) 84,
      (byte) 131,
      (byte) 4,
      (byte) 78,
      (byte) 194,
      (byte) 179,
      (byte) 3,
      (byte) 57,
      (byte) 97,
      (byte) 38,
      (byte) 103,
      (byte) 167,
      (byte) 247,
      (byte) 22,
      (byte) 96,
      (byte) 208,
      (byte) 77,
      (byte) 71,
      (byte) 105,
      (byte) 73,
      (byte) 219,
      (byte) 119,
      (byte) 110,
      (byte) 62,
      (byte) 74,
      (byte) 106,
      (byte) 209,
      (byte) 174,
      (byte) 220,
      (byte) 90,
      (byte) 214,
      (byte) 217,
      (byte) 102,
      (byte) 11,
      (byte) 223,
      (byte) 64,
      (byte) 240,
      (byte) 59,
      (byte) 216,
      (byte) 55,
      (byte) 83,
      (byte) 174,
      (byte) 188,
      (byte) 169,
      (byte) 197,
      (byte) 158,
      (byte) 187,
      (byte) 222,
      (byte) 127,
      (byte) 207,
      (byte) 178,
      (byte) 71,
      (byte) 233,
      byte.MaxValue,
      (byte) 181,
      (byte) 48,
      (byte) 28,
      (byte) 249,
      (byte) 189,
      (byte) 189,
      (byte) 138,
      (byte) 205,
      (byte) 186,
      (byte) 202,
      (byte) 48,
      (byte) 158,
      (byte) 179,
      (byte) 83,
      (byte) 166,
      (byte) 163,
      (byte) 188,
      (byte) 36,
      (byte) 5,
      (byte) 59,
      (byte) 208,
      (byte) 186,
      (byte) 163,
      (byte) 6,
      (byte) 215,
      (byte) 205,
      (byte) 233,
      (byte) 87,
      (byte) 222,
      (byte) 84,
      (byte) 191,
      (byte) 103,
      (byte) 217,
      (byte) 35,
      (byte) 46,
      (byte) 114,
      (byte) 102,
      (byte) 179,
      (byte) 184,
      (byte) 74,
      (byte) 97,
      (byte) 196,
      (byte) 2,
      (byte) 27,
      (byte) 56,
      (byte) 93,
      (byte) 148,
      (byte) 43,
      (byte) 111,
      (byte) 43,
      (byte) 55,
      (byte) 190,
      (byte) 203,
      (byte) 180,
      (byte) 161,
      (byte) 142,
      (byte) 204,
      (byte) 195,
      (byte) 27,
      (byte) 223,
      (byte) 13,
      (byte) 90,
      (byte) 141,
      (byte) 237,
      (byte) 2,
      (byte) 45
    }, false))
    {
      using (BinaryReader binaryReader = new BinaryReader((Stream) memoryStream))
      {
        int num1 = 0;
        for (int index1 = 0; index1 < 1024; index1 += 4)
        {
          uint num2 = binaryReader.ReadUInt32();
          for (uint index2 = 0; index2 < 256U; ++index2)
          {
            uint num3 = index2 >> 1;
            if ((index2 & 1U) > 0U)
              num3 ^= num2;
            for (int index3 = 0; index3 < 7; ++index3)
            {
              if ((num3 & 1U) > 0U)
                num3 = num3 >> 1 ^ num2;
              else
                num3 >>= 1;
            }
            numArray[num1++] = num3;
          }
        }
      }
    }
    return numArray;
  }

  private static ulong smethod_4(uint uint_9, uint uint_10)
  {
    ulong num = (ulong) uint_9;
    return (ulong) uint_10 << 32 | num;
  }

  private static uint smethod_5(ushort ushort_1, ushort ushort_2)
  {
    uint num = (uint) ushort_1;
    return (uint) ushort_2 << 16 | num;
  }

  private static ushort smethod_6(byte byte_3, byte byte_4)
  {
    ushort num = (ushort) byte_3;
    return (ushort) ((uint) (ushort) byte_4 << 8 | (uint) num);
  }

  private static ushort smethod_7(uint uint_9)
  {
    return (ushort) (uint_9 & (uint) ushort.MaxValue);
  }

  private static ushort smethod_8(uint uint_9)
  {
    return (ushort) (uint_9 >> 16 & (uint) ushort.MaxValue);
  }

  private static byte smethod_9(ushort ushort_1)
  {
    return (byte) ((uint) ushort_1 & (uint) byte.MaxValue);
  }

  private static byte smethod_10(ushort ushort_1)
  {
    return (byte) ((int) ushort_1 >> 8 & (int) byte.MaxValue);
  }

  private static ulong smethod_11()
  {
    byte[] buffer = new byte[8];
    GClass4.random_0.NextBytes(buffer);
    return BitConverter.ToUInt64(buffer, 0);
  }

  private static uint smethod_12()
  {
    byte[] buffer = new byte[4];
    GClass4.random_0.NextBytes(buffer);
    return BitConverter.ToUInt32(buffer, 0);
  }

  private static ushort smethod_13()
  {
    byte[] buffer = new byte[2];
    GClass4.random_0.NextBytes(buffer);
    return BitConverter.ToUInt16(buffer, 0);
  }

  private static byte smethod_14()
  {
    return (byte) ((uint) GClass4.smethod_13() & (uint) byte.MaxValue);
  }

  private uint method_0(ref uint uint_9)
  {
    for (int index = 0; index < 32; ++index)
      uint_9 = (uint) (((int) (((((uint_9 >> 2 ^ uint_9) >> 2 ^ uint_9) >> 1 ^ uint_9) >> 1 ^ uint_9) >> 1) ^ (int) uint_9) & 1 | (((int) uint_9 & 1) << 31 | (int) (uint_9 >> 1)) & -2);
    return uint_9;
  }

  private void method_1(uint uint_9)
  {
    if (uint_9 == 0U)
      uint_9 = 2596254646U;
    uint uint_9_1 = uint_9;
    uint num1 = this.method_0(ref uint_9_1);
    uint num2 = this.method_0(ref uint_9_1);
    uint num3 = this.method_0(ref uint_9_1);
    int num4 = (int) this.method_0(ref uint_9_1);
    byte num5 = (byte) ((int) uint_9_1 & (int) byte.MaxValue ^ (int) num3 & (int) byte.MaxValue);
    byte num6 = (byte) ((int) num1 & (int) byte.MaxValue ^ (int) num2 & (int) byte.MaxValue);
    if (num5 == (byte) 0)
      num5 = (byte) 1;
    if (num6 == (byte) 0)
      num6 = (byte) 1;
    this.byte_0[0] = (byte) ((uint) num5 ^ (uint) num6);
    this.byte_0[1] = num6;
    this.byte_0[2] = num5;
  }

  private uint method_2(uint uint_9, uint uint_10, uint uint_11)
  {
    long num1 = 1;
    long num2 = (long) uint_11;
    uint num3;
    if (uint_10 != 0U)
    {
      while (uint_10 > 0U)
      {
        if ((uint_10 & 1U) > 0U)
          num1 = num2 * num1 % (long) uint_9;
        uint_10 >>= 1;
        num2 = num2 * num2 % (long) uint_9;
      }
      num3 = (uint) num1;
    }
    else
      num3 = 1U;
    return num3;
  }

  private void method_3(ref ulong ulong_4, uint uint_9, byte byte_3)
  {
    byte[] bytes = BitConverter.GetBytes(ulong_4);
    bytes[0] ^= (byte) ((uint) bytes[0] + (uint) GClass4.smethod_9(GClass4.smethod_7(uint_9)) + (uint) byte_3);
    bytes[1] ^= (byte) ((uint) bytes[1] + (uint) GClass4.smethod_10(GClass4.smethod_7(uint_9)) + (uint) byte_3);
    bytes[2] ^= (byte) ((uint) bytes[2] + (uint) GClass4.smethod_9(GClass4.smethod_8(uint_9)) + (uint) byte_3);
    bytes[3] ^= (byte) ((uint) bytes[3] + (uint) GClass4.smethod_10(GClass4.smethod_8(uint_9)) + (uint) byte_3);
    bytes[4] ^= (byte) ((uint) bytes[4] + (uint) GClass4.smethod_9(GClass4.smethod_7(uint_9)) + (uint) byte_3);
    bytes[5] ^= (byte) ((uint) bytes[5] + (uint) GClass4.smethod_10(GClass4.smethod_7(uint_9)) + (uint) byte_3);
    bytes[6] ^= (byte) ((uint) bytes[6] + (uint) GClass4.smethod_9(GClass4.smethod_8(uint_9)) + (uint) byte_3);
    bytes[7] ^= (byte) ((uint) bytes[7] + (uint) GClass4.smethod_10(GClass4.smethod_8(uint_9)) + (uint) byte_3);
    ulong_4 = BitConverter.ToUInt64(bytes, 0);
  }

  private byte method_4(bool bool_3)
  {
    byte num1 = (byte) ((uint) this.byte_0[2] * ((uint) ~this.byte_0[0] + (uint) this.byte_0[1]));
    byte num2 = (byte) ((uint) num1 ^ (uint) num1 >> 4);
    if (bool_3)
      this.byte_0[0] = num2;
    return num2;
  }

  private byte method_5(byte[] byte_3, int int_0, int int_1)
  {
    uint num1 = uint.MaxValue;
    uint num2 = this.uint_8 << 8;
    for (int index = int_0; index < int_0 + int_1; ++index)
      num1 = num1 >> 8 ^ GClass4.uint_0[(int) num2 + (((int) byte_3[index] ^ (int) num1) & (int) byte.MaxValue)];
    return (byte) (((int) (num1 >> 24) & (int) byte.MaxValue) + ((int) (num1 >> 8) & (int) byte.MaxValue) + ((int) (num1 >> 16) & (int) byte.MaxValue) + ((int) num1 & (int) byte.MaxValue));
  }

  private byte method_6(byte[] byte_3)
  {
    return this.method_5(byte_3, 0, byte_3.Length);
  }

  private void method_7(GClass4.Class7 class7_1)
  {
    this.byte_1 = GClass4.smethod_1(class7_1);
    this.class7_0 = class7_1;
    this.bool_0 = true;
    GClass3 gclass3 = new GClass3((ushort) 20480);
    gclass3.method_31(this.byte_1);
    if (this.class7_0.byte_1 == (byte) 1)
    {
      this.ulong_0 = GClass4.smethod_11();
      this.gclass0_0.method_5(BitConverter.GetBytes(this.ulong_0));
      gclass3.method_37(this.ulong_0);
    }
    if (this.class7_0.byte_2 == (byte) 1)
    {
      this.uint_7 = (uint) GClass4.smethod_14();
      this.method_1(this.uint_7);
      this.uint_8 = (uint) GClass4.smethod_14();
      gclass3.method_35(this.uint_7);
      gclass3.method_35(this.uint_8);
    }
    if (this.class7_0.byte_3 == (byte) 1)
    {
      this.ulong_1 = GClass4.smethod_11();
      this.uint_1 = GClass4.smethod_12() & (uint) int.MaxValue;
      this.uint_2 = GClass4.smethod_12() & (uint) int.MaxValue;
      this.uint_3 = GClass4.smethod_12() & (uint) int.MaxValue;
      this.uint_4 = this.method_2(this.uint_3, this.uint_1, this.uint_2);
      gclass3.method_37(this.ulong_1);
      gclass3.method_35(this.uint_2);
      gclass3.method_35(this.uint_3);
      gclass3.method_35(this.uint_4);
    }
    this.list_1.Add(gclass3);
  }

  private void method_8(ushort ushort_1, Class5 class5_0, bool bool_3)
  {
    if (bool_3)
      throw new Exception("[SecurityAPI::Handshake] Received an illogical (encrypted) handshake packet.");
    if (this.bool_0)
    {
      if (this.class7_0.byte_3 != (byte) 0)
      {
        if (ushort_1 != (ushort) 36864)
        {
          if (ushort_1 != (ushort) 20480)
            throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (programmer error).");
          if (this.bool_2)
            throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (duplicate 0x5000).");
          this.bool_2 = true;
          this.uint_5 = class5_0.ReadUInt32();
          this.ulong_2 = class5_0.ReadUInt64();
          this.uint_6 = this.method_2(this.uint_3, this.uint_1, this.uint_5);
          ulong ulong_4 = GClass4.smethod_4(this.uint_4, this.uint_5);
          this.method_3(ref ulong_4, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_6)) & 3U));
          this.gclass0_0.method_5(BitConverter.GetBytes(ulong_4));
          this.ulong_2 = BitConverter.ToUInt64(this.gclass0_0.method_10(BitConverter.GetBytes(this.ulong_2)), 0);
          ulong_4 = GClass4.smethod_4(this.uint_5, this.uint_4);
          this.method_3(ref ulong_4, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_5)) & 7U));
          if ((long) this.ulong_2 != (long) ulong_4)
            throw new Exception("[SecurityAPI::Handshake] Client signature error.");
          ulong_4 = GClass4.smethod_4(this.uint_4, this.uint_5);
          this.method_3(ref ulong_4, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_6)) & 3U));
          this.gclass0_0.method_5(BitConverter.GetBytes(ulong_4));
          this.ulong_3 = GClass4.smethod_4(this.uint_4, this.uint_5);
          this.method_3(ref this.ulong_3, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_4)) & 7U));
          this.ulong_3 = BitConverter.ToUInt64(this.gclass0_0.method_8(BitConverter.GetBytes(this.ulong_3)), 0);
          this.method_3(ref this.ulong_1, this.uint_6, (byte) 3);
          this.gclass0_0.method_5(BitConverter.GetBytes(this.ulong_1));
          byte byte_1 = GClass4.smethod_1(new GClass4.Class7()
          {
            byte_4 = (byte) 1
          });
          GClass3 gclass3 = new GClass3((ushort) 20480);
          gclass3.method_31(byte_1);
          gclass3.method_37(this.ulong_3);
          this.list_1.Add(gclass3);
        }
        else
        {
          if (!this.bool_2)
            throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (out of order 0x9000).");
          if (this.bool_1)
            throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (duplicate 0x9000).");
          this.bool_1 = true;
        }
      }
      else if (ushort_1 == (ushort) 36864)
      {
        if (this.bool_1)
          throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (duplicate 0x9000).");
        this.bool_1 = true;
      }
      else
      {
        if (ushort_1 == (ushort) 20480)
          throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (0x5000 with no handshake).");
        throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (programmer error).");
      }
    }
    else
    {
      if (ushort_1 != (ushort) 20480)
        throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (programmer error).");
      byte byte_3 = class5_0.ReadByte();
      GClass4.Class7 class7 = GClass4.smethod_2(byte_3);
      if (this.byte_1 == (byte) 0)
      {
        this.byte_1 = byte_3;
        this.class7_0 = class7;
      }
      if (class7.byte_1 == (byte) 1)
      {
        this.ulong_0 = class5_0.ReadUInt64();
        this.gclass0_0.method_5(BitConverter.GetBytes(this.ulong_0));
      }
      if (class7.byte_2 == (byte) 1)
      {
        this.uint_7 = class5_0.ReadUInt32();
        this.uint_8 = class5_0.ReadUInt32();
        this.method_1(this.uint_7);
      }
      if (class7.byte_3 == (byte) 1)
      {
        this.ulong_1 = class5_0.ReadUInt64();
        this.uint_2 = class5_0.ReadUInt32();
        this.uint_3 = class5_0.ReadUInt32();
        this.uint_4 = class5_0.ReadUInt32();
        this.uint_1 = GClass4.smethod_12() & (uint) int.MaxValue;
        this.uint_5 = this.method_2(this.uint_3, this.uint_1, this.uint_2);
        this.uint_6 = this.method_2(this.uint_3, this.uint_1, this.uint_4);
        ulong ulong_4 = GClass4.smethod_4(this.uint_4, this.uint_5);
        this.method_3(ref ulong_4, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_6)) & 3U));
        this.gclass0_0.method_5(BitConverter.GetBytes(ulong_4));
        this.ulong_2 = GClass4.smethod_4(this.uint_5, this.uint_4);
        this.method_3(ref this.ulong_2, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_5)) & 7U));
        this.ulong_2 = BitConverter.ToUInt64(this.gclass0_0.method_8(BitConverter.GetBytes(this.ulong_2)), 0);
      }
      if (class7.byte_4 == (byte) 1)
      {
        this.ulong_3 = class5_0.ReadUInt64();
        ulong ulong_4 = GClass4.smethod_4(this.uint_4, this.uint_5);
        this.method_3(ref ulong_4, this.uint_6, (byte) ((uint) GClass4.smethod_9(GClass4.smethod_7(this.uint_4)) & 7U));
        if ((long) this.ulong_3 != (long) BitConverter.ToUInt64(this.gclass0_0.method_8(BitConverter.GetBytes(ulong_4)), 0))
          throw new Exception("[SecurityAPI::Handshake] Server signature error.");
        this.method_3(ref this.ulong_1, this.uint_6, (byte) 3);
        this.gclass0_0.method_5(BitConverter.GetBytes(this.ulong_1));
      }
      if (class7.byte_3 == (byte) 1 && class7.byte_4 == (byte) 0)
      {
        if ((this.bool_2 ? 1 : (this.bool_1 ? 1 : 0)) != 0)
          throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (duplicate 0x5000).");
        GClass3 gclass3 = new GClass3((ushort) 20480);
        gclass3.method_35(this.uint_5);
        gclass3.method_37(this.ulong_2);
        this.list_1.Insert(0, gclass3);
        this.bool_2 = true;
      }
      else
      {
        if (this.bool_1)
          throw new Exception("[SecurityAPI::Handshake] Received an illogical handshake packet (duplicate 0x5000).");
        GClass3 gclass3_1 = new GClass3((ushort) 36864);
        GClass3 gclass3_2 = new GClass3((ushort) 8193, true, false);
        gclass3_2.method_41(this.string_0);
        gclass3_2.method_31(this.byte_2);
        this.list_1.Insert(0, gclass3_2);
        this.list_1.Insert(0, gclass3_1);
        this.bool_2 = true;
        this.bool_1 = true;
      }
    }
  }

  private byte[] method_9(ushort ushort_1, byte[] byte_3, bool bool_3)
  {
    if (byte_3.Length >= 32768)
      throw new Exception("[SecurityAPI::FormatPacket] Payload is too large!");
    ushort length = (ushort) byte_3.Length;
    Class6 class6 = new Class6();
    class6.Write(length);
    class6.Write(ushort_1);
    class6.Write((ushort) 0);
    class6.Write(byte_3);
    class6.Flush();
    if ((!bool_3 ? 0 : (this.class7_0.byte_1 == (byte) 1 ? 1 : (this.class7_0.byte_2 != (byte) 1 ? 0 : (this.class7_0.byte_1 == (byte) 0 ? 1 : 0)))) != 0)
    {
      long offset = class6.BaseStream.Seek(0L, SeekOrigin.Current);
      ushort num = (ushort) ((uint) length | 32768U);
      class6.BaseStream.Seek(0L, SeekOrigin.Begin);
      class6.Write(num);
      class6.Flush();
      class6.BaseStream.Seek(offset, SeekOrigin.Begin);
    }
    if (!this.bool_0 && this.class7_0.byte_2 == (byte) 1)
    {
      long offset = class6.BaseStream.Seek(0L, SeekOrigin.Current);
      byte num1 = this.method_4(true);
      class6.BaseStream.Seek(4L, SeekOrigin.Begin);
      class6.Write(num1);
      class6.Flush();
      byte num2 = this.method_6(class6.method_0());
      class6.BaseStream.Seek(5L, SeekOrigin.Begin);
      class6.Write(num2);
      class6.Flush();
      class6.BaseStream.Seek(offset, SeekOrigin.Begin);
    }
    if (bool_3 && this.class7_0.byte_1 == (byte) 1)
    {
      byte[] byte_0 = class6.method_0();
      byte[] buffer = this.gclass0_0.method_9(byte_0, 2, byte_0.Length - 2);
      class6.BaseStream.Seek(2L, SeekOrigin.Begin);
      class6.Write(buffer);
      class6.Flush();
    }
    else
    {
      int num;
      if (bool_3)
      {
        if (this.class7_0.byte_2 == (byte) 1)
        {
          if (this.class7_0.byte_1 == (byte) 0)
            goto label_13;
          else
            goto label_15;
        }
        else
          num = 0;
      }
      else
        num = 0;
      if (num == 0)
        goto label_15;
label_13:
      long offset = class6.BaseStream.Seek(0L, SeekOrigin.Current);
      class6.BaseStream.Seek(0L, SeekOrigin.Begin);
      class6.Write(length);
      class6.Flush();
      class6.BaseStream.Seek(offset, SeekOrigin.Begin);
    }
label_15:
    return class6.method_0();
  }

  private bool method_10()
  {
    bool flag;
    if (this.list_1.Count != 0)
    {
      if (!this.bool_1)
      {
        GClass3 gclass3 = this.list_1[0];
        flag = (gclass3.UInt16_0 == (ushort) 20480 ? 1 : (gclass3.UInt16_0 == (ushort) 36864 ? 1 : 0)) != 0;
      }
      else
        flag = true;
    }
    else
      flag = false;
    return flag;
  }

  private KeyValuePair<GClass5, GClass3> method_11()
  {
    if (this.list_1.Count == 0)
      throw new Exception("[SecurityAPI::GetPacketToSend] No packets are avaliable to send.");
    GClass3 gclass3 = this.list_1[0];
    this.list_1.RemoveAt(0);
    if (!gclass3.Boolean_1)
    {
      bool bool_3 = gclass3.Boolean_0;
      if (!this.bool_0 && this.list_2.Contains(gclass3.UInt16_0))
        bool_3 = true;
      byte[] byte_1 = this.method_9(gclass3.UInt16_0, gclass3.method_0(), bool_3);
      gclass3.method_1();
      return new KeyValuePair<GClass5, GClass3>(new GClass5(byte_1, 0, byte_1.Length, true), gclass3);
    }
    ushort num = 0;
    Class6 class6_1 = new Class6();
    Class6 class6_2 = new Class6();
    byte[] numArray = gclass3.method_0();
    Class5 class5 = new Class5(numArray);
    GClass5 gclass5 = new GClass5(4089, 0, numArray.Length);
    while (gclass5.Int32_1 > 0)
    {
      Class6 class6_3 = new Class6();
      int count = gclass5.Int32_1 <= 4089 ? gclass5.Int32_1 : 4089;
      class6_3.Write((byte) 0);
      class6_3.Write(numArray, gclass5.Int32_0, count);
      gclass5.Int32_0 += count;
      gclass5.Int32_1 -= count;
      class6_2.Write(this.method_9((ushort) 24589, class6_3.method_0(), false));
      ++num;
    }
    Class6 class6_4 = new Class6();
    class6_4.Write((byte) 1);
    class6_4.Write((short) num);
    class6_4.Write(gclass3.UInt16_0);
    class6_1.Write(this.method_9((ushort) 24589, class6_4.method_0(), false));
    class6_1.Write(class6_2.method_0());
    byte[] byte_1_1 = class6_1.method_0();
    gclass3.method_1();
    return new KeyValuePair<GClass5, GClass3>(new GClass5(byte_1_1, 0, byte_1_1.Length, true), gclass3);
  }

  public GClass4()
  {
    this.uint_1 = 0U;
    this.uint_2 = 0U;
    this.uint_3 = 0U;
    this.uint_4 = 0U;
    this.uint_5 = 0U;
    this.uint_6 = 0U;
    this.uint_7 = 0U;
    this.uint_8 = 0U;
    this.ulong_0 = 0UL;
    this.ulong_1 = 0UL;
    this.byte_0 = new byte[3];
    this.byte_0[0] = (byte) 0;
    this.byte_0[1] = (byte) 0;
    this.byte_0[2] = (byte) 0;
    this.ulong_2 = 0UL;
    this.ulong_3 = 0UL;
    this.bool_0 = false;
    this.byte_1 = (byte) 0;
    this.class7_0 = new GClass4.Class7();
    this.bool_1 = false;
    this.bool_2 = false;
    this.byte_2 = (byte) 0;
    this.string_0 = "SR_Client";
    this.list_1 = new List<GClass3>();
    this.list_0 = new List<GClass3>();
    this.list_2 = new List<ushort>();
    this.list_2.Add((ushort) 8193);
    this.list_2.Add((ushort) 24832);
    this.list_2.Add((ushort) 24833);
    this.list_2.Add((ushort) 24834);
    this.list_2.Add((ushort) 24835);
    this.list_2.Add((ushort) 24839);
    this.gclass0_0 = new GClass0();
    this.gclass5_0 = new GClass5(8192);
    this.gclass5_1 = (GClass5) null;
    this.ushort_0 = (ushort) 0;
    this.gclass3_0 = (GClass3) null;
    this.object_0 = new object();
  }

  public void method_12(string string_1, byte byte_3)
  {
    lock (this.object_0)
    {
      this.string_0 = string_1;
      this.byte_2 = byte_3;
    }
  }

  public void method_13(bool bool_3, bool bool_4, bool bool_5)
  {
    lock (this.object_0)
    {
      GClass4.Class7 class7_1 = new GClass4.Class7();
      if (bool_3)
      {
        class7_1.byte_0 = (byte) 0;
        class7_1.byte_1 = (byte) 1;
      }
      if (bool_4)
      {
        class7_1.byte_0 = (byte) 0;
        class7_1.byte_2 = (byte) 1;
      }
      if (bool_5)
      {
        class7_1.byte_0 = (byte) 0;
        class7_1.byte_3 = (byte) 1;
      }
      if (!bool_3 && !bool_4 && !bool_5)
        class7_1.byte_0 = (byte) 1;
      this.method_7(class7_1);
    }
  }

  public void method_14(ushort ushort_1)
  {
    lock (this.object_0)
    {
      if (this.list_2.Contains(ushort_1))
        return;
      this.list_2.Add(ushort_1);
    }
  }

  public void method_15(GClass3 gclass3_1)
  {
    if ((gclass3_1.UInt16_0 == (ushort) 20480 ? 1 : (gclass3_1.UInt16_0 == (ushort) 36864 ? 1 : 0)) != 0)
      throw new Exception("[SecurityAPI::Send] Handshake packets cannot be sent through this function.");
    lock (this.object_0)
      this.list_1.Add(gclass3_1);
  }

  public void method_16(byte[] byte_3, int int_0, int int_1)
  {
    this.method_17(new GClass5(byte_3, int_0, int_1, true));
  }

  public void method_17(GClass5 gclass5_2)
  {
    List<GClass5> gclass5List = new List<GClass5>();
    lock (this.object_0)
    {
      int num1 = gclass5_2.Int32_1 - gclass5_2.Int32_0;
      int num2 = 0;
      while (num1 > 0)
      {
        int count = num1;
        int num3 = this.gclass5_0.Byte_0.Length - this.gclass5_0.Int32_1;
        if (count > num3)
          count = num3;
        num1 -= count;
        Buffer.BlockCopy((Array) gclass5_2.Byte_0, gclass5_2.Int32_0 + num2, (Array) this.gclass5_0.Byte_0, this.gclass5_0.Int32_1, count);
        this.gclass5_0.Int32_1 += count;
        num2 += count;
        while (this.gclass5_0.Int32_1 > 0)
        {
          if (this.gclass5_1 == null)
          {
            if (this.gclass5_0.Int32_1 >= 2)
            {
              int num4 = (int) this.gclass5_0.Byte_0[1] << 8 | (int) this.gclass5_0.Byte_0[0];
              int num5;
              if ((num4 & 32768) <= 0)
              {
                num5 = num4 + 6;
              }
              else
              {
                int num6 = num4 & (int) short.MaxValue;
                num5 = this.class7_0.byte_1 == (byte) 1 ? 2 + this.gclass0_0.method_7(num6 + 4) : num6 + 6;
              }
              this.gclass5_1 = new GClass5(num5, 0, num5);
            }
            else
              break;
          }
          int num7 = this.gclass5_1.Int32_1 - this.gclass5_1.Int32_0;
          if (num7 > this.gclass5_0.Int32_1)
            num7 = this.gclass5_0.Int32_1;
          Buffer.BlockCopy((Array) this.gclass5_0.Byte_0, 0, (Array) this.gclass5_1.Byte_0, this.gclass5_1.Int32_0, num7);
          this.gclass5_1.Int32_0 += num7;
          this.gclass5_0.Int32_1 -= num7;
          if (this.gclass5_0.Int32_1 > 0)
            Buffer.BlockCopy((Array) this.gclass5_0.Byte_0, num7, (Array) this.gclass5_0.Byte_0, 0, this.gclass5_0.Int32_1);
          if (this.gclass5_1.Int32_1 == this.gclass5_1.Int32_0)
          {
            this.gclass5_1.Int32_0 = 0;
            gclass5List.Add(this.gclass5_1);
            this.gclass5_1 = (GClass5) null;
          }
          else
            break;
        }
      }
      if (gclass5List.Count <= 0)
        return;
      foreach (GClass5 gclass5 in gclass5List)
      {
        bool bool_3 = false;
        int num3 = (int) gclass5.Byte_0[1] << 8 | (int) gclass5.Byte_0[0];
        if ((num3 & 32768) > 0)
        {
          if (this.class7_0.byte_1 != (byte) 1)
          {
            num3 &= (int) short.MaxValue;
          }
          else
          {
            num3 &= (int) short.MaxValue;
            bool_3 = true;
          }
        }
        if (bool_3)
        {
          byte[] numArray1 = this.gclass0_0.method_11(gclass5.Byte_0, 2, gclass5.Int32_1 - 2);
          byte[] numArray2 = new byte[6 + num3];
          Buffer.BlockCopy((Array) BitConverter.GetBytes((ushort) num3), 0, (Array) numArray2, 0, 2);
          Buffer.BlockCopy((Array) numArray1, 0, (Array) numArray2, 2, 4 + num3);
          gclass5.Byte_0 = (byte[]) null;
          gclass5.Byte_0 = numArray2;
        }
        Class5 class5_0 = new Class5(gclass5.Byte_0);
        int int_1 = (int) class5_0.ReadUInt16();
        ushort ushort_1 = class5_0.ReadUInt16();
        byte num4 = class5_0.ReadByte();
        byte num5 = class5_0.ReadByte();
        if (this.bool_0 && this.class7_0.byte_2 == (byte) 1)
        {
          byte num6 = this.method_4(true);
          if ((int) num4 != (int) num6)
            throw new Exception("[SecurityAPI::Recv] Count byte mismatch.");
          if ((bool_3 || (this.class7_0.byte_2 != (byte) 1 ? 0 : (this.class7_0.byte_1 == (byte) 0 ? 1 : 0)) != 0) && (bool_3 || this.list_2.Contains(ushort_1)))
          {
            int_1 |= 32768;
            Buffer.BlockCopy((Array) BitConverter.GetBytes((ushort) int_1), 0, (Array) gclass5.Byte_0, 0, 2);
          }
          gclass5.Byte_0[5] = (byte) 0;
          byte num7 = this.method_6(gclass5.Byte_0);
          if ((int) num5 != (int) num7)
            throw new Exception("[SecurityAPI::Recv] CRC byte mismatch.");
          gclass5.Byte_0[4] = (byte) 0;
          if ((!bool_3 ? (this.class7_0.byte_2 != (byte) 1 ? 0 : (this.class7_0.byte_1 == (byte) 0 ? 1 : 0)) : 1) != 0 && (bool_3 || this.list_2.Contains(ushort_1)))
          {
            int_1 &= (int) short.MaxValue;
            Buffer.BlockCopy((Array) BitConverter.GetBytes((ushort) int_1), 0, (Array) gclass5.Byte_0, 0, 2);
          }
        }
        if ((ushort_1 == (ushort) 20480 ? 1 : (ushort_1 == (ushort) 36864 ? 1 : 0)) != 0)
        {
          this.method_8(ushort_1, class5_0, bool_3);
          GClass3 gclass3 = new GClass3(ushort_1, bool_3, false, gclass5.Byte_0, 6, int_1);
          gclass3.method_1();
          this.list_0.Add(gclass3);
        }
        else
        {
          if (this.bool_0 && !this.bool_1)
            throw new Exception("[SecurityAPI::Recv] The client has not accepted the handshake.");
          if (ushort_1 == (ushort) 24589)
          {
            if (class5_0.ReadByte() != (byte) 1)
            {
              if (this.gclass3_0 == null)
                throw new Exception("[SecurityAPI::Recv] A malformed 0x600D packet was received.");
              this.gclass3_0.method_57(class5_0.ReadBytes(int_1 - 1));
              --this.ushort_0;
              if (this.ushort_0 == (ushort) 0)
              {
                this.gclass3_0.method_1();
                this.list_0.Add(this.gclass3_0);
                this.gclass3_0 = (GClass3) null;
              }
            }
            else
            {
              this.ushort_0 = class5_0.ReadUInt16();
              this.gclass3_0 = new GClass3(class5_0.ReadUInt16(), bool_3, true);
            }
          }
          else
          {
            GClass3 gclass3 = new GClass3(ushort_1, bool_3, false, gclass5.Byte_0, 6, int_1);
            gclass3.method_1();
            this.list_0.Add(gclass3);
          }
        }
      }
    }
  }

  public List<KeyValuePair<GClass5, GClass3>> method_18()
  {
    List<KeyValuePair<GClass5, GClass3>> keyValuePairList = new List<KeyValuePair<GClass5, GClass3>>();
    lock (this.object_0)
    {
      if (this.method_10())
      {
        keyValuePairList = new List<KeyValuePair<GClass5, GClass3>>();
        while (this.method_10())
          keyValuePairList.Add(this.method_11());
      }
    }
    return keyValuePairList;
  }

  public List<GClass3> method_19()
  {
    List<GClass3> gclass3List = new List<GClass3>();
    lock (this.object_0)
    {
      if (this.list_0.Count > 0)
      {
        gclass3List = this.list_0;
        this.list_0 = new List<GClass3>();
      }
    }
    return gclass3List;
  }

  [StructLayout(LayoutKind.Explicit, Size = 8)]
  private class Class7
  {
    [FieldOffset(0)]
    public byte byte_0;
    [FieldOffset(1)]
    public byte byte_1;
    [FieldOffset(2)]
    public byte byte_2;
    [FieldOffset(3)]
    public byte byte_3;
    [FieldOffset(4)]
    public byte byte_4;
    [FieldOffset(5)]
    public byte byte_5;
    [FieldOffset(6)]
    public byte byte_6;
    [FieldOffset(7)]
    public byte byte_7;
  }
}
