﻿// Decompiled with JetBrains decompiler
// Type: Class21
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;

internal class Class21
{
  private static SqlDataReader sqlDataReader_0;

  public static void smethod_0()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Class21.Class22 stateMachine = new Class21.Class22();
    // ISSUE: reference to a compiler-generated field
    stateMachine.asyncVoidMethodBuilder_0 = AsyncVoidMethodBuilder.Create();
    // ISSUE: reference to a compiler-generated field
    stateMachine.int_0 = -1;
    // ISSUE: reference to a compiler-generated field
    stateMachine.asyncVoidMethodBuilder_0.Start<Class21.Class22>(ref stateMachine);
  }

  public static void smethod_1()
  {
    try
    {
      try
      {
        Class21.sqlDataReader_0 = new SqlCommand("SELECT *FROM [_RefOpcode]", GClass7.sqlConnection_0).ExecuteReader();
        while (Class21.sqlDataReader_0.Read())
        {
          string str1 = Convert.ToString(Class21.sqlDataReader_0["Opcode"]);
          string str2 = Convert.ToString(Class21.sqlDataReader_0["String"]);
          if (!Class32.dictionary_2.ContainsKey(Convert.ToUInt16(str1, 16)))
            Class32.dictionary_2.Add(Convert.ToUInt16(str1, 16), str2);
        }
        Class21.sqlDataReader_0.Close();
      }
      catch (SqlException ex)
      {
        Class18.smethod_0(string.Concat((object) ex) ?? "", "Red");
      }
    }
    catch
    {
    }
  }
}
