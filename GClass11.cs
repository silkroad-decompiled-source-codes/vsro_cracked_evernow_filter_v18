﻿// Decompiled with JetBrains decompiler
// Type: GClass11
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;

public sealed class GClass11
{
  public static Dictionary<int, string> dictionary_0 = new Dictionary<int, string>();
  public Socket socket_0 = (Socket) null;
  public object object_0 = new object();
  public Socket socket_1 = (Socket) null;
  public byte[] byte_0 = new byte[8192];
  public byte[] byte_1 = new byte[8192];
  public GClass4 gclass4_0 = new GClass4();
  public GClass4 gclass4_1 = new GClass4();
  public ulong ulong_0 = 0;
  private DateTime dateTime_0 = DateTime.Now;
  public string string_0 = "";
  private DateTime dateTime_1 = new DateTime();
  private DateTime dateTime_2 = new DateTime();
  private DateTime dateTime_3 = new DateTime();
  private DateTime dateTime_4 = new DateTime();
  private DateTime dateTime_5 = DateTime.Now;
  private DateTime dateTime_6 = new DateTime();
  private DateTime dateTime_7 = new DateTime();
  private DateTime dateTime_8 = new DateTime();
  private DateTime dateTime_9 = new DateTime();
  private DateTime dateTime_10 = new DateTime();
  private DateTime dateTime_11 = new DateTime();
  private DateTime dateTime_12 = new DateTime();
  private DateTime dateTime_13 = new DateTime();
  private DateTime dateTime_14 = new DateTime();
  private DateTime dateTime_15 = new DateTime();
  private DateTime dateTime_16 = new DateTime();
  private DateTime dateTime_17 = new DateTime();
  private DateTime dateTime_18 = new DateTime();
  public string string_1 = (string) null;
  public string string_2 = (string) null;
  public string string_3 = (string) null;
  public string string_4 = "non";
  public string string_5 = "non";
  public string string_6 = "non";
  public string string_7 = (string) null;
  public bool bool_0 = true;
  public bool bool_1 = false;
  public bool bool_2 = true;
  public bool bool_3 = false;
  public bool bool_4 = false;
  public bool bool_5 = false;
  public bool bool_6 = false;
  public bool bool_7 = false;
  public bool bool_8 = false;
  public bool bool_9 = false;
  public bool bool_10 = false;
  public bool bool_11 = true;
  public bool bool_12 = false;
  public bool bool_13 = false;
  public bool bool_14 = false;
  public bool bool_15 = false;
  public bool bool_16 = true;
  public bool bool_17 = false;
  public bool bool_18 = false;
  public bool bool_19 = false;
  public bool bool_20 = false;
  public bool bool_21 = false;
  public bool bool_22 = false;
  public bool bool_23 = false;
  private int int_0 = 0;
  private int int_1 = 0;
  private int int_2 = 0;
  private int int_3 = 0;
  private int int_4 = 0;
  private int int_5 = 0;
  private int int_6 = 0;
  private int int_7 = 0;
  private int int_8 = 0;
  public uint uint_0 = 0;
  public uint uint_1 = 0;
  public uint uint_2 = 0;
  private uint uint_6 = 0;
  private uint uint_7 = 0;
  private uint uint_8 = 0;
  private bool bool_24 = false;
  private bool bool_25 = false;
  private bool bool_26 = false;
  private bool bool_27 = true;
  private DateTime dateTime_19 = DateTime.Now;
  private DateTime dateTime_20 = DateTime.Now;
  private bool bool_28 = false;
  private bool bool_29 = false;
  private bool bool_30 = false;
  private SqlDataReader sqlDataReader_0;
  public GClass9.GEnum9 genum9_0;
  public GClass9.GDelegate0 gdelegate0_0;
  public string string_8;
  public uint uint_3;
  public uint uint_4;
  public uint uint_5;
  public byte byte_2;
  public byte byte_3;

  private void method_0()
  {
    try
    {
      if (!Class32.bool_4 || !this.bool_9)
        return;
      this.dateTime_19 = DateTime.Now;
      if (Class32.bool_6)
      {
        if (this.bool_1 || this.int_2 < (int) Convert.ToInt16(Class32.int_1))
          return;
        this.method_8();
        if (!Class32.bool_5)
          return;
        this.method_10(Class33.string_114.Replace("{Amount}", Class32.uint_0.ToString()));
      }
      else
      {
        if (this.int_2 < (int) Convert.ToInt16(Class32.int_1))
          return;
        this.method_8();
        if (!Class32.bool_5)
          return;
        this.method_10(Class33.string_114.Replace("{Amount}", Class32.uint_0.ToString()));
      }
    }
    catch
    {
    }
  }

  public GClass11(Socket socket_2, GClass9.GDelegate0 gdelegate0_1)
  {
    try
    {
      this.gdelegate0_0 = gdelegate0_1;
      this.socket_0 = socket_2;
      this.genum9_0 = GClass9.GEnum9.AgentServer;
      this.socket_1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      this.string_0 = ((IPEndPoint) socket_2.RemoteEndPoint).Address.ToString();
      this.method_24();
      this.method_23();
    }
    catch
    {
    }
    try
    {
      this.socket_1.Connect((EndPoint) new IPEndPoint(IPAddress.Parse(Class32.string_22), Convert.ToInt32(Class32.string_19)));
      this.gclass4_0.method_13(true, true, true);
      this.method_4();
      this.method_5(false);
      this.method_20();
      this.method_25();
      this.method_26();
    }
    catch
    {
      this.method_7();
    }
  }

  private void method_1(IAsyncResult iasyncResult_0)
  {
    lock (this.object_0)
    {
      try
      {
        int int_1 = this.socket_0.EndReceive(iasyncResult_0);
        if ((uint) int_1 > 0U)
        {
          this.gclass4_0.method_16(this.byte_0, 0, int_1);
          List<GClass3> gclass3List = this.gclass4_0.method_19();
          if (gclass3List != null)
          {
            foreach (GClass3 gclass3 in gclass3List)
            {
              this.method_20();
              this.method_19(gclass3);
              ushort uint160 = gclass3.UInt16_0;
              DateTime dateTime;
              TimeSpan timeSpan;
              if (uint160 <= (ushort) 28839)
              {
                if (uint160 > (ushort) 28688)
                {
                  switch (uint160)
                  {
                    case 28705:
                      if ((Class32.bool_73 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && (this.bool_19 ? (Class32.list_22.Contains(this.int_4) ? 1 : 0) : 0) != 0)
                      {
                        this.method_10(Class33.string_14);
                        continue;
                      }
                      if ((!Class32.bool_81 || Class32.list_15.Contains(this.string_2) || !Class32.list_21.Contains(this.int_4) ? 0 : (!this.bool_15 ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_105);
                        continue;
                      }
                      if ((Class32.bool_7 ? (this.bool_2 ? 1 : 0) : 0) != 0)
                      {
                        if (Class32.string_23 == "Symbol")
                        {
                          GClass3 gclass3_1 = new GClass3((ushort) 29698);
                          gclass3_1.method_31((byte) 0);
                          this.gclass4_1.method_15(gclass3_1);
                          this.method_5(true);
                        }
                        this.bool_2 = false;
                      }
                      this.method_45();
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28709:
                      if (Class32.dictionary_5.ContainsKey(this.string_2))
                      {
                        try
                        {
                          string str = Class32.dictionary_5[this.string_2];
                          dateTime = Convert.ToDateTime(DateTime.Now);
                          long num1 = long.Parse(dateTime.ToString("yyyyMMddHHmm"));
                          dateTime = Convert.ToDateTime(str);
                          long num2 = long.Parse(dateTime.ToString("yyyyMMddHHmm"));
                          if (num1 <= num2)
                          {
                            this.method_10(Class33.string_48);
                            continue;
                          }
                        }
                        catch
                        {
                        }
                      }
                      switch (gclass3.method_4())
                      {
                        case 1:
                        case 3:
                          int num3 = (int) gclass3.method_4();
                          string string_0_1 = gclass3.method_14().ToString();
                          if ((!Class34.smethod_9(string_0_1) ? 0 : (Class32.bool_113 ? 1 : 0)) != 0)
                          {
                            this.method_10(Class33.string_93);
                            continue;
                          }
                          if (Class32.bool_130 && string_0_1.Equals(Class32.string_51, StringComparison.InvariantCultureIgnoreCase))
                          {
                            if (!Class32.bool_124)
                              this.method_9("Availables Commands:");
                            else
                              this.method_11("Availables Commands:", (byte) 56);
                            if (Class32.bool_86)
                            {
                              if (!Class32.bool_124)
                              {
                                this.method_9("!lock PinCode - lock your character to avoid any hacking.");
                                this.method_9("!unlock PinCode - remove lock from your character.");
                              }
                              else
                              {
                                this.method_11("!lock PinCode - lock your character to avoid any hacking.", (byte) 50);
                                this.method_11("!unlock PinCode - remove lock from your character.", (byte) 50);
                              }
                            }
                            if (Class32.bool_126)
                            {
                              if (!Class32.bool_124)
                              {
                                this.method_9("!titles - Displays available titles for your character.");
                                this.method_9("!set TitleID - Sets your character title to your desired one.");
                                continue;
                              }
                              this.method_11("!titles - Displays available titles for your character.", (byte) 50);
                              this.method_11("!set TitleID - Sets your character title to your desired one.", (byte) 50);
                              continue;
                            }
                            continue;
                          }
                          if (Class32.bool_126)
                          {
                            if (string_0_1.Equals("!titles", StringComparison.InvariantCultureIgnoreCase))
                            {
                              if (!Class32.bool_124)
                                this.method_9("Availables Titles: (TitleID:TitleName)");
                              else
                                this.method_11("Availables Titles: (TitleID:TitleName)", (byte) 56);
                              try
                              {
                                this.int_8 = 0;
                                GClass11.dictionary_0.Clear();
                                this.sqlDataReader_0 = new SqlCommand("SELECT *FROM [_TitlesID] WHERE CharName = '" + this.string_1 + "' ", GClass7.sqlConnection_0).ExecuteReader();
                                while (this.sqlDataReader_0.Read())
                                {
                                  int int32 = Convert.ToInt32(this.sqlDataReader_0["TitleID"]);
                                  string str = Convert.ToString(this.sqlDataReader_0["TitleName"]);
                                  if (!GClass11.dictionary_0.ContainsKey(int32))
                                  {
                                    lock (GClass11.dictionary_0)
                                      GClass11.dictionary_0.Add(int32, str);
                                    if (Class32.bool_124)
                                      this.method_11(int32.ToString() + ":" + str, (byte) 50);
                                    else
                                      this.method_9(int32.ToString() + ":" + str);
                                    ++this.int_8;
                                  }
                                }
                                this.sqlDataReader_0.Close();
                                if (this.int_8 == 0)
                                {
                                  if (Class32.bool_124)
                                  {
                                    this.method_11("You haven't titles yet.", (byte) 50);
                                    continue;
                                  }
                                  this.method_9("You haven't titles yet.");
                                  continue;
                                }
                                continue;
                              }
                              catch
                              {
                                continue;
                              }
                            }
                            else if (string_0_1.Contains("!set"))
                            {
                              try
                              {
                                int int32 = Convert.ToInt32(string_0_1.Replace("!set ", ""));
                                if (GClass11.dictionary_0.ContainsKey(int32))
                                {
                                  GClass7.smethod_0("UPDATE [" + Class32.string_9 + "].[dbo].[_Char] SET HwanLevel = '" + (object) int32 + "' WHERE CharName16 = '" + this.string_1 + "'");
                                  if (!Class32.bool_124)
                                    this.method_9("[Titles System] Your title has been changed.");
                                  else
                                    this.method_11("[Titles System] Your title has been changed.", (byte) 58);
                                  if (!Class32.dictionary_10.ContainsKey(this.uint_1))
                                  {
                                    lock (Class32.dictionary_10)
                                      Class32.dictionary_10.Add(this.uint_1, int32);
                                  }
                                  else
                                  {
                                    lock (Class32.dictionary_10)
                                      Class32.dictionary_10.Remove(this.uint_1);
                                    Class32.dictionary_10.Add(this.uint_1, int32);
                                  }
                                  this.method_15(this.uint_1, int32);
                                  continue;
                                }
                                if (Class32.bool_124)
                                {
                                  this.method_11("[Titles System] title dosen't exist in your Titles list.", (byte) 58);
                                  continue;
                                }
                                this.method_9("[Titles System] title dosen't exist in your Titles list.");
                                continue;
                              }
                              catch
                              {
                                if (Class32.bool_124)
                                {
                                  this.method_11("[Titles System] Your title is incorrect format.", (byte) 58);
                                  continue;
                                }
                                this.method_9("[Titles System] Your title is incorrect format.");
                                continue;
                              }
                            }
                          }
                          if ((!this.bool_2 ? 0 : (Class32.bool_7 ? 1 : 0)) != 0)
                          {
                            if (Class32.string_23 == "Symbol")
                            {
                              GClass3 gclass3_1 = new GClass3((ushort) 29698);
                              gclass3_1.method_31((byte) 0);
                              this.gclass4_1.method_15(gclass3_1);
                              this.method_5(true);
                            }
                            this.bool_1 = false;
                            this.dateTime_5 = DateTime.Now;
                            this.bool_2 = false;
                          }
                          if (Class32.bool_86)
                          {
                            try
                            {
                              if (string_0_1.Contains("!lock"))
                              {
                                string str = string_0_1.Replace(" ", "".ToString()).Replace("!lock", "".ToString());
                                if ((str.Length == 0 || str.Length > 18 || (string.IsNullOrEmpty(str) || str.Contains(" ")) ? 0 : (!Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) != 0)
                                {
                                  if (Class32.bool_124)
                                  {
                                    this.method_11(Class33.string_94.Replace("{Pass}", str.ToString()), (byte) 7);
                                    this.method_11("[Lock System] Your character has been locked.", (byte) 51);
                                  }
                                  else
                                    this.method_10(Class33.string_94.Replace("{Pass}", str.ToString()));
                                  if (!Class32.dictionary_4.ContainsKey(this.string_1))
                                  {
                                    lock (Class32.dictionary_4)
                                      Class32.dictionary_4.Add(this.string_1, str);
                                  }
                                  if (!Class32.list_28.Contains(this.uint_1))
                                  {
                                    lock (Class32.list_28)
                                    {
                                      Class32.list_28.Add(this.uint_1);
                                      continue;
                                    }
                                  }
                                  else
                                    continue;
                                }
                                else
                                {
                                  if (str.Length <= 18)
                                  {
                                    if (!Class32.dictionary_4.ContainsKey(this.string_1))
                                    {
                                      if (Class32.bool_124)
                                      {
                                        this.method_11(Class33.string_97, (byte) 7);
                                        continue;
                                      }
                                      this.method_10(Class33.string_97);
                                      continue;
                                    }
                                    if (Class32.bool_124)
                                    {
                                      this.method_11(Class33.string_96, (byte) 7);
                                      continue;
                                    }
                                    this.method_10(Class33.string_96);
                                    continue;
                                  }
                                  if (!Class32.bool_124)
                                  {
                                    this.method_10(Class33.string_95);
                                    continue;
                                  }
                                  this.method_11(Class33.string_95, (byte) 7);
                                  continue;
                                }
                              }
                              else if (string_0_1.Contains("!unlock"))
                              {
                                string str = string_0_1.Replace(" ", "".ToString()).Replace("!unlock", "".ToString());
                                if ((str.Length == 0 || str.Length > 18 || (string.IsNullOrEmpty(str) || str.Contains(" ")) || !Class32.dictionary_4.ContainsKey(this.string_1) ? 0 : (str == Class32.dictionary_4[this.string_1] ? 1 : 0)) != 0)
                                {
                                  if (!Class32.bool_124)
                                  {
                                    this.method_10(Class33.string_98.Replace("{Pass}", str.ToString()));
                                  }
                                  else
                                  {
                                    this.method_11(Class33.string_98.Replace("{Pass}", str.ToString()), (byte) 7);
                                    this.method_11("[Lock System] Your character has been unlocked.", (byte) 51);
                                  }
                                  if (Class32.dictionary_4.ContainsKey(this.string_1))
                                  {
                                    lock (Class32.dictionary_4)
                                      Class32.dictionary_4.Remove(this.string_1);
                                  }
                                  while (Class32.list_28.Contains(this.uint_1))
                                    ;
                                  continue;
                                }
                                if (str.Length <= 18)
                                {
                                  if (Class32.dictionary_4.ContainsKey(this.string_1))
                                  {
                                    if (str != Class32.dictionary_4[this.string_1])
                                    {
                                      if (Class32.bool_124)
                                      {
                                        this.method_11(Class33.string_102, (byte) 7);
                                        continue;
                                      }
                                      this.method_10(Class33.string_102);
                                      continue;
                                    }
                                    if (!Class32.bool_124)
                                    {
                                      this.method_10(Class33.string_101);
                                      continue;
                                    }
                                    this.method_11(Class33.string_101, (byte) 7);
                                    continue;
                                  }
                                  if (Class32.bool_124)
                                  {
                                    this.method_11(Class33.string_100, (byte) 7);
                                    continue;
                                  }
                                  this.method_10(Class33.string_100);
                                  continue;
                                }
                                if (!Class32.bool_124)
                                {
                                  this.method_10(Class33.string_99);
                                  continue;
                                }
                                this.method_11(Class33.string_99, (byte) 7);
                                continue;
                              }
                            }
                            catch
                            {
                            }
                          }
                          if (Class32.bool_13)
                          {
                            try
                            {
                              if (string_0_1.Contains(Class32.string_25))
                              {
                                int num1;
                                try
                                {
                                  num1 = Convert.ToInt32(string_0_1.Replace(Class32.string_25, "".ToString()));
                                }
                                catch
                                {
                                  num1 = -1;
                                }
                                if (num1 >= 0)
                                {
                                  if (Class32.dictionary_1.ContainsKey(this.string_1))
                                  {
                                    lock (Class32.dictionary_1)
                                      Class32.dictionary_1.Remove(this.string_1);
                                    Class32.dictionary_1.Add(this.string_1, num1);
                                  }
                                  else
                                  {
                                    lock (Class32.dictionary_1)
                                      Class32.dictionary_1.Add(this.string_1, num1);
                                  }
                                  this.method_9("[Plus System] Order has been Successfully.");
                                  continue;
                                }
                                this.method_9("[Plus System] Hit Number of plus.");
                                continue;
                              }
                              string str1 = string_0_1;
                              string str2 = Class32.string_24;
                              if (str2 == null)
                                str2 = "";
                              else if (str2 == null)
                                str2 = "";
                              if (!str1.Equals(str2, StringComparison.InvariantCultureIgnoreCase))
                              {
                                string str3 = string_0_1;
                                string str4 = Class32.string_26;
                                if (str4 == null)
                                  str4 = "";
                                else if (str4 == null)
                                  str4 = "";
                                if (str3.Equals(str4, StringComparison.InvariantCultureIgnoreCase))
                                {
                                  if (Class32.dictionary_1.ContainsKey(this.string_1))
                                  {
                                    lock (Class32.dictionary_1)
                                      Class32.dictionary_1.Remove(this.string_1);
                                    this.method_9("[Plus System] Order has been Successfully.");
                                    continue;
                                  }
                                  this.method_9("[Plus System] You Aren't in System.");
                                  this.method_9("[Plus System] For More Information , Hit [" + Class32.string_24 + "].");
                                  continue;
                                }
                              }
                              else
                              {
                                this.method_9("[Plus System] To limit your Plus Notice , Hit [" + Class32.string_25 + "AmountPlus].");
                                this.method_9("[Plus System] To Cancel your Plus Notice Limit and will be like a normal player , Hit [" + Class32.string_26 + "].");
                                this.method_9("[Plus System] To Stop your Plus Notice from appear , Hit [" + Class32.string_25 + "0].");
                                continue;
                              }
                            }
                            catch
                            {
                            }
                          }
                          if (Class32.bool_93)
                          {
                            try
                            {
                              if ((string_0_1.Contains(Class32.string_34) || string_0_1.Contains(Class32.string_35) || (string_0_1.Contains(Class32.string_36) || string_0_1.Contains(Class32.string_37)) || string_0_1.Contains(Class32.string_38) ? 1 : (string_0_1.Contains(Class32.string_39) ? 1 : 0)) != 0)
                              {
                                if (Class32.list_37.Contains(this.string_2))
                                {
                                  if (!string_0_1.Contains(Class32.string_34))
                                  {
                                    if (!string_0_1.Contains(Class32.string_36))
                                    {
                                      if (string_0_1.Contains(Class32.string_37))
                                      {
                                        int num1;
                                        try
                                        {
                                          num1 = Convert.ToInt32(string_0_1.Replace(Class32.string_37, "".ToString()));
                                        }
                                        catch
                                        {
                                          num1 = 0;
                                        }
                                        if (num1 > 0)
                                        {
                                          string key = GClass7.smethod_1("SELECT TOP 1 StrUserID FROM [" + Class32.string_10 + "].[dbo].[TB_User] WHERE JID = (SELECT TOP 1 UserJID FROM [" + Class32.string_9 + "].[dbo].[_User] WHERE CharID = '" + (object) this.int_0 + "')");
                                          if (!string.IsNullOrEmpty(key))
                                          {
                                            if (Class32.dictionary_5.ContainsKey(key))
                                            {
                                              GClass7.smethod_0("UPDATE [_BlockChat] SET EndDate = DATEADD (DAY," + (object) num1 + ", EndDate) WHERE StrUserID = '" + key + "'");
                                              string str = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                              lock (Class32.dictionary_5)
                                                Class32.dictionary_5.Remove(key);
                                              Class32.dictionary_5.Add(key, str);
                                              this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been updated blocked to [" + str + "].");
                                              continue;
                                            }
                                            GClass7.smethod_0("INSERT INTO [_BlockChat] (StrUserID,StartDate,EndDate) VALUES ('" + key + "',GETDATE(),DATEADD (DAY," + (object) num1 + ", GETDATE()))");
                                            string str1 = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                            lock (Class32.dictionary_5)
                                              Class32.dictionary_5.Add(key, str1);
                                            this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been blocked to [" + str1 + "].");
                                            continue;
                                          }
                                        }
                                        else if (num1 == 0)
                                        {
                                          this.method_9("[ChatBlock System] Time is incorrect format.");
                                          continue;
                                        }
                                      }
                                      else if (string_0_1.Contains(Class32.string_38))
                                      {
                                        int num1;
                                        try
                                        {
                                          num1 = Convert.ToInt32(string_0_1.Replace(Class32.string_38, "".ToString()));
                                        }
                                        catch
                                        {
                                          num1 = 0;
                                        }
                                        if (num1 > 0)
                                        {
                                          string key = GClass7.smethod_1("SELECT TOP 1 StrUserID FROM [" + Class32.string_10 + "].[dbo].[TB_User] WHERE JID = (SELECT TOP 1 UserJID FROM [" + Class32.string_9 + "].[dbo].[_User] WHERE CharID = '" + (object) this.int_0 + "')");
                                          if (!string.IsNullOrEmpty(key))
                                          {
                                            if (Class32.dictionary_5.ContainsKey(key))
                                            {
                                              GClass7.smethod_0("UPDATE [_BlockChat] SET EndDate = DATEADD (MONTH," + (object) num1 + ", EndDate) WHERE StrUserID = '" + key + "'");
                                              string str = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                              lock (Class32.dictionary_5)
                                                Class32.dictionary_5.Remove(key);
                                              Class32.dictionary_5.Add(key, str);
                                              this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been updated blocked to [" + str + "].");
                                              continue;
                                            }
                                            GClass7.smethod_0("INSERT INTO [_BlockChat] (StrUserID,StartDate,EndDate) VALUES ('" + key + "',GETDATE(),DATEADD (MONTH," + (object) num1 + ", GETDATE()))");
                                            string str1 = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                            lock (Class32.dictionary_5)
                                              Class32.dictionary_5.Add(key, str1);
                                            this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been blocked to [" + str1 + "].");
                                            continue;
                                          }
                                        }
                                        else if (num1 == 0)
                                        {
                                          this.method_9("[ChatBlock System] Time is incorrect format.");
                                          continue;
                                        }
                                      }
                                      else if (string_0_1.Contains(Class32.string_39))
                                      {
                                        int num1;
                                        try
                                        {
                                          num1 = Convert.ToInt32(string_0_1.Replace(Class32.string_39, "".ToString()));
                                        }
                                        catch
                                        {
                                          num1 = 0;
                                        }
                                        if (num1 > 0)
                                        {
                                          string key = GClass7.smethod_1("SELECT TOP 1 StrUserID FROM [" + Class32.string_10 + "].[dbo].[TB_User] WHERE JID = (SELECT TOP 1 UserJID FROM [" + Class32.string_9 + "].[dbo].[_User] WHERE CharID = '" + (object) this.int_0 + "')");
                                          if (!string.IsNullOrEmpty(key))
                                          {
                                            if (Class32.dictionary_5.ContainsKey(key))
                                            {
                                              GClass7.smethod_0("UPDATE [_BlockChat] SET EndDate = DATEADD (YEAR," + (object) num1 + ", EndDate) WHERE StrUserID = '" + key + "'");
                                              string str = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                              lock (Class32.dictionary_5)
                                                Class32.dictionary_5.Remove(key);
                                              Class32.dictionary_5.Add(key, str);
                                              this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been updated blocked to [" + str + "].");
                                              continue;
                                            }
                                            GClass7.smethod_0("INSERT INTO [_BlockChat] (StrUserID,StartDate,EndDate) VALUES ('" + key + "',GETDATE(),DATEADD (YEAR," + (object) num1 + ", GETDATE()))");
                                            string str1 = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                            lock (Class32.dictionary_5)
                                              Class32.dictionary_5.Add(key, str1);
                                            this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been blocked to [" + str1 + "].");
                                            continue;
                                          }
                                        }
                                        else if (num1 == 0)
                                        {
                                          this.method_9("[ChatBlock System] Time is incorrect format.");
                                          continue;
                                        }
                                      }
                                      else if (string_0_1.Contains(Class32.string_35))
                                      {
                                        string str;
                                        try
                                        {
                                          str = string_0_1.Replace(Class32.string_35, "".ToString());
                                        }
                                        catch
                                        {
                                          str = (string) null;
                                        }
                                        if (!string.IsNullOrEmpty(str))
                                        {
                                          string key = GClass7.smethod_1("SELECT TOP 1 StrUserID FROM [" + Class32.string_10 + "].[dbo].[TB_User] WHERE JID = (SELECT TOP 1 UserJID FROM [" + Class32.string_9 + "].[dbo].[_User] WHERE CharID = '" + (object) this.int_0 + "')");
                                          if (Class32.dictionary_5.ContainsKey(key))
                                          {
                                            GClass7.smethod_0("DELETE FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                            lock (Class32.dictionary_5)
                                              Class32.dictionary_5.Remove(key);
                                            this.method_9("[ChatBlock System] CharName:[" + str + "] has been unblocked.");
                                            continue;
                                          }
                                          this.method_9("[ChatBlock System] This CharName [" + str + "] haven't block chat yet.");
                                          continue;
                                        }
                                        this.method_9("[ChatBlock System] Give me CharName First by Hit [" + Class32.string_35 + "CharName].");
                                        continue;
                                      }
                                    }
                                    else
                                    {
                                      int num1;
                                      try
                                      {
                                        num1 = Convert.ToInt32(string_0_1.Replace(Class32.string_36, "".ToString()));
                                      }
                                      catch
                                      {
                                        num1 = 0;
                                      }
                                      if (num1 > 0)
                                      {
                                        string key = GClass7.smethod_1("SELECT TOP 1 StrUserID FROM [" + Class32.string_10 + "].[dbo].[TB_User] WHERE JID = (SELECT TOP 1 UserJID FROM [" + Class32.string_9 + "].[dbo].[_User] WHERE CharID = '" + (object) this.int_0 + "')");
                                        if (!string.IsNullOrEmpty(key))
                                        {
                                          if (Class32.dictionary_5.ContainsKey(key))
                                          {
                                            GClass7.smethod_0("UPDATE [_BlockChat] SET EndDate = DATEADD (HOUR," + (object) num1 + ", EndDate) WHERE StrUserID = '" + key + "'");
                                            string str = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                            lock (Class32.dictionary_5)
                                              Class32.dictionary_5.Remove(key);
                                            Class32.dictionary_5.Add(key, str);
                                            this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been updated blocked to [" + str + "].");
                                            continue;
                                          }
                                          GClass7.smethod_0("INSERT INTO [_BlockChat] (StrUserID,StartDate,EndDate) VALUES ('" + key + "',GETDATE(),DATEADD (HOUR," + (object) num1 + ", GETDATE()))");
                                          string str1 = GClass7.smethod_1("SELECT TOP 1 EndDate FROM [_BlockChat] WHERE StrUserID = '" + key + "'");
                                          lock (Class32.dictionary_5)
                                            Class32.dictionary_5.Add(key, str1);
                                          this.method_9("[ChatBlock System] CharName:[" + this.string_7 + "] has been blocked to [" + str1 + "].");
                                          continue;
                                        }
                                      }
                                      else if (num1 == 0)
                                      {
                                        this.method_9("[ChatBlock System] Time is incorrect format.");
                                        continue;
                                      }
                                    }
                                  }
                                  else
                                  {
                                    this.string_7 = string_0_1.Replace(Class32.string_34, "".ToString());
                                    this.int_0 = GClass7.smethod_2("SELECT TOP 1 CharID FROM [" + Class32.string_9 + "].[dbo].[_Char] WHERE CharName16 = '" + this.string_7 + "'");
                                    if (this.int_0 > 0)
                                    {
                                      this.method_9("[ChatBlock System] Choose time for block.");
                                      this.method_9("[ChatBlock System] Hit [" + Class32.string_36 + "No of Hour(s)].");
                                      this.method_9("[ChatBlock System] Hit [" + Class32.string_37 + "No of Day(s)].");
                                      this.method_9("[ChatBlock System] Hit [" + Class32.string_38 + "No of Month(s)].");
                                      this.method_9("[ChatBlock System] Hit [" + Class32.string_39 + "No of Year(s)].");
                                      continue;
                                    }
                                    if (this.int_0 != 0)
                                    {
                                      this.method_9("[ChatBlock System] This CharName is incorrect format.");
                                      continue;
                                    }
                                    this.method_9("[ChatBlock System] This CharName doesn't exist in server.");
                                    continue;
                                  }
                                }
                                else
                                  Class18.smethod_0("Warring:StrUserID [" + this.string_2 + "] try to use Chat Block System and he isn't from your team.", "Orange");
                              }
                            }
                            catch
                            {
                            }
                          }
                          if (Class32.bool_94)
                          {
                            string str1 = string_0_1;
                            string str2 = Class32.string_43;
                            if (str2 == null)
                              str2 = "";
                            else if (str2 == null)
                              str2 = "";
                            if ((str1.Equals(str2, StringComparison.InvariantCultureIgnoreCase) ? 1 : (Class34.smethod_11(string_0_1) ? 1 : 0)) == 0)
                            {
                              string str3 = string_0_1;
                              string str4 = Class32.string_42;
                              if (str4 == null)
                                str4 = "";
                              else if (str4 == null)
                                str4 = "";
                              if ((str3.Equals(str4, StringComparison.InvariantCultureIgnoreCase) ? 1 : (Class34.smethod_10(string_0_1) ? 1 : 0)) != 0)
                              {
                                GClass3 gclass3_1 = new GClass3((ushort) 28709);
                                gclass3_1.method_31((byte) 2);
                                gclass3_1.method_31((byte) 0);
                                gclass3_1.method_41(Class32.string_40);
                                gclass3_1.method_41(string_0_1);
                                this.gclass4_1.method_15(gclass3_1);
                                this.method_5(true);
                                continue;
                              }
                              break;
                            }
                            GClass3 gclass3_1_1 = new GClass3((ushort) 28709);
                            gclass3_1_1.method_31((byte) 2);
                            gclass3_1_1.method_31((byte) 0);
                            gclass3_1_1.method_41(Class32.string_41);
                            gclass3_1_1.method_41(string_0_1);
                            this.gclass4_1.method_15(gclass3_1_1);
                            this.method_5(true);
                            continue;
                          }
                          break;
                        case 2:
                          int num4 = (int) gclass3.method_4();
                          string str5 = Class17.smethod_1(gclass3.method_14());
                          string string_0_2 = Class17.smethod_1(gclass3.method_14().ToString());
                          if ((!this.bool_2 ? 0 : (Class32.bool_7 ? 1 : 0)) != 0)
                          {
                            if (Class32.string_23 == "Symbol")
                            {
                              GClass3 gclass3_1 = new GClass3((ushort) 29698);
                              gclass3_1.method_31((byte) 0);
                              this.gclass4_1.method_15(gclass3_1);
                              this.method_5(true);
                            }
                            this.bool_2 = false;
                            this.bool_1 = false;
                            this.dateTime_5 = DateTime.Now;
                          }
                          if ((!Class34.smethod_9(string_0_2) ? 0 : (Class32.bool_113 ? 1 : 0)) != 0)
                          {
                            this.method_10(Class33.string_93);
                            continue;
                          }
                          if (Class32.bool_110)
                            GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + string_0_2.Replace("'", "''") + "','" + str5 + "',6");
                          if (Class32.bool_94)
                          {
                            if ((!this.bool_28 ? 0 : (str5 == Class32.string_40 ? 1 : 0)) != 0)
                            {
                              this.method_10("Close npc first before pm [" + Class32.string_40 + "].");
                              continue;
                            }
                            if ((this.bool_29 ? (str5 == Class32.string_40 ? 1 : 0) : 0) != 0)
                            {
                              this.method_10("Close storage-keeper first before pm [" + Class32.string_40 + "].");
                              continue;
                            }
                            break;
                          }
                          break;
                        case 4:
                        case 5:
                        case 11:
                        case 16:
                          int num5 = (int) gclass3.method_4();
                          string lower1 = gclass3.method_14().ToString().ToLower();
                          if ((!this.bool_2 ? 0 : (Class32.bool_7 ? 1 : 0)) != 0)
                          {
                            if (Class32.string_23 == "Symbol")
                            {
                              GClass3 gclass3_1 = new GClass3((ushort) 29698);
                              gclass3_1.method_31((byte) 0);
                              this.gclass4_1.method_15(gclass3_1);
                              this.method_5(true);
                            }
                            this.bool_2 = false;
                            this.bool_1 = false;
                            this.dateTime_5 = DateTime.Now;
                          }
                          if ((!Class34.smethod_9(lower1) ? 0 : (Class32.bool_113 ? 1 : 0)) != 0)
                          {
                            this.method_10(Class33.string_93);
                            continue;
                          }
                          break;
                        case 7:
                          int num6 = (int) gclass3.method_4();
                          string str6 = gclass3.method_14().ToString();
                          if ((!Class32.bool_101 || !Class32.list_35.Contains(this.string_2) ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && !Class32.list_55.Contains(this.string_2))
                          {
                            this.method_10("You are not allowed to use [Notice] GM commands.");
                            continue;
                          }
                          if (Class32.bool_134)
                          {
                            if (Class32.bool_136)
                              Class18.smethod_2("[Notice] " + str6.Replace("ABEV#$13", ""));
                            else if (Class32.bool_141 && this.string_1 == Class32.string_58)
                              Class18.smethod_2("[Notice] " + str6.Replace("ABEV#$13", ""));
                          }
                          if (Class32.bool_133)
                            GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + str6.Replace("'", "''") + "','None',8");
                          if ((!Class32.bool_131 ? 0 : (this.string_1 == Class32.string_52 ? 1 : 0)) != 0)
                          {
                            if ((Class32.bool_132 ? (str6.Contains("ABEV#$13") ? 1 : 0) : 0) != 0)
                            {
                              string string54 = Class32.string_54;
                              byte byte_4 = !(string54 == "blue") ? (!(string54 == "green") ? (string54 == "pink" ? (byte) 13 : (byte) 0) : (byte) 12) : (byte) 11;
                              if (byte_4 > (byte) 0)
                              {
                                this.method_14(str6.Replace("ABEV#$13", ""), byte_4);
                                continue;
                              }
                              continue;
                            }
                            string string53 = Class32.string_53;
                            byte byte_4_1 = string53 == "blue" ? (byte) 11 : (!(string53 == "green") ? (!(string53 == "pink") ? (byte) 0 : (byte) 13) : (byte) 12);
                            if (byte_4_1 > (byte) 0)
                            {
                              this.method_14(str6.Replace("ABEV#$13", ""), byte_4_1);
                              continue;
                            }
                            continue;
                          }
                          break;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28724:
                      switch (gclass3.method_4())
                      {
                        case 0:
                          int num7 = (int) gclass3.method_4();
                          if (gclass3.method_4() == (byte) 8)
                          {
                            if (Class32.bool_96)
                              this.method_9(Class33.string_2);
                            if (Class32.bool_28 && this.bool_26)
                            {
                              this.method_10(Class33.string_106);
                              continue;
                            }
                            if (Class32.bool_39 && !this.bool_7)
                            {
                              if (Class34.smethod_3(this.string_6, this.string_0) < Class32.int_8)
                              {
                                if (this.bool_18)
                                {
                                  if (Class34.smethod_4(this.string_6, this.string_0) >= Class32.int_9)
                                  {
                                    this.method_10(Class33.string_6);
                                    continue;
                                  }
                                  break;
                                }
                                if (!this.bool_10)
                                {
                                  if (this.bool_8 && Class34.smethod_6(this.string_6, this.string_0) >= Class32.int_11)
                                  {
                                    this.method_10(Class33.string_8);
                                    continue;
                                  }
                                  break;
                                }
                                if (Class34.smethod_5(this.string_6, this.string_0) >= Class32.int_10)
                                {
                                  this.method_10(Class33.string_7);
                                  continue;
                                }
                                break;
                              }
                              this.method_10(Class33.string_5);
                              continue;
                            }
                            break;
                          }
                          break;
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                          if ((Class32.bool_86 ? (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0) : 0) != 0)
                          {
                            this.method_10(Class33.string_20);
                            continue;
                          }
                          break;
                        case 24:
                          if ((Class32.bool_86 ? (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0) : 0) != 0)
                          {
                            this.method_10(Class33.string_20);
                            this.method_7();
                            break;
                          }
                          break;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28732:
                    case 28752:
                    case 28753:
                    case 28833:
                      goto label_1149;
                    case 28742:
                      if (Class32.bool_94)
                      {
                        int num1 = (int) gclass3.method_8();
                        switch (gclass3.method_4())
                        {
                          case 1:
                            this.bool_28 = true;
                            this.bool_29 = false;
                            break;
                          case 3:
                            this.bool_28 = false;
                            this.bool_29 = true;
                            break;
                        }
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28747:
                      if (Class32.bool_94)
                      {
                        if (!this.bool_28)
                        {
                          if (this.bool_29)
                            this.bool_29 = false;
                        }
                        else
                          this.bool_28 = false;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28748:
                      uint num8 = (uint) gclass3.method_4();
                      uint num9 = (uint) gclass3.method_6();
                      if (Class32.bool_80 && !Class32.list_15.Contains(this.string_2))
                      {
                        if (GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',4,'" + (object) num8 + "'") == 1)
                        {
                          this.method_10(Class33.string_39);
                          continue;
                        }
                      }
                      if ((!Class32.bool_36 || Class32.list_15.Contains(this.string_2) ? 0 : (this.bool_7 ? 1 : 0)) != 0)
                      {
                        if (GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',5,'" + (object) num8 + "'") == 1)
                        {
                          this.method_10(Class33.string_40);
                          continue;
                        }
                      }
                      uint num10 = num9;
                      if (num10 <= 6636U)
                      {
                        if (num10 <= 4301U)
                        {
                          if (num10 != 2253U)
                          {
                            if (num10 != 2540U)
                            {
                              if (num10 == 4301U)
                              {
                                if ((Convert.ToInt32(Class32.int_32) <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                                {
                                  try
                                  {
                                    dateTime = DateTime.Now;
                                    timeSpan = dateTime.Subtract(this.dateTime_13);
                                    int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                                    if (int32 < Convert.ToInt32(Class32.int_32))
                                    {
                                      this.method_10(Class33.string_60.Replace("{Delay}", (Convert.ToInt32(Class32.int_32) - int32).ToString()));
                                      continue;
                                    }
                                  }
                                  catch
                                  {
                                  }
                                  this.dateTime_13 = DateTime.Now;
                                  goto label_583;
                                }
                                else
                                  goto label_583;
                              }
                              else
                                goto label_583;
                            }
                            else
                            {
                              if (Class32.bool_29 && !Class32.list_15.Contains(this.string_2) && (num8 == 31U && this.bool_8) && Class32.list_29.Contains(this.int_4))
                              {
                                this.method_10(Class33.string_61);
                                continue;
                              }
                              goto label_583;
                            }
                          }
                        }
                        else if (num10 != 4588U && num10 != 4589U)
                        {
                          if (num10 == 6636U)
                            goto label_546;
                          else
                            goto label_583;
                        }
                        this.bool_26 = true;
                        int num1 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',17,'" + (object) num8 + "'");
                        if ((!Class32.bool_28 || !this.bool_7 ? 0 : (num1 > 0 ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_106);
                          continue;
                        }
                        if ((!Class32.bool_38 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_8)
                        {
                          this.bool_11 = false;
                          if (Class32.list_29.Contains(this.int_4))
                          {
                            this.method_10(Class33.string_58);
                            continue;
                          }
                        }
                        if ((Convert.ToInt32(Class32.int_33) > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                        {
                          try
                          {
                            dateTime = DateTime.Now;
                            timeSpan = dateTime.Subtract(this.dateTime_14);
                            int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                            if (int32 < Convert.ToInt32(Class32.int_33))
                            {
                              this.method_10(Class33.string_59.Replace("{Delay}", (Convert.ToInt32(Class32.int_33) - int32).ToString()));
                              continue;
                            }
                          }
                          catch
                          {
                          }
                          this.dateTime_14 = DateTime.Now;
                        }
                        if (Convert.ToInt32(Class32.int_32) > 0)
                        {
                          if (!Class32.list_15.Contains(this.string_2))
                          {
                            try
                            {
                              dateTime = DateTime.Now;
                              timeSpan = dateTime.Subtract(this.dateTime_13);
                              int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                              if (int32 < Convert.ToInt32(Class32.int_32))
                              {
                                this.method_10(Class33.string_60.Replace("{Delay}", (Convert.ToInt32(Class32.int_32) - int32).ToString()));
                                continue;
                              }
                            }
                            catch
                            {
                            }
                            this.dateTime_13 = DateTime.Now;
                          }
                        }
                        if ((!Class32.bool_81 || Class32.list_15.Contains(this.string_2) ? 0 : (Class32.list_21.Contains(this.int_4) ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_105);
                          continue;
                        }
                        goto label_583;
                      }
                      else if (num10 <= 10733U)
                      {
                        if (num10 != 6637U)
                        {
                          if (num10 == 10732U || num10 == 10733U)
                          {
                            string string_0_3 = gclass3.method_14().ToString();
                            int num1 = 0;
                            if ((Class32.bool_125 ? 1 : (Class32.bool_134 ? 1 : 0)) != 0)
                              num1 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',18,'" + (object) num8 + "'");
                            if (Class32.dictionary_5.ContainsKey(this.string_2))
                            {
                              string str1 = Class32.dictionary_5[this.string_2];
                              dateTime = Convert.ToDateTime(DateTime.Now);
                              long num2 = long.Parse(dateTime.ToString("yyyyMMddHHmm"));
                              dateTime = Convert.ToDateTime(str1);
                              long num11 = long.Parse(dateTime.ToString("yyyyMMddHHmm"));
                              if (num2 <= num11)
                              {
                                this.method_10(Class33.string_48);
                                continue;
                              }
                            }
                            if (this.int_2 < Convert.ToInt32(Class32.int_17) && !Class32.list_15.Contains(this.string_2))
                            {
                              this.method_10(Class33.string_49.Replace("{Level}", Convert.ToInt32(Class32.int_17).ToString()));
                              continue;
                            }
                            if ((Convert.ToInt32(Class32.int_25) <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                            {
                              try
                              {
                                dateTime = DateTime.Now;
                                timeSpan = dateTime.Subtract(this.dateTime_3);
                                int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                                if (int32 < Convert.ToInt32(Class32.int_25))
                                {
                                  this.method_10(Class33.string_50.Replace("{Delay}", (Convert.ToInt32(Class32.int_25) - int32).ToString()));
                                  continue;
                                }
                              }
                              catch
                              {
                              }
                              this.dateTime_3 = DateTime.Now;
                            }
                            if ((!Class32.bool_113 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && Class34.smethod_9(string_0_3))
                            {
                              this.method_10(Class33.string_51);
                              continue;
                            }
                            if (Class32.bool_134 && Class32.list_78.Contains(num1))
                            {
                              if (Class32.bool_135)
                                Class18.smethod_2("[Global] [" + this.string_1 + "] " + string_0_3.Replace("ABEV#$13", ""));
                              else if (Class32.bool_140 && this.string_1 == Class32.string_58)
                                Class18.smethod_2("[Global] " + string_0_3.Replace("ABEV#$13", ""));
                            }
                            if ((!Class32.bool_86 ? 0 : (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) == 0)
                            {
                              if (Class32.bool_125)
                              {
                                try
                                {
                                  if (num1 == Class32.int_42)
                                  {
                                    if (!Class32.dictionary_12.ContainsKey(this.string_1))
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 52);
                                    }
                                    else
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Remove(this.string_1);
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 52);
                                    }
                                  }
                                  else if (num1 == Class32.int_43)
                                  {
                                    if (!Class32.dictionary_12.ContainsKey(this.string_1))
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 53);
                                    }
                                    else
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Remove(this.string_1);
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 53);
                                    }
                                  }
                                  else if (num1 == Class32.int_44)
                                  {
                                    if (!Class32.dictionary_12.ContainsKey(this.string_1))
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 54);
                                    }
                                    else
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Remove(this.string_1);
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 54);
                                    }
                                  }
                                  else if (num1 == Class32.int_45)
                                  {
                                    if (!Class32.dictionary_12.ContainsKey(this.string_1))
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 55);
                                    }
                                    else
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Remove(this.string_1);
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 55);
                                    }
                                  }
                                  else if (num1 == Class32.int_46)
                                  {
                                    if (!Class32.dictionary_12.ContainsKey(this.string_1))
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 56);
                                    }
                                    else
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Remove(this.string_1);
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 56);
                                    }
                                  }
                                  else if (num1 == Class32.int_47)
                                  {
                                    if (!Class32.dictionary_12.ContainsKey(this.string_1))
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 57);
                                    }
                                    else
                                    {
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Remove(this.string_1);
                                      lock (Class32.dictionary_12)
                                        Class32.dictionary_12.Add(this.string_1, (byte) 57);
                                    }
                                  }
                                  else if (Class32.dictionary_12.ContainsKey(this.string_1))
                                  {
                                    lock (Class32.dictionary_12)
                                      Class32.dictionary_12.Remove(this.string_1);
                                  }
                                }
                                catch
                                {
                                }
                              }
                              if (Class32.bool_107)
                                GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + string_0_3.Replace("'", "''") + "','None',4");
                              if (Class32.bool_131 && this.string_1 == Class32.string_52 && (!Class32.bool_132 ? 0 : (string_0_3.Contains("ABEV#$13") ? 1 : 0)) != 0)
                              {
                                string string54 = Class32.string_54;
                                byte byte_4 = string54 == "blue" ? (byte) 11 : (string54 == "green" ? (byte) 12 : (string54 == "pink" ? (byte) 13 : (byte) 0));
                                if (byte_4 > (byte) 0)
                                {
                                  this.method_14(string_0_3.Replace("ABEV#$13", ""), byte_4);
                                  continue;
                                }
                                continue;
                              }
                              goto label_583;
                            }
                            else
                            {
                              this.method_10(Class33.string_20);
                              continue;
                            }
                          }
                          else
                            goto label_583;
                        }
                      }
                      else if (num10 > 14061U)
                      {
                        switch (num10)
                        {
                          case 16620:
                            if (Class32.bool_90 && this.bool_15)
                            {
                              this.method_10(Class33.string_41);
                              continue;
                            }
                            if (Class32.bool_41 && this.bool_13)
                            {
                              this.method_10(Class33.string_42);
                              continue;
                            }
                            if (Class32.bool_34 && this.bool_7)
                            {
                              this.method_10(Class33.string_43);
                              continue;
                            }
                            goto label_583;
                          case 20205:
                            if (Class32.bool_32 && this.bool_7)
                            {
                              this.method_10(Class33.string_62);
                              continue;
                            }
                            goto label_583;
                          default:
                            goto label_583;
                        }
                      }
                      else if (num10 == 14060U || num10 == 14061U)
                      {
                        if ((!Class32.bool_22 ? 0 : (this.bool_7 ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_52);
                          continue;
                        }
                        if (this.int_2 >= Convert.ToInt32(Class32.int_21))
                        {
                          if (Convert.ToInt32(Class32.int_30) > 0)
                          {
                            try
                            {
                              dateTime = DateTime.Now;
                              timeSpan = dateTime.Subtract(this.dateTime_6);
                              int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                              if (int32 < Convert.ToInt32(Class32.int_30))
                              {
                                this.method_10(Class33.string_54.Replace("{Delay}", (Convert.ToInt32(Class32.int_30) - int32).ToString()));
                                continue;
                              }
                            }
                            catch
                            {
                            }
                            this.dateTime_6 = DateTime.Now;
                          }
                          if (Class32.bool_44 && this.bool_13)
                          {
                            this.method_10(Class33.string_55);
                            continue;
                          }
                          if (Class32.bool_45 && this.bool_5)
                          {
                            this.method_10(Class33.string_56);
                            continue;
                          }
                          if (Class32.bool_46 && this.bool_6)
                          {
                            this.method_10(Class33.string_57);
                            continue;
                          }
                          goto label_583;
                        }
                        else
                        {
                          this.method_10(Class33.string_53.Replace("{Level}", Convert.ToInt32(Class32.int_21).ToString()));
                          continue;
                        }
                      }
                      else
                        goto label_583;
label_546:
                      if (Class32.bool_112)
                      {
                        uint num1 = (uint) gclass3.method_4();
                        if ((num1 == 2U ? 1 : (num1 == 3U ? 1 : 0)) != 0 && GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',7,0") == 1)
                        {
                          this.method_10(Class33.string_44);
                          continue;
                        }
                      }
                      if ((!Class32.bool_26 || !this.bool_7 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) == 0)
                      {
                        if ((this.int_2 >= Convert.ToInt32(Class32.int_20) ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_46.Replace("{Level}", Convert.ToInt32(Class32.int_20).ToString()));
                          continue;
                        }
                        if ((Convert.ToInt32(Class32.int_26) <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          try
                          {
                            dateTime = DateTime.Now;
                            timeSpan = dateTime.Subtract(this.dateTime_4);
                            int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                            if (int32 < Convert.ToInt32(Class32.int_26))
                            {
                              this.method_10(Class33.string_47.Replace("{Delay}", (Convert.ToInt32(Class32.int_26) - int32).ToString()));
                              continue;
                            }
                          }
                          catch
                          {
                          }
                          this.dateTime_4 = DateTime.Now;
                        }
                      }
                      else
                      {
                        this.method_10(Class33.string_45);
                        continue;
                      }
label_583:
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28762:
                      int num12 = (int) gclass3.method_8();
                      if (gclass3.method_4() == (byte) 2)
                      {
                        this.uint_0 = gclass3.method_8();
                        if ((Class32.bool_23 || Class32.bool_24 ? 1 : (Class32.bool_25 ? 1 : 0)) != 0 && (Class32.list_23.Contains(this.uint_0) || Class32.list_25.Contains(this.uint_0) ? 1 : (Class32.list_24.Contains(this.uint_0) ? 1 : 0)) != 0 && ((!Class32.list_23.Contains(this.uint_0) || !this.bool_18 || this.bool_10 ? 0 : (!this.bool_8 ? 1 : 0)) == 0 && (!Class32.list_25.Contains(this.uint_0) || this.bool_18 || !this.bool_10 ? 0 : (!this.bool_8 ? 1 : 0)) == 0) && (!Class32.list_24.Contains(this.uint_0) || this.bool_18 || this.bool_10 ? 0 : (this.bool_8 ? 1 : 0)) == 0)
                        {
                          this.method_10(Class33.string_44);
                          continue;
                        }
                        if ((!Class32.bool_71 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && ((this.uint_0 == 166U ? 1 : (this.uint_0 == 167U ? 1 : 0)) != 0 && this.bool_19))
                        {
                          this.method_10(Class33.string_66);
                          continue;
                        }
                        if ((Class32.bool_129 ? (Class32.dictionary_9.ContainsKey(this.uint_0) ? 1 : 0) : 0) != 0 && Class32.dictionary_9[this.uint_0] == 0)
                        {
                          this.method_10(Class33.string_122);
                          continue;
                        }
                        if ((!Class32.bool_48 || this.bool_13 ? 0 : (Class32.list_43.Contains(this.uint_0) ? 1 : 0)) != 0 && (Class34.smethod_2(this.string_6, this.string_0) >= Class32.int_12 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                        {
                          this.method_10(Class33.string_10);
                          continue;
                        }
                      }
                      try
                      {
                        dateTime = DateTime.Now;
                        timeSpan = dateTime.Subtract(this.dateTime_8);
                        int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                        if ((int32 >= Convert.ToInt32(Class32.int_31) ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_67.Replace("{Delay}", (Convert.ToInt32(Class32.int_31) - int32).ToString()));
                          continue;
                        }
                      }
                      catch
                      {
                      }
                      this.dateTime_8 = DateTime.Now;
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28765:
                      if ((!Class32.bool_48 ? 0 : (!this.bool_13 ? 1 : 0)) != 0 && (Class34.smethod_2(this.string_6, this.string_0) >= Class32.int_12 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                      {
                        this.method_10(Class33.string_10);
                        continue;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28766:
                      int num13 = (int) gclass3.method_8();
                      switch ((uint) gclass3.method_4())
                      {
                        case 1:
                          if (Class32.bool_116)
                          {
                            this.method_10("You can not change tax.");
                            continue;
                          }
                          break;
                        case 26:
                          int num14 = (int) gclass3.method_8();
                          string str7 = gclass3.method_14();
                          if ((str7.Contains("'") || str7.Contains("\"") ? 1 : (str7.Contains("-") ? 1 : 0)) != 0)
                          {
                            if (Class32.bool_121)
                              Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[FW EXPLOITING]", "Magenta");
                            this.method_10("This Exploit is disabled.");
                            continue;
                          }
                          break;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28768:
                    case 28781:
                      break;
                    case 28777:
                    case 28778:
                      this.bool_14 = true;
                      int num15 = (int) gclass3.method_8();
                      int num16 = (int) gclass3.method_8();
                      int num17 = (int) gclass3.method_4();
                      int num18 = (int) gclass3.method_4();
                      int num19 = (int) gclass3.method_4();
                      int num20 = (int) gclass3.method_4();
                      if ((!Class34.smethod_9(gclass3.method_14().ToString().ToLower()) || !Class32.bool_113 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_91);
                        continue;
                      }
                      if ((Class32.bool_82 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && Class32.list_18.Contains(this.int_4))
                      {
                        this.method_10(Class33.string_92);
                        continue;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28788:
                      try
                      {
                        if (gclass3.method_4() == (byte) 1)
                        {
                          switch (gclass3.method_4())
                          {
                            case 2:
                              if (Class32.bool_37)
                              {
                                if (!Class32.list_15.Contains(this.string_2))
                                {
                                  if (Class32.list_29.Contains(this.int_4))
                                  {
                                    if ((!this.bool_8 ? 0 : (!this.bool_11 ? 1 : 0)) != 0)
                                    {
                                      this.method_10(Class33.string_81);
                                      continue;
                                    }
                                    break;
                                  }
                                  break;
                                }
                                break;
                              }
                              break;
                            case 3:
                              if ((!Class32.bool_21 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_7)
                              {
                                this.method_10(Class33.string_82);
                                continue;
                              }
                              if ((Class32.bool_43 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && this.bool_13)
                              {
                                this.method_10(Class33.string_84);
                                continue;
                              }
                              if ((!Class32.bool_53 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_5)
                              {
                                this.method_10(Class33.string_83);
                                continue;
                              }
                              if ((!Class32.bool_59 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_6)
                              {
                                this.method_10(Class33.string_119);
                                continue;
                              }
                              if ((Class32.bool_63 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                              {
                                if (this.bool_19)
                                {
                                  this.method_10(Class33.string_85);
                                  continue;
                                }
                                break;
                              }
                              break;
                            case 4:
                              ushort num1 = gclass3.method_6();
                              if ((Convert.ToInt32(Class32.int_34) <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                              {
                                if ((!Class32.list_38.Contains(num1) || !this.bool_4 ? 0 : (!this.bool_15 ? 1 : 0)) != 0)
                                {
                                  try
                                  {
                                    dateTime = DateTime.Now;
                                    timeSpan = dateTime.Subtract(this.dateTime_12);
                                    if (Convert.ToInt32(timeSpan.TotalSeconds) < Convert.ToInt32(Class32.int_34))
                                      continue;
                                  }
                                  catch
                                  {
                                  }
                                  this.dateTime_12 = DateTime.Now;
                                }
                              }
                              if (Class32.list_34.Contains(num1))
                              {
                                if (!Class32.list_15.Contains(this.string_2))
                                {
                                  this.method_10(Class33.string_86);
                                  continue;
                                }
                                break;
                              }
                              if ((!Class32.bool_42 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && (Class32.list_31.Contains(num1) ? (this.bool_13 ? 1 : 0) : 0) != 0)
                              {
                                this.method_10(Class33.string_90);
                                continue;
                              }
                              if ((!Class32.bool_50 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && (!Class32.list_32.Contains(num1) ? 0 : (this.bool_5 ? 1 : 0)) != 0)
                              {
                                this.method_10(Class33.string_87);
                                continue;
                              }
                              if ((!Class32.bool_56 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && (Class32.list_33.Contains(num1) ? (this.bool_6 ? 1 : 0) : 0) != 0)
                              {
                                this.method_10(Class33.string_120);
                                continue;
                              }
                              if ((!Class32.bool_35 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && (Class32.list_26.Contains(num1) ? (this.bool_7 ? 1 : 0) : 0) != 0)
                              {
                                this.method_10(Class33.string_88);
                                continue;
                              }
                              if ((!Class32.bool_77 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                              {
                                try
                                {
                                  if ((uint) Class32.dataTable_0.Select("RegionID = " + (object) this.int_4 + " AND RefSkillID = " + (object) num1 ?? "").Length > 0U)
                                  {
                                    this.method_10(Class33.string_89);
                                    continue;
                                  }
                                  break;
                                }
                                catch
                                {
                                  break;
                                }
                              }
                              else
                                break;
                          }
                        }
                      }
                      catch
                      {
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28801:
                      this.bool_16 = true;
                      uint num21 = gclass3.method_8();
                      if ((this.int_2 < Convert.ToInt32(Class32.int_22) ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                      {
                        this.method_10(Class33.string_16.Replace("{Level}", Convert.ToInt32(Class32.int_22).ToString()));
                        continue;
                      }
                      if ((Convert.ToInt32(Class32.int_23) > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                      {
                        try
                        {
                          dateTime = DateTime.Now;
                          timeSpan = dateTime.Subtract(this.dateTime_2);
                          int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                          if (int32 < Convert.ToInt32(Class32.int_23))
                          {
                            this.method_10(Class33.string_17.Replace("{Delay}", (Convert.ToInt32(Class32.int_23) - int32).ToString()));
                            continue;
                          }
                        }
                        catch
                        {
                        }
                        this.dateTime_2 = DateTime.Now;
                      }
                      if ((!Class32.bool_27 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                      {
                        if (!this.bool_7)
                        {
                          if (Class32.list_13.Contains(num21))
                          {
                            this.method_10(Class33.string_18);
                            continue;
                          }
                        }
                        else
                        {
                          this.method_10(Class33.string_18);
                          continue;
                        }
                      }
                      if (Class32.bool_84 && (!this.bool_15 ? (Class32.list_12.Contains(num21) ? 1 : 0) : 1) != 0)
                      {
                        this.method_10(Class33.string_19);
                        continue;
                      }
                      if (Class32.bool_86)
                      {
                        if (Class32.dictionary_4.ContainsKey(this.string_1))
                        {
                          this.method_10(Class33.string_20);
                          continue;
                        }
                        if (Class32.list_28.Contains(num21))
                        {
                          this.method_10(Class33.string_20);
                          continue;
                        }
                      }
                      if ((!Class32.bool_51 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_5)
                      {
                        this.method_10(Class33.string_21);
                        continue;
                      }
                      if ((Class32.bool_57 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && this.bool_6)
                      {
                        this.method_10(Class33.string_116);
                        continue;
                      }
                      if (Class32.bool_85 && !Class32.list_29.Contains(this.int_4))
                      {
                        this.method_10(Class33.string_22);
                        continue;
                      }
                      if (Class32.bool_64 && !Class32.list_15.Contains(this.string_2) && this.bool_19)
                      {
                        this.method_10(Class33.string_23);
                        continue;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28834:
                      if ((!Class32.bool_86 ? 0 : (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) == 0)
                      {
                        int num1 = (int) gclass3.method_8();
                        if (gclass3.method_4() == (byte) 1)
                        {
                          this.gclass4_1.method_15(gclass3);
                          this.method_5(true);
                          continue;
                        }
                        this.method_10("You have been temporarily blocked from performing this action.");
                        continue;
                      }
                      this.method_10(Class33.string_20);
                      continue;
                    case 28839:
                      if ((Class32.bool_78 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && Class32.list_17.Contains(this.int_4))
                      {
                        this.method_10(Class33.string_33);
                        continue;
                      }
                      this.dateTime_10 = DateTime.Now;
                      if (gclass3.method_4() != (byte) 1)
                      {
                        this.method_7();
                        continue;
                      }
                      if ((this.int_2 < Convert.ToInt32(Class32.int_19) ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) == 0)
                      {
                        if ((Convert.ToInt32(Class32.int_29) > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                        {
                          try
                          {
                            dateTime = DateTime.Now;
                            timeSpan = dateTime.Subtract(this.dateTime_7);
                            int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                            if (int32 < Convert.ToInt32(Class32.int_29))
                            {
                              this.method_10(Class33.string_32.Replace("{Delay}", (Convert.ToInt32(Class32.int_29) - int32).ToString()));
                              continue;
                            }
                          }
                          catch
                          {
                          }
                          this.dateTime_7 = DateTime.Now;
                        }
                        if (Class32.bool_33 && this.bool_7)
                        {
                          this.method_10(Class33.string_34);
                          continue;
                        }
                        if (Class32.bool_40 && this.bool_13)
                        {
                          this.method_10(Class33.string_35);
                          continue;
                        }
                        if (Class32.bool_89 && this.bool_15)
                        {
                          this.method_10(Class33.string_36);
                          continue;
                        }
                        if (Class32.bool_49 && this.bool_5)
                        {
                          this.method_10(Class33.string_37);
                          continue;
                        }
                        if (Class32.bool_55 && this.bool_6)
                        {
                          this.method_10(Class33.string_118);
                          continue;
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      }
                      this.method_10(Class33.string_31.Replace("{Level}", Convert.ToInt32(Class32.int_19).ToString()));
                      continue;
                    default:
                      goto label_1145;
                  }
                }
                else if (uint160 <= (ushort) 13439)
                {
                  if (uint160 > (ushort) 12306)
                  {
                    switch (uint160)
                    {
                      case 12398:
                        break;
                      case 12416:
                        if (Class32.bool_86 && Class32.dictionary_4.ContainsKey(this.string_1))
                        {
                          this.method_10(Class33.string_20);
                          continue;
                        }
                        if ((!Class32.bool_82 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && Class32.list_18.Contains(this.int_4))
                        {
                          this.method_10(Class33.string_107);
                          continue;
                        }
                        if ((Class32.bool_70 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && this.bool_19)
                        {
                          this.method_10(Class33.string_108);
                          continue;
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      case 13439:
                        goto label_968;
                      default:
                        goto label_1145;
                    }
                  }
                  else
                  {
                    switch (uint160)
                    {
                      case 8193:
                        this.method_3();
                        this.method_5(false);
                        continue;
                      case 8194:
                        this.int_7 = gclass3.method_0().Length;
                        if (this.int_7 > 0)
                        {
                          Class18.smethod_0("[" + this.string_0 + "] 0x2002 size problem.", "Magenta");
                          continue;
                        }
                        if ((Class32.bool_7 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                        {
                          try
                          {
                            if (this.bool_3)
                            {
                              try
                              {
                                if (Class32.bool_7)
                                {
                                  if (!this.bool_5)
                                  {
                                    if (!this.bool_13)
                                    {
                                      if (!this.bool_6)
                                      {
                                        dateTime = DateTime.Now;
                                        timeSpan = dateTime.Subtract(this.dateTime_5);
                                        this.bool_1 = Convert.ToInt32(timeSpan.TotalSeconds) > Convert.ToInt32(Class32.int_3) * 60;
                                      }
                                    }
                                  }
                                }
                              }
                              catch
                              {
                              }
                              if (this.bool_1)
                              {
                                if (Class32.string_23 == "Symbol")
                                {
                                  GClass3 gclass3_1 = new GClass3((ushort) 29698);
                                  gclass3_1.method_31((byte) 2);
                                  this.gclass4_1.method_15(gclass3_1);
                                  this.method_5(true);
                                  this.bool_2 = true;
                                }
                                else
                                {
                                  this.method_7();
                                  continue;
                                }
                              }
                            }
                          }
                          catch
                          {
                          }
                        }
                        if ((!Class32.bool_7 || !this.bool_5 || !Class32.bool_9 ? 0 : (this.bool_3 ? 1 : 0)) != 0)
                        {
                          dateTime = DateTime.Now;
                          timeSpan = dateTime.Subtract(this.dateTime_16);
                          this.bool_20 = Convert.ToInt32(timeSpan.TotalSeconds) > Convert.ToInt32(Class32.int_5) * 60;
                          if (this.bool_20 && Class32.bool_9)
                          {
                            this.method_7();
                            continue;
                          }
                        }
                        if ((!Class32.bool_7 || !this.bool_13 || !Class32.bool_8 ? 0 : (this.bool_3 ? 1 : 0)) != 0)
                        {
                          dateTime = DateTime.Now;
                          timeSpan = dateTime.Subtract(this.dateTime_17);
                          this.bool_21 = Convert.ToInt32(timeSpan.TotalSeconds) > Convert.ToInt32(Class32.int_4) * 60;
                          if (this.bool_21 && Class32.bool_8)
                          {
                            this.method_7();
                            continue;
                          }
                        }
                        if (Class32.bool_7 && this.bool_6 && (Class32.bool_114 && this.bool_3))
                        {
                          dateTime = DateTime.Now;
                          timeSpan = dateTime.Subtract(this.dateTime_18);
                          this.bool_22 = Convert.ToInt32(timeSpan.TotalSeconds) > Convert.ToInt32(Class32.string_46) * 60;
                          if (this.bool_22 && Class32.bool_114)
                          {
                            this.method_7();
                            continue;
                          }
                        }
                        if ((!Class32.bool_81 || Class32.list_15.Contains(this.string_2) || (!Class32.list_21.Contains(this.int_4) || this.bool_15) ? 0 : (!this.bool_7 ? 1 : 0)) != 0)
                        {
                          try
                          {
                            dateTime = DateTime.Now;
                            timeSpan = dateTime.Subtract(this.dateTime_15);
                            if (Convert.ToInt32(timeSpan.TotalSeconds) >= 60)
                            {
                              this.dateTime_15 = DateTime.Now;
                              GClass3 gclass3_1 = new GClass3((ushort) 29974);
                              gclass3_1.method_31((byte) 5);
                              this.gclass4_1.method_15(gclass3_1);
                              this.method_5(true);
                              continue;
                            }
                          }
                          catch
                          {
                          }
                        }
                        if (Class32.bool_4)
                        {
                          dateTime = DateTime.Now;
                          timeSpan = dateTime.Subtract(this.dateTime_19);
                          if (Convert.ToInt32(timeSpan.TotalSeconds) >= Convert.ToInt32(Class32.int_2) * 60)
                            this.method_0();
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      case 12306:
                        if ((!Class32.bool_39 || !this.bool_7 ? 0 : (this.bool_30 ? 1 : 0)) != 0)
                        {
                          if (!this.bool_7)
                          {
                            if (!this.bool_18)
                            {
                              if (!this.bool_10)
                              {
                                if (this.bool_8)
                                {
                                  this.method_10(Class33.string_8);
                                  this.method_7();
                                  return;
                                }
                              }
                              else
                              {
                                this.method_10(Class33.string_7);
                                this.method_7();
                                return;
                              }
                            }
                            else
                            {
                              this.method_10(Class33.string_6);
                              this.method_7();
                              return;
                            }
                          }
                          else
                          {
                            this.method_10(Class33.string_5);
                            this.method_7();
                            return;
                          }
                        }
                        if (!this.bool_17)
                        {
                          this.bool_17 = true;
                          try
                          {
                            if (Class32.bool_122)
                            {
                              if (!string.IsNullOrEmpty(this.string_1))
                                GClass7.smethod_0("EXEC [_PlayersLogging] '" + this.string_2 + "','" + this.string_0 + "','" + this.string_6 + "','" + this.string_1 + "','ONLINE'");
                            }
                          }
                          catch
                          {
                            this.method_10("Problem occured with your login, try again later.");
                            Class18.smethod_0("Problem at 0x3012, user: " + this.string_2 + " disconnected", "Red");
                            this.method_7();
                            continue;
                          }
                          if ((uint) gclass3.method_0().Length <= 0U)
                          {
                            if (Class32.bool_74)
                            {
                              this.method_10(Class32.string_32.Replace("{playername}", this.string_1.ToString()).Replace("{servername}", Class32.string_33.ToString()));
                              this.bool_17 = true;
                            }
                            if (Class32.bool_95)
                            {
                              this.method_9(Class33.string_0.Replace("{Helper}", Class32.string_43.ToString()));
                              this.method_9(Class33.string_1.Replace("{System}", Class32.string_42.ToString()));
                            }
                          }
                          else
                          {
                            Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                            this.method_7();
                            continue;
                          }
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      default:
                        goto label_1145;
                    }
                  }
                }
                else if (uint160 <= (ushort) 24835)
                {
                  if (uint160 != (ushort) 13481)
                  {
                    if (uint160 != (ushort) 20480)
                    {
                      if (uint160 == (ushort) 24835)
                      {
                        int num1 = (int) gclass3.method_8();
                        this.string_2 = gclass3.method_14().ToLower();
                        this.string_3 = gclass3.method_14();
                        int num2 = (int) gclass3.method_4();
                        if ((this.string_2.Contains("'") ? 1 : (this.string_2.Contains("\"") ? 1 : 0)) != 0)
                        {
                          Class31.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] attempted to SQL inject StrUserID at 0x6103", this.string_0, this.string_2);
                          this.method_7();
                          continue;
                        }
                        if (Class32.bool_98 && !Class32.list_15.Contains(this.string_2) && Class34.smethod_7(this.string_0) > Convert.ToInt32(Class32.string_44))
                        {
                          this.method_10(Class33.string_4);
                          this.method_7();
                          continue;
                        }
                        if (Class32.bool_99 && !Class32.list_15.Contains(this.string_2) && this.string_0 != Class32.string_15)
                        {
                          this.string_6 = Class32.dictionary_8[this.string_2];
                          if (Class34.smethod_0(this.string_6) >= Convert.ToInt32(Class32.string_45))
                          {
                            this.method_10(Class33.string_3);
                            this.method_7();
                            continue;
                          }
                          lock (Class32.list_44)
                            Class32.list_44.Add(this.string_6);
                        }
                        if (!this.bool_25)
                        {
                          ++Class32.int_0;
                          Console.Title = "EverNow [Filter] : ONLINE PLAYER(S) [" + (object) Class32.int_0 + "]";
                          Class18.smethod_1("ONLINE," + (object) Class32.int_0 ?? "");
                          this.bool_25 = true;
                        }
                        if (Class32.list_42.Contains(this.string_2))
                        {
                          this.bool_27 = true;
                          ++Class32.int_38;
                          Class18.smethod_1("CLIENTLESS," + (object) Class32.int_38 ?? "");
                        }
                        else
                          this.bool_27 = false;
                        if (Class32.bool_61)
                        {
                          this.bool_19 = Class32.list_54.Contains(this.string_2);
                          if (this.bool_19 && !this.bool_27)
                          {
                            ++Class32.int_37;
                            Class18.smethod_1("THIRDPART," + (object) Class32.int_37 ?? "");
                          }
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      }
                      goto label_1145;
                    }
                    else
                      goto label_973;
                  }
                  else
                  {
                    if ((!Class32.bool_86 ? 0 : (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) == 0)
                    {
                      string lower2 = gclass3.method_14().ToString().ToLower();
                      if (Class32.bool_19)
                      {
                        this.method_10(Class33.string_38);
                        continue;
                      }
                      if (lower2.Contains("avatar"))
                      {
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      }
                      if (Class32.bool_121)
                        Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] try to use [Avatar Exploit]", "Magenta");
                      this.method_10(Class33.string_38);
                      continue;
                    }
                    this.method_10(Class33.string_20);
                    continue;
                  }
                }
                else if (uint160 > (ushort) 28677)
                {
                  switch (uint160)
                  {
                    case 28679:
                      switch (gclass3.method_4())
                      {
                        case 1:
                          try
                          {
                            gclass3.method_14();
                            int num1 = (int) gclass3.method_8();
                            int num2 = (int) gclass3.method_4();
                            int num11 = (int) gclass3.method_8();
                            int num22 = (int) gclass3.method_8();
                            int num23 = (int) gclass3.method_8();
                            int num24 = (int) gclass3.method_8();
                            break;
                          }
                          catch
                          {
                            if (Class32.bool_121)
                              Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                            this.method_7();
                            break;
                          }
                        case 2:
                          if (gclass3.method_0().Length > 1)
                          {
                            if (Class32.bool_121)
                              Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                            this.method_7();
                            break;
                          }
                          break;
                        case 3:
                          int length1 = gclass3.method_14().Length;
                          if (gclass3.method_0().Length - length1 != 3)
                          {
                            if (Class32.bool_121)
                              Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                            this.method_7();
                            break;
                          }
                          break;
                        case 4:
                          int length2 = gclass3.method_14().Length;
                          if (gclass3.method_0().Length - length2 != 3)
                          {
                            if (Class32.bool_121)
                              Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                            this.method_7();
                            break;
                          }
                          break;
                        case 5:
                          int length3 = gclass3.method_14().Length;
                          if (gclass3.method_0().Length - length3 != 3)
                          {
                            if (Class32.bool_121)
                              Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                            this.method_7();
                            break;
                          }
                          break;
                        default:
                          this.method_7();
                          return;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28688:
                      uint num25 = (uint) gclass3.method_4();
                      if (Class32.bool_101)
                      {
                        if ((Class32.list_36.Contains(this.string_2) || Class32.list_15.Contains(this.string_2) ? 0 : (this.string_0 != Class32.string_15 ? 1 : 0)) == 0)
                        {
                          if ((Class32.list_35.Contains(this.string_2) ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                          {
                            switch (num25)
                            {
                              case 2:
                                if (!Class32.list_50.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [GoTown] GM commands.");
                                  continue;
                                }
                                break;
                              case 3:
                                if (!Class32.list_51.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [UserToTown] GM commands.");
                                  continue;
                                }
                                break;
                              case 4:
                                if (!Class32.list_57.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [worldstatus] GM commands.");
                                  continue;
                                }
                                break;
                              case 6:
                                if (!Class32.list_46.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [LoadMonster] GM commands.");
                                  continue;
                                }
                                break;
                              case 7:
                                if (!Class32.list_45.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [LoadItem] GM commands.");
                                  continue;
                                }
                                break;
                              case 8:
                                if (!Class32.list_49.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [MoveTouser] GM commands.");
                                  continue;
                                }
                                break;
                              case 13:
                                if (!Class32.list_47.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [BanUser] GM commands.");
                                  continue;
                                }
                                break;
                              case 14:
                                if (!Class32.list_58.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [Invisible] GM commands.");
                                  continue;
                                }
                                break;
                              case 15:
                                if (!Class32.list_59.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [Invincible] GM commands.");
                                  continue;
                                }
                                break;
                              case 16:
                                if (!Class32.list_56.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [wp] GM commands.");
                                  continue;
                                }
                                break;
                              case 17:
                                if (Class32.list_48.Contains(this.string_2))
                                {
                                  int num1 = (int) gclass3.method_4();
                                  if (this.method_13(gclass3.method_14().ToString().Replace(" ", "")))
                                  {
                                    this.method_10("You cant recall player under job mood!");
                                    continue;
                                  }
                                  break;
                                }
                                this.method_10("You are not allowed to use [Recalluser] GM commands.");
                                continue;
                              case 20:
                                if (!Class32.list_52.Contains(this.string_2))
                                {
                                  this.method_10("You are not allowed to use [Mobkill] GM commands.");
                                  continue;
                                }
                                break;
                              default:
                                this.method_10("You are not allowed to use this GM commands.");
                                continue;
                            }
                          }
                        }
                        else
                        {
                          Class31.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] attempted to use GM Console.", this.string_0, this.string_2);
                          this.method_7();
                          continue;
                        }
                      }
                      if ((!Class32.bool_102 ? 0 : (num25 == 20U ? 1 : 0)) != 0)
                      {
                        uint uint_0 = gclass3.method_8();
                        GClass3 gclass3_1 = new GClass3((ushort) 28688);
                        gclass3_1.method_33((ushort) 20);
                        gclass3_1.method_35(uint_0);
                        gclass3_1.method_31((byte) 1);
                        this.gclass4_1.method_15(gclass3_1);
                        this.method_5(true);
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    default:
                      goto label_1145;
                  }
                }
                else
                {
                  switch (uint160)
                  {
                    case 28673:
                      this.int_7 = gclass3.method_0().Length;
                      this.bool_23 = true;
                      if (!this.bool_9)
                      {
                        try
                        {
                          if (this.int_7 > 2)
                          {
                            this.string_1 = gclass3.method_14();
                            if (!this.string_1.Contains("'") && !this.string_1.Contains("\""))
                            {
                              this.string_1.Replace("%", "".ToString());
                              this.bool_9 = true;
                              if (Class32.bool_4)
                                this.dateTime_19 = DateTime.Now;
                            }
                            else
                            {
                              Class31.smethod_0("IP: [" + this.string_0 + "] UserID: [" + Class17.smethod_1(this.string_2) + "] attempted to SQL inject charname from IP: " + this.string_0 ?? "", this.string_0, Class17.smethod_1(this.string_2));
                              this.method_7();
                              continue;
                            }
                          }
                        }
                        catch
                        {
                        }
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 28677:
                      this.int_7 = gclass3.method_0().Length;
                      if (this.bool_23)
                      {
                        if (this.int_7 == 1)
                        {
                          switch ((uint) gclass3.method_4())
                          {
                            case 1:
                              try
                              {
                                if ((Convert.ToInt32(Class32.int_28) <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                                {
                                  dateTime = DateTime.Now;
                                  timeSpan = dateTime.Subtract(this.dateTime_9);
                                  int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                                  if (int32 < Convert.ToInt32(Class32.int_28))
                                  {
                                    this.method_10(Class33.string_76.Replace("{Delay}", (Convert.ToInt32(Class32.int_28) - int32).ToString()));
                                    continue;
                                  }
                                }
                              }
                              catch
                              {
                              }
                              this.dateTime_9 = DateTime.Now;
                              break;
                            case 2:
                              try
                              {
                                if ((Convert.ToInt32(Class32.int_27) > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                                {
                                  dateTime = DateTime.Now;
                                  timeSpan = dateTime.Subtract(this.dateTime_11);
                                  int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                                  if (int32 < Convert.ToInt32(Class32.int_27))
                                  {
                                    this.method_10(Class33.string_77.Replace("{Delay}", (Convert.ToInt32(Class32.int_27) - int32).ToString()));
                                    continue;
                                  }
                                }
                              }
                              catch
                              {
                              }
                              this.dateTime_11 = DateTime.Now;
                              if ((Class32.bool_91 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                              {
                                this.method_10(Class33.string_78);
                                continue;
                              }
                              break;
                            default:
                              Class31.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Character list packet exploit detected, Disconnect..", this.string_0, this.string_2);
                              this.method_7();
                              break;
                          }
                          this.gclass4_1.method_15(gclass3);
                          this.method_5(true);
                          continue;
                        }
                        Class31.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Character list packet exploit detected, Disconnect..", this.string_0, this.string_2);
                        this.method_7();
                        continue;
                      }
                      this.method_7();
                      continue;
                    default:
                      goto label_1145;
                  }
                }
                if ((Class32.bool_82 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && Class32.list_18.Contains(this.int_4))
                {
                  this.method_10(Class33.string_107);
                  continue;
                }
                if ((!Class32.bool_70 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_19)
                {
                  this.method_10(Class33.string_108);
                  continue;
                }
                this.gclass4_1.method_15(gclass3);
                this.method_5(true);
                continue;
              }
              if (uint160 > (ushort) 29016)
              {
                if (uint160 <= (ushort) 29808)
                {
                  if (uint160 > (ushort) 29266)
                  {
                    if (uint160 > (ushort) 29449)
                    {
                      switch (uint160)
                      {
                        case 29698:
                          if (!Class32.bool_7 || gclass3.method_4() != (byte) 2 || !(Class32.string_23 == "Symbol"))
                          {
                            this.gclass4_1.method_15(gclass3);
                            this.method_5(true);
                            continue;
                          }
                          continue;
                        case 29808:
                          if ((Class32.bool_87 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                          {
                            this.method_10(Class33.string_103);
                            continue;
                          }
                          this.gclass4_1.method_15(gclass3);
                          this.method_5(true);
                          continue;
                        default:
                          goto label_1145;
                      }
                    }
                    else
                    {
                      switch (uint160)
                      {
                        case 29272:
                          goto label_1149;
                        case 29449:
                          if ((Class32.bool_113 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                          {
                            gclass3.method_14();
                            if (Class34.smethod_9(gclass3.method_14().ToString().ToLower()))
                            {
                              this.method_10(Class33.string_93);
                              continue;
                            }
                          }
                          this.gclass4_1.method_15(gclass3);
                          this.method_5(true);
                          continue;
                        default:
                          goto label_1145;
                      }
                    }
                  }
                  else
                  {
                    switch (uint160)
                    {
                      case 29023:
                        int num26 = (int) gclass3.method_8();
                        uint num27 = gclass3.method_8();
                        if ((Class32.bool_112 ? (num27 == 3795U ? 1 : 0) : 0) != 0)
                        {
                          uint num1 = (uint) gclass3.method_4();
                          if ((num1 == 2U ? 1 : (num1 == 3U ? 1 : 0)) != 0 && GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',7,0") == 1)
                          {
                            this.method_10(Class33.string_44);
                            continue;
                          }
                        }
                        if ((Class32.bool_80 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && Class32.list_20.Contains(this.int_4))
                        {
                          this.method_10(Class33.string_113);
                          continue;
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      case 29264:
                      case 29266:
                        goto label_1149;
                      default:
                        goto label_1145;
                    }
                  }
                }
                else if (uint160 <= (ushort) 29874)
                {
                  if (uint160 != (ushort) 29810 && uint160 != (ushort) 29822)
                  {
                    if (uint160 == (ushort) 29874)
                    {
                      if ((!Class32.bool_60 || Class32.list_15.Contains(this.string_2) ? 0 : (Class32.int_49 == 1 ? 1 : 0)) != 0)
                      {
                        if (!Class32.list_63.Contains(this.string_1))
                        {
                          if (Class32.string_30 == "IP")
                          {
                            lock (Class32.list_2)
                              Class32.list_2.Add(this.string_0);
                          }
                          else
                          {
                            lock (Class32.list_2)
                              Class32.list_2.Add(this.string_6);
                          }
                          lock (Class32.list_63)
                            Class32.list_63.Add(this.string_1);
                        }
                        else if (Class32.list_63.Contains(this.string_1))
                        {
                          if (Class32.string_30 == "IP")
                          {
                            lock (Class32.list_2)
                              Class32.list_2.Remove(this.string_0);
                          }
                          else
                          {
                            lock (Class32.list_2)
                              Class32.list_2.Remove(this.string_6);
                          }
                          lock (Class32.list_63)
                            Class32.list_63.Remove(this.string_1);
                        }
                        if ((Class34.smethod_8(this.string_6, this.string_0) <= Class32.int_14 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_115);
                          continue;
                        }
                      }
                      if ((!Class32.bool_128 ? 0 : (this.bool_27 ? 1 : 0)) == 0)
                      {
                        if ((this.int_2 < Class32.int_16 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) == 0)
                        {
                          this.gclass4_1.method_15(gclass3);
                          this.method_5(true);
                          continue;
                        }
                        this.method_10(Class33.string_63);
                        continue;
                      }
                      continue;
                    }
                    goto label_1145;
                  }
                }
                else if (uint160 > (ushort) 29966)
                {
                  switch (uint160)
                  {
                    case 29974:
                      if (gclass3.method_4() == (byte) 0 && (!Class32.bool_81 || Class32.list_15.Contains(this.string_2) || !Class32.list_21.Contains(this.int_4) ? 0 : (this.bool_15 ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_105);
                        continue;
                      }
                      if (Class32.bool_79 && !Class32.list_15.Contains(this.string_2) && Class32.list_19.Contains(this.int_4))
                      {
                        this.method_10(Class33.string_15);
                        continue;
                      }
                      if (Class32.bool_62 && !Class32.list_15.Contains(this.string_2) && this.bool_19)
                      {
                        this.method_10(Class33.string_15);
                        continue;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 36864:
                      goto label_973;
                    default:
                      goto label_1145;
                  }
                }
                else
                {
                  switch (uint160)
                  {
                    case 29907:
                      if ((!Class32.bool_54 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                      {
                        switch (gclass3.method_4())
                        {
                          case 1:
                            if (!Class32.list_62.Contains(this.string_1))
                            {
                              if (Class32.string_29 == "IP")
                              {
                                lock (Class32.list_61)
                                  Class32.list_61.Add(this.string_0);
                              }
                              else
                              {
                                lock (Class32.list_61)
                                  Class32.list_61.Add(this.string_6);
                              }
                              lock (Class32.list_62)
                              {
                                Class32.list_62.Add(this.string_1);
                                break;
                              }
                            }
                            else
                              break;
                          case 2:
                            if (Class32.string_29 == "IP")
                            {
                              lock (Class32.list_61)
                                Class32.list_61.Remove(this.string_0);
                            }
                            else
                            {
                              lock (Class32.list_61)
                                Class32.list_61.Remove(this.string_6);
                            }
                            lock (Class32.list_62)
                            {
                              Class32.list_62.Remove(this.string_1);
                              break;
                            }
                        }
                        if ((Class34.smethod_1(this.string_6, this.string_0) <= Class32.int_13 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_9);
                          continue;
                        }
                      }
                      if ((this.int_2 >= Class32.int_15 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_64);
                        continue;
                      }
                      if ((Class32.bool_127 ? (this.bool_27 ? 1 : 0) : 0) == 0)
                      {
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      }
                      continue;
                    case 29966:
                      this.bool_16 = false;
                      if ((uint) gclass3.method_0().Length <= 0U)
                      {
                        if ((!Class32.list_16.Contains(this.string_2) || !Class32.bool_100 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          GClass3 gclass3_1 = new GClass3((ushort) 28688);
                          gclass3_1.method_33((ushort) 14);
                          this.gclass4_1.method_15(gclass3_1);
                          this.method_5(true);
                        }
                        if ((!Class32.bool_82 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && Class32.list_18.Contains(this.int_4))
                        {
                          this.gclass4_1.method_15(new GClass3((ushort) 28769));
                          this.method_5(true);
                        }
                        if ((!Class32.bool_7 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                        {
                          if (this.bool_2)
                          {
                            if (Class32.string_23 == "Symbol")
                            {
                              GClass3 gclass3_1 = new GClass3((ushort) 29698);
                              gclass3_1.method_31((byte) 0);
                              this.gclass4_1.method_15(gclass3_1);
                              this.method_5(true);
                            }
                            this.bool_2 = false;
                          }
                          if (Class32.bool_9 && this.bool_5)
                          {
                            this.bool_20 = false;
                            this.dateTime_16 = DateTime.Now;
                          }
                          if ((!Class32.bool_8 ? 0 : (this.bool_13 ? 1 : 0)) != 0)
                          {
                            this.bool_21 = false;
                            this.dateTime_17 = DateTime.Now;
                          }
                          if ((!Class32.bool_114 ? 0 : (this.bool_6 ? 1 : 0)) != 0)
                          {
                            this.bool_22 = false;
                            this.dateTime_18 = DateTime.Now;
                          }
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      }
                      Class18.smethod_0("IP: [" + this.string_0 + "] UserID: [" + this.string_2 + "] Reason:[EXPLOITING]", "Magenta");
                      this.method_7();
                      continue;
                    default:
                      goto label_1145;
                  }
                }
              }
              else if (uint160 > (ushort) 28875)
              {
                if (uint160 <= (ushort) 28950)
                {
                  switch ((int) uint160 - 28913)
                  {
                    case 0:
                    case 1:
                    case 3:
                    case 9:
                      goto label_1149;
                    case 2:
                      if ((Convert.ToInt32(Class32.int_36) <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                      {
                        if (this.int_5 >= Convert.ToInt32(Class32.int_36))
                        {
                          this.method_10(Class33.string_79);
                          continue;
                        }
                        ++this.int_5;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                      goto label_1145;
                    case 10:
                      if ((Convert.ToInt32(Class32.int_35) > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                      {
                        this.int_6 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',3,0");
                        if (this.int_6 >= Convert.ToInt32(Class32.int_35))
                        {
                          this.method_10(Class33.string_80);
                          continue;
                        }
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    default:
                      if (uint160 != (ushort) 28932)
                      {
                        if (uint160 == (ushort) 28950)
                        {
                          this.bool_26 = false;
                          this.gclass4_1.method_15(gclass3);
                          this.method_5(true);
                          continue;
                        }
                        goto label_1145;
                      }
                      else
                        goto label_1149;
                  }
                }
                else if (uint160 > (ushort) 29009)
                {
                  switch (uint160)
                  {
                    case 29015:
                      if (Class32.bool_86 && Class32.dictionary_4.ContainsKey(this.string_1))
                      {
                        this.method_10(Class33.string_20);
                        continue;
                      }
                      if ((!Class32.bool_18 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && gclass3.method_4() == (byte) 1)
                      {
                        int num1 = (int) gclass3.method_4();
                        if (num1 == 13)
                        {
                          string str = GClass7.smethod_3("EXEC [_Methods] '" + this.string_1 + "',3,'" + (object) num1 + "'");
                          if (Class32.list_27.Contains(str))
                          {
                            this.method_10(Class33.string_110);
                            continue;
                          }
                        }
                        else
                        {
                          this.method_10(Class33.string_109);
                          continue;
                        }
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    case 29016:
                      if (Class32.bool_123 && this.socket_0.NoDelay)
                        this.socket_0.NoDelay = false;
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    default:
                      goto label_1145;
                  }
                }
                else
                {
                  switch (uint160)
                  {
                    case 29008:
                      if ((!Class32.bool_66 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_19)
                      {
                        this.method_10(Class33.string_68);
                        continue;
                      }
                      if ((!Class32.bool_86 ? 0 : (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_20);
                        continue;
                      }
                      if ((!Class32.bool_15 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) == 0)
                      {
                        if (gclass3.method_4() == (byte) 2)
                        {
                          string key = (string) null;
                          switch (gclass3.method_4())
                          {
                            case 3:
                              int num1 = (int) gclass3.method_4();
                              int num2 = (int) gclass3.method_4();
                              int num3;
                              if (Class32.bool_20)
                                num3 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',6,'" + (object) num2 + "'");
                              else
                                num3 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',1,'" + (object) num2 + "'");
                              if ((Class32.bool_16 ? 1 : (Class32.bool_115 ? 1 : 0)) != 0)
                                key = GClass7.smethod_3("EXEC [_Methods] '" + this.string_1 + "',3,'" + (object) num2 + "'");
                              if ((!Class32.bool_115 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && (!this.bool_19 ? 0 : (Class32.list_39.Contains(key) ? 1 : 0)) != 0)
                              {
                                this.method_10(Class33.string_121);
                                continue;
                              }
                              if ((!Class32.bool_16 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) == 0)
                              {
                                if ((Class32.int_7 <= 0 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && num3 >= Class32.int_7)
                                {
                                  this.method_10(Class33.string_71);
                                  continue;
                                }
                                break;
                              }
                              if (!Class32.dictionary_3.ContainsKey(key))
                              {
                                if (num3 >= Class32.int_7)
                                {
                                  this.method_10(Class33.string_71);
                                  continue;
                                }
                                break;
                              }
                              int num4 = Class32.dictionary_3[key];
                              if (num3 >= num4)
                              {
                                this.method_10(Class33.string_70);
                                continue;
                              }
                              break;
                            case 8:
                              if (Class32.bool_14)
                              {
                                this.method_10(Class33.string_72);
                                continue;
                              }
                              if (Class32.bool_20)
                              {
                                int num5 = (int) gclass3.method_4();
                                int num6 = (int) gclass3.method_4();
                                int num7 = (int) gclass3.method_4();
                                int num8 = GClass7.smethod_4("EXEC [_AdvSystem] '" + this.string_1 + "','" + (object) num6 + "','" + (object) num7 + "'");
                                if ((Class32.bool_16 ? 1 : (Class32.bool_115 ? 1 : 0)) != 0)
                                  key = GClass7.smethod_3("EXEC [_Methods] '" + this.string_1 + "',3,'" + (object) num7 + "'");
                                if ((!Class32.bool_115 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && (!this.bool_19 ? 0 : (Class32.list_39.Contains(key) ? 1 : 0)) != 0)
                                {
                                  this.method_10(Class33.string_121);
                                  continue;
                                }
                                if ((!Class32.bool_16 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                                {
                                  if (!Class32.dictionary_3.ContainsKey(key))
                                  {
                                    if (num8 > Class32.int_7)
                                    {
                                      this.method_10(Class33.string_73);
                                      continue;
                                    }
                                    break;
                                  }
                                  int num9 = Class32.dictionary_3[key];
                                  if (num8 > num9)
                                  {
                                    this.method_10(Class33.string_73);
                                    continue;
                                  }
                                  break;
                                }
                                if ((Class32.int_7 > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && num8 > Class32.int_7)
                                {
                                  this.method_10(Class33.string_73);
                                  continue;
                                }
                                break;
                              }
                              break;
                          }
                        }
                        this.gclass4_1.method_15(gclass3);
                        this.method_5(true);
                        continue;
                      }
                      this.method_10(Class33.string_69);
                      continue;
                    case 29009:
                      if ((!Class32.bool_72 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_19)
                      {
                        this.method_10(Class33.string_74);
                        continue;
                      }
                      if ((!Class32.bool_86 ? 0 : (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_20);
                        continue;
                      }
                      if ((!Class32.bool_17 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                      {
                        this.method_10(Class33.string_75);
                        continue;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    default:
                      goto label_1145;
                  }
                }
              }
              else if (uint160 > (ushort) 28858)
              {
                switch (uint160)
                {
                  case 28869:
                    if ((Class32.bool_73 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0 && (!this.bool_19 ? 0 : (Class32.list_22.Contains(this.int_4) ? 1 : 0)) != 0)
                    {
                      this.method_10(Class33.string_14);
                      continue;
                    }
                    if ((!Class32.bool_81 || Class32.list_15.Contains(this.string_2) || !Class32.list_21.Contains(this.int_4) ? 0 : (!this.bool_15 ? 1 : 0)) == 0)
                    {
                      if ((!Class32.bool_7 ? 0 : (this.bool_2 ? 1 : 0)) != 0)
                      {
                        if (Class32.string_23 == "Symbol")
                        {
                          GClass3 gclass3_1 = new GClass3((ushort) 29698);
                          gclass3_1.method_31((byte) 0);
                          this.gclass4_1.method_15(gclass3_1);
                          this.method_5(true);
                        }
                        this.bool_2 = false;
                      }
                      this.gclass4_1.method_15(gclass3);
                      this.method_5(true);
                      continue;
                    }
                    this.method_10(Class33.string_105);
                    continue;
                  case 28870:
                    this.bool_11 = true;
                    if (!Class32.list_15.Contains(this.string_2))
                    {
                      if (Class32.bool_30)
                      {
                        int num1 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',10,0");
                        if ((!Class32.list_29.Contains(this.int_4) ? 0 : (num1 > 0 ? 1 : 0)) != 0)
                        {
                          this.method_10(Class33.string_65);
                          continue;
                        }
                      }
                      else if (Class32.bool_31 && GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',10,0") > 0)
                      {
                        this.method_10(Class33.string_65);
                        continue;
                      }
                    }
                    this.gclass4_1.method_15(gclass3);
                    this.method_5(true);
                    continue;
                  case 28875:
                    uint num28 = (uint) gclass3.method_4();
                    gclass3.method_14();
                    if (num28 >= 1U)
                    {
                      if (Class32.bool_83)
                      {
                        try
                        {
                          dateTime = DateTime.Now;
                          timeSpan = dateTime.Subtract(this.dateTime_10);
                          if (Convert.ToInt32(timeSpan.TotalSeconds) < 55)
                          {
                            this.method_10(Class33.string_111);
                            continue;
                          }
                        }
                        catch
                        {
                        }
                      }
                      if (Class32.bool_47 && this.bool_13)
                      {
                        this.method_10(Class33.string_112);
                        continue;
                      }
                      if (Class32.bool_81 && !Class32.list_15.Contains(this.string_2) && Class32.list_21.Contains(this.int_4))
                      {
                        this.method_10(Class33.string_105);
                        continue;
                      }
                    }
                    this.gclass4_1.method_15(gclass3);
                    this.method_5(true);
                    continue;
                  default:
                    goto label_1145;
                }
              }
              else
              {
                switch (uint160)
                {
                  case 28849:
                    if (this.bool_16)
                    {
                      this.method_10(Class33.string_24);
                      continue;
                    }
                    if ((this.int_2 >= Convert.ToInt32(Class32.int_18) ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0)
                    {
                      this.method_10(Class33.string_25.Replace("{Level}", Convert.ToInt32(Class32.int_18).ToString()));
                      continue;
                    }
                    if ((Convert.ToInt32(Class32.int_24) > 0 ? (!Class32.list_15.Contains(this.string_2) ? 1 : 0) : 0) != 0)
                    {
                      try
                      {
                        dateTime = DateTime.Now;
                        timeSpan = dateTime.Subtract(this.dateTime_1);
                        int int32 = Convert.ToInt32(timeSpan.TotalSeconds);
                        if (int32 < Convert.ToInt32(Class32.int_24))
                        {
                          this.method_10(Class33.string_26.Replace("{Delay}", (Convert.ToInt32(Class32.int_24) - int32).ToString()));
                          continue;
                        }
                      }
                      catch
                      {
                      }
                      this.dateTime_1 = DateTime.Now;
                    }
                    if ((!Class32.bool_113 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && Class34.smethod_9(gclass3.method_14().ToString().ToLower()))
                    {
                      this.method_10(Class33.string_27);
                      continue;
                    }
                    if (Class32.bool_86 && Class32.dictionary_4.ContainsKey(this.string_1))
                    {
                      this.method_10(Class33.string_20);
                      continue;
                    }
                    if ((!Class32.bool_52 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_5)
                    {
                      this.method_10(Class33.string_28);
                      continue;
                    }
                    if ((!Class32.bool_58 ? 0 : (!Class32.list_15.Contains(this.string_2) ? 1 : 0)) != 0 && this.bool_6)
                    {
                      this.method_10(Class33.string_117);
                      continue;
                    }
                    if (Class32.bool_92 && !Class32.list_29.Contains(this.int_4))
                    {
                      this.method_10(Class33.string_29);
                      continue;
                    }
                    if (Class32.bool_69 && !Class32.list_15.Contains(this.string_2) && this.bool_19)
                    {
                      this.method_10(Class33.string_30);
                      continue;
                    }
                    this.gclass4_1.method_15(gclass3);
                    this.method_5(true);
                    continue;
                  case 28852:
                    goto label_1149;
                  case 28858:
                    if (!Class32.list_15.Contains(this.string_2) && Class32.bool_113 && Class34.smethod_9(gclass3.method_14().ToString().ToLower()))
                    {
                      this.method_10(Class33.string_93);
                      continue;
                    }
                    this.gclass4_1.method_15(gclass3);
                    this.method_5(true);
                    continue;
                  default:
                    goto label_1145;
                }
              }
label_968:
              if (Class32.bool_88 && !Class32.list_15.Contains(this.string_2))
              {
                this.method_10(Class33.string_104);
                continue;
              }
              this.gclass4_1.method_15(gclass3);
              this.method_5(true);
              continue;
label_973:
              this.method_5(false);
              continue;
label_1145:
              this.gclass4_1.method_15(gclass3);
              this.method_5(true);
              continue;
label_1149:
              if ((!Class32.bool_86 ? 0 : (Class32.dictionary_4.ContainsKey(this.string_1) ? 1 : 0)) != 0)
              {
                this.method_10(Class33.string_20);
              }
              else
              {
                this.gclass4_1.method_15(gclass3);
                this.method_5(true);
              }
            }
            this.method_4();
          }
          else
            this.method_4();
        }
        else
        {
          try
          {
            this.method_7();
          }
          catch
          {
          }
          this.gdelegate0_0(ref this.socket_0, this.genum9_0);
        }
      }
      catch
      {
        try
        {
          this.method_7();
        }
        catch
        {
        }
        this.gdelegate0_0(ref this.socket_0, this.genum9_0);
      }
    }
  }

  private void method_2(IAsyncResult iasyncResult_0)
  {
    lock (this.object_0)
    {
      try
      {
        int int_1 = this.socket_1.EndReceive(iasyncResult_0);
        if ((uint) int_1 > 0U)
        {
          this.gclass4_1.method_16(this.byte_1, 0, int_1);
          List<GClass3> gclass3List = this.gclass4_1.method_19();
          if (gclass3List != null)
          {
            foreach (GClass3 gclass3_1_1 in gclass3List)
            {
              if (gclass3_1_1.UInt16_0 != (ushort) 20480 && gclass3_1_1.UInt16_0 != (ushort) 36864)
              {
                if ((gclass3_1_1.UInt16_0 != (ushort) 12344 ? 0 : (Class32.bool_123 ? 1 : 0)) != 0)
                {
                  if ((int) gclass3_1_1.method_8() == (int) this.uint_1 && !this.socket_0.NoDelay)
                    this.socket_0.NoDelay = true;
                  this.gclass4_0.method_15(gclass3_1_1);
                  this.method_5(false);
                }
                else if (gclass3_1_1.UInt16_0 == (ushort) 12320)
                {
                  gclass3_1_1.method_1();
                  this.uint_1 = gclass3_1_1.method_8();
                  this.gclass4_0.method_15(gclass3_1_1);
                  this.method_5(false);
                }
                else if (gclass3_1_1.UInt16_0 != (ushort) 12307)
                {
                  if ((gclass3_1_1.UInt16_0 != (ushort) 45392 ? 0 : (Class32.bool_10 ? 1 : 0)) != 0)
                  {
                    try
                    {
                      string key = (string) null;
                      int num1 = (int) gclass3_1_1.method_6();
                      int num2 = (int) gclass3_1_1.method_4();
                      int num3 = (int) gclass3_1_1.method_4();
                      long num4 = (long) gclass3_1_1.method_10();
                      int int_9 = (int) gclass3_1_1.method_4();
                      if (int_9 > 0)
                        key = GClass7.smethod_3("EXEC [_Methods] '" + this.string_1 + "',3,'" + (object) num3 + "'");
                      if ((!Class32.bool_109 ? 0 : (int_9 > 0 ? 1 : 0)) != 0)
                        GClass7.smethod_0("EXEC [_StoreAlchemyLog] '" + this.string_1 + "','" + key + "','" + (object) int_9 + "'");
                      int num5 = Class32.bool_12 ? (!Class32.dictionary_0.ContainsKey(key) ? Convert.ToInt32(Class32.int_6) : Class32.dictionary_0[key]) : Convert.ToInt32(Class32.int_6);
                      if (Class32.bool_11)
                        this.int_3 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',9,'" + (object) num3 + "'");
                      if ((int_9 < num5 ? 0 : (int_9 > 0 ? 1 : 0)) != 0)
                      {
                        string str = GClass7.smethod_3("EXEC [_Methods] '" + this.string_1 + "',1,'" + (object) num3 + "'");
                        if (string.IsNullOrEmpty(str))
                          str = "An Item";
                        if (!Class32.bool_11)
                          this.method_16("[Plus System] [" + this.string_1 + "] has increased [" + str + "] to plus (" + (object) int_9 + ")", int_9);
                        else if (this.int_3 != 0)
                          this.method_16("[Plus System] [" + this.string_1 + "] has increased [" + str + "] to plus (" + (object) (int_9 + this.int_3) + ") (have Adv [" + (object) this.int_3 + "])", int_9);
                        else
                          this.method_16("[Plus System] [" + this.string_1 + "] has increased [" + str + "] to plus (" + (object) int_9 + ") (No Adv)", int_9);
                      }
                    }
                    catch
                    {
                    }
                    this.gclass4_0.method_15(gclass3_1_1);
                    this.method_5(false);
                  }
                  else if (gclass3_1_1.UInt16_0 != (ushort) 45089)
                  {
                    if (gclass3_1_1.UInt16_0 != (ushort) 46358)
                    {
                      if (gclass3_1_1.UInt16_0 == (ushort) 12326)
                      {
                        try
                        {
                          gclass3_1_1.method_1();
                          switch (gclass3_1_1.method_4())
                          {
                            case 1:
                              if ((int) gclass3_1_1.method_8() == (int) this.uint_1)
                              {
                                if (Class32.bool_103)
                                {
                                  GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + Class17.smethod_1(gclass3_1_1.method_14()).Replace("'", "''") + "','None',0");
                                  break;
                                }
                                break;
                              }
                              break;
                            case 3:
                              if ((int) gclass3_1_1.method_8() == (int) this.uint_1)
                              {
                                if (Class32.bool_104)
                                {
                                  GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + Class17.smethod_1(gclass3_1_1.method_14()).Replace("'", "''") + "','None',1");
                                  break;
                                }
                                break;
                              }
                              break;
                            case 4:
                              if (Class32.bool_105)
                              {
                                if (gclass3_1_1.method_14() == this.string_1)
                                {
                                  GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + Class17.smethod_1(gclass3_1_1.method_14()).Replace("'", "''") + "','None',2");
                                  break;
                                }
                                break;
                              }
                              break;
                            case 5:
                              if (Class32.bool_106)
                              {
                                if (gclass3_1_1.method_14() == this.string_1)
                                {
                                  GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + Class17.smethod_1(gclass3_1_1.method_14()).Replace("'", "''") + "','None',3");
                                  break;
                                }
                                break;
                              }
                              break;
                            case 6:
                              string key = gclass3_1_1.method_14();
                              string str = gclass3_1_1.method_14();
                              if (Class32.bool_107 && key == this.string_1)
                                GClass7.smethod_0("EXEC [_StoreLog] '" + key + "','" + str.Replace("'", "''") + "','None',4");
                              if (Class32.bool_125)
                              {
                                if (Class32.dictionary_12.ContainsKey(key))
                                {
                                  this.method_11(key + ":" + str.Replace("+", "plus") ?? "", Class32.dictionary_12[key]);
                                  continue;
                                }
                                break;
                              }
                              break;
                            case 11:
                              if (Class32.bool_108)
                              {
                                if (gclass3_1_1.method_14() == this.string_1)
                                {
                                  GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + Class17.smethod_1(gclass3_1_1.method_14()).Replace("'", "''") + "','None',5");
                                  break;
                                }
                                break;
                              }
                              break;
                            case 16:
                              if (Class32.bool_111)
                              {
                                if (gclass3_1_1.method_14() == this.string_1)
                                {
                                  GClass7.smethod_0("EXEC [_StoreLog] '" + this.string_1 + "','" + Class17.smethod_1(gclass3_1_1.method_14()).Replace("'", "''") + "','None',7");
                                  break;
                                }
                                break;
                              }
                              break;
                          }
                        }
                        catch
                        {
                        }
                        this.gclass4_0.method_15(gclass3_1_1);
                        this.method_5(false);
                      }
                      else if (gclass3_1_1.UInt16_0 != (ushort) 12310)
                      {
                        if (gclass3_1_1.UInt16_0 != (ushort) 12627)
                        {
                          if (gclass3_1_1.UInt16_0 != (ushort) 12300)
                          {
                            this.gclass4_0.method_15(gclass3_1_1);
                            this.method_5(false);
                          }
                          else
                          {
                            switch (gclass3_1_1.method_4())
                            {
                              case 5:
                                gclass3_1_1.method_17(1);
                                uint key1 = gclass3_1_1.method_8();
                                if (Class32.bool_134 && Class32.string_58 == this.string_1 && (Class32.bool_137 && Class32.dictionary_11.ContainsKey(key1)))
                                {
                                  uint num = key1;
                                  if (num > 1982U)
                                  {
                                    switch (num)
                                    {
                                      case 2002:
                                        Class18.smethod_2("[Unique Log] " + Class32.dictionary_11[key1] + " has appeared in Karakoram.");
                                        goto label_77;
                                      case 3810:
                                        Class18.smethod_2("[Unique Log] " + Class32.dictionary_11[key1] + " has appeared in Taklamakan.");
                                        goto label_77;
                                      case 3875:
                                        Class18.smethod_2("[Unique Log] " + Class32.dictionary_11[key1] + " has appeared on Mt. Roc.");
                                        goto label_77;
                                    }
                                  }
                                  else
                                  {
                                    switch (num)
                                    {
                                      case 1954:
                                        Class18.smethod_2("[Unique Log] " + Class32.dictionary_11[key1] + " has appeared on Tiger Mountain.");
                                        goto label_77;
                                      case 1982:
                                        Class18.smethod_2("[Unique Log] " + Class32.dictionary_11[key1] + " has appeared in Tarim Basin.");
                                        goto label_77;
                                    }
                                  }
                                  Class18.smethod_2("[Unique Log] " + Class32.dictionary_11[key1] + " has appeared");
                                  break;
                                }
                                break;
                              case 6:
                                gclass3_1_1.method_17(1);
                                uint key2 = gclass3_1_1.method_8();
                                string str = gclass3_1_1.method_14();
                                if ((!Class32.bool_134 || !(this.string_1 == Class32.string_58) || !Class32.bool_138 ? 0 : (Class32.dictionary_11.ContainsKey(key2) ? 1 : 0)) != 0)
                                {
                                  Class18.smethod_2("[Unique Log] " + str + " has killed " + Class32.dictionary_11[key2] ?? "");
                                  break;
                                }
                                break;
                            }
label_77:
                            this.gclass4_0.method_15(gclass3_1_1);
                            this.method_5(false);
                          }
                        }
                        else
                        {
                          this.uint_6 = gclass3_1_1.method_8();
                          this.uint_7 = gclass3_1_1.method_8();
                          this.uint_8 = gclass3_1_1.method_8();
                          this.gclass4_0.method_15(gclass3_1_1);
                          this.method_5(false);
                        }
                      }
                      else
                      {
                        if (this.bool_26)
                          this.bool_26 = false;
                        this.gclass4_0.method_15(gclass3_1_1);
                        this.method_5(false);
                      }
                    }
                    else
                    {
                      if ((Class32.bool_81 ? 1 : (Class32.bool_84 ? 1 : 0)) != 0)
                      {
                        gclass3_1_1.method_1();
                        if (gclass3_1_1.method_4() == (byte) 1)
                        {
                          uint num1 = gclass3_1_1.method_8();
                          int num2 = (int) gclass3_1_1.method_4();
                          if ((int) num1 == (int) this.uint_1)
                          {
                            if ((num2 <= 0 ? 0 : (num2 <= 5 ? 1 : 0)) != 0)
                            {
                              this.bool_15 = true;
                              if (!Class32.list_12.Contains(this.uint_1))
                              {
                                lock (Class32.list_12)
                                  Class32.list_12.Add(this.uint_1);
                              }
                            }
                            else
                            {
                              this.bool_15 = false;
                              if (Class32.list_12.Contains(this.uint_1))
                              {
                                lock (Class32.list_12)
                                  Class32.list_12.Remove(this.uint_1);
                              }
                            }
                          }
                        }
                      }
                      this.gclass4_0.method_15(gclass3_1_1);
                      this.method_5(false);
                    }
                  }
                  else
                  {
                    gclass3_1_1.method_1();
                    uint index = gclass3_1_1.method_8();
                    if ((int) index != (int) this.uint_1)
                    {
                      if (Class32.bool_126 && Class32.dictionary_10.ContainsKey(index))
                      {
                        GClass3 gclass3_1_2 = new GClass3((ushort) 12511);
                        gclass3_1_2.method_35(index);
                        gclass3_1_2.method_44((object) Class32.dictionary_10[index]);
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                      }
                    }
                    else
                    {
                      int num1 = (int) gclass3_1_1.method_4();
                      short num2 = gclass3_1_1.method_7();
                      if ((num2.ToString().Length < 5 ? 0 : (gclass3_1_1.method_0().Length >= 24 ? 1 : 0)) != 0)
                      {
                        this.int_4 = (int) num2;
                        if (Class32.list_9.Contains(this.int_4))
                        {
                          this.bool_13 = true;
                          this.bool_5 = false;
                        }
                        else if (Class32.list_10.Contains(this.int_4))
                        {
                          this.bool_5 = true;
                          this.bool_13 = false;
                        }
                        else
                        {
                          this.bool_13 = false;
                          this.bool_5 = false;
                        }
                      }
                      if (Class32.bool_7 && !this.bool_5 && (!this.bool_13 && !this.bool_6))
                      {
                        this.dateTime_5 = DateTime.Now;
                        this.bool_1 = false;
                        if (this.bool_2)
                        {
                          if (Class32.string_23 == "Symbol")
                          {
                            GClass3 gclass3_1_2 = new GClass3((ushort) 29698);
                            gclass3_1_2.method_31((byte) 0);
                            this.gclass4_1.method_15(gclass3_1_2);
                            this.method_5(true);
                          }
                          this.bool_2 = false;
                        }
                      }
                      else if ((!Class32.bool_9 ? 0 : (this.bool_5 ? 1 : 0)) != 0)
                      {
                        this.dateTime_16 = DateTime.Now;
                        this.bool_20 = false;
                      }
                      else if (Class32.bool_8 && this.bool_13)
                      {
                        this.dateTime_17 = DateTime.Now;
                        this.bool_21 = false;
                      }
                      else if ((!Class32.bool_114 ? 0 : (this.bool_6 ? 1 : 0)) != 0)
                      {
                        this.dateTime_18 = DateTime.Now;
                        this.bool_22 = false;
                      }
                    }
                    this.gclass4_0.method_15(gclass3_1_1);
                    this.method_5(false);
                  }
                }
                else
                {
                  gclass3_1_1.method_1();
                  this.bool_3 = true;
                  if (Class32.bool_7)
                  {
                    this.bool_1 = false;
                    this.dateTime_5 = DateTime.Now;
                  }
                  this.method_32();
                  this.method_33();
                  this.method_34();
                  this.bool_6 = false;
                  this.method_35();
                  this.method_22();
                  this.method_36();
                  this.method_21();
                  this.method_37();
                  this.method_38();
                  this.method_39();
                  this.method_40();
                  this.method_41();
                  this.method_42();
                  this.method_43();
                  this.method_44();
                  this.gclass4_0.method_15(gclass3_1_1);
                  this.method_5(false);
                }
              }
              else
                this.method_5(true);
            }
          }
          this.method_3();
        }
        else
        {
          try
          {
            this.method_7();
          }
          catch
          {
          }
          this.gdelegate0_0(ref this.socket_0, this.genum9_0);
        }
      }
      catch
      {
        try
        {
          this.method_7();
        }
        catch
        {
        }
        this.gdelegate0_0(ref this.socket_0, this.genum9_0);
      }
    }
  }

  private void method_3()
  {
    try
    {
      this.socket_1.BeginReceive(this.byte_1, 0, this.byte_1.Length, SocketFlags.None, new AsyncCallback(this.method_2), (object) null);
    }
    catch
    {
      try
      {
        this.method_7();
      }
      catch
      {
      }
      this.gdelegate0_0(ref this.socket_0, this.genum9_0);
    }
  }

  private void method_4()
  {
    try
    {
      this.socket_0.BeginReceive(this.byte_0, 0, this.byte_0.Length, SocketFlags.None, new AsyncCallback(this.method_1), (object) null);
    }
    catch
    {
      try
      {
        this.method_7();
      }
      catch
      {
      }
      this.gdelegate0_0(ref this.socket_0, this.genum9_0);
    }
  }

  public void method_5(bool bool_31)
  {
    lock (this.object_0)
    {
      foreach (KeyValuePair<GClass5, GClass3> keyValuePair in (bool_31 ? this.gclass4_1 : this.gclass4_0).method_18())
        (bool_31 ? this.socket_1 : this.socket_0).Send(keyValuePair.Key.Byte_0);
    }
  }

  private double method_6()
  {
    double num1 = 0.0;
    TimeSpan timeSpan = DateTime.Now - this.dateTime_0;
    if (this.ulong_0 > (ulong) int.MaxValue)
      this.ulong_0 = 0UL;
    if (this.ulong_0 > 0UL)
    {
      try
      {
        double num2 = timeSpan.TotalSeconds;
        if (timeSpan.TotalSeconds < 1.0)
          num2 = 1.0;
        num1 = Math.Round((double) this.ulong_0 / num2, 2);
      }
      catch
      {
      }
    }
    return num1;
  }

  public void method_7()
  {
    if (this.bool_24)
      return;
    this.bool_24 = true;
    try
    {
      if (this.socket_1 != null)
      {
        this.method_30();
        this.method_31();
        this.method_27();
        this.method_28();
        this.method_29();
        if (this.socket_1.Connected)
          this.socket_1.Disconnect(false);
        if (this.socket_0.Connected)
          this.socket_0.Disconnect(false);
        try
        {
          this.socket_1.Close();
          this.socket_0.Close();
        }
        catch
        {
        }
      }
      this.socket_1 = (Socket) null;
    }
    catch
    {
    }
  }

  private void method_8()
  {
    try
    {
      if (!(Class32.string_14 == "Silk"))
      {
        if (Class32.string_14 == "Gift")
          this.uint_7 += Class32.uint_0;
        else
          this.uint_8 += Class32.uint_0;
      }
      else
        this.uint_6 += Class32.uint_0;
      GClass7.smethod_0("EXEC [_SilkSystem] '" + this.string_2 + "','" + Class32.string_14 + "' ,'" + (object) Class32.uint_0 + "' ");
      GClass3 gclass3_1_1 = new GClass3((ushort) 12627);
      gclass3_1_1.method_35(this.uint_6);
      gclass3_1_1.method_35(this.uint_7);
      gclass3_1_1.method_35(this.uint_8);
      this.gclass4_0.method_15(gclass3_1_1);
      this.method_5(false);
      GClass3 gclass3_1_2 = new GClass3((ushort) 12628);
      gclass3_1_2.method_31((byte) 4);
      gclass3_1_2.method_31((byte) 0);
      gclass3_1_2.method_35(Class32.uint_0);
      this.gclass4_0.method_15(gclass3_1_2);
      this.method_5(false);
    }
    catch
    {
    }
  }

  public void method_9(string string_9)
  {
    GClass3 gclass3_1 = new GClass3((ushort) 12326);
    gclass3_1.method_31((byte) 2);
    gclass3_1.method_41(Class32.string_31);
    gclass3_1.method_41(string_9);
    this.gclass4_0.method_15(gclass3_1);
    this.method_5(false);
  }

  public void method_10(string string_9)
  {
    if (Class32.string_50 == "none")
    {
      GClass3 gclass3_1 = new GClass3((ushort) 12326);
      gclass3_1.method_31((byte) 7);
      gclass3_1.method_41(string_9);
      this.gclass4_0.method_15(gclass3_1);
      this.method_5(false);
    }
    else
    {
      string string50 = Class32.string_50;
      byte byte_4 = !(string50 == "blue") ? (!(string50 == "green") ? (!(string50 == "pink") ? (byte) 0 : (byte) 61) : (byte) 60) : (byte) 59;
      if (byte_4 <= (byte) 0)
        return;
      this.method_11(string_9, byte_4);
    }
  }

  public void method_11(string string_9, byte byte_4)
  {
    GClass3 gclass3_1 = new GClass3((ushort) 12300);
    gclass3_1.method_31((byte) 28);
    gclass3_1.method_31((byte) 12);
    gclass3_1.method_31(byte_4);
    gclass3_1.method_41(string_9);
    this.gclass4_0.method_15(gclass3_1);
    this.method_5(false);
  }

  public void method_12(string string_9)
  {
    foreach (GClass12 gclass12 in Class32.list_7)
    {
      if (gclass12.socket_0.Connected)
      {
        GClass3 gclass3_1 = new GClass3((ushort) 12326);
        gclass3_1.method_31((byte) 7);
        gclass3_1.method_41(string_9);
        gclass12.gclass4_0.method_15(gclass3_1);
        gclass12.method_5(false);
      }
    }
    if (!(Class32.string_22 != "0"))
      return;
    foreach (GClass11 gclass11 in Class32.list_8)
    {
      if (gclass11.socket_0.Connected)
      {
        GClass3 gclass3_1 = new GClass3((ushort) 12326);
        gclass3_1.method_31((byte) 7);
        gclass3_1.method_41(string_9);
        gclass11.gclass4_0.method_15(gclass3_1);
        gclass11.method_5(false);
      }
    }
  }

  public bool method_13(string string_9)
  {
    bool flag = false;
    foreach (GClass12 gclass12 in Class32.list_7)
    {
      if ((!gclass12.socket_0.Connected ? 0 : (gclass12.string_1 == string_9 ? 1 : 0)) != 0)
      {
        flag = gclass12.bool_7;
        break;
      }
    }
    if (Class32.string_22 != "0")
    {
      foreach (GClass11 gclass11 in Class32.list_8)
      {
        if (gclass11.socket_0.Connected && gclass11.string_1 == string_9)
        {
          flag = gclass11.bool_7;
          break;
        }
      }
    }
    return flag;
  }

  public void method_14(string string_9, byte byte_4)
  {
    foreach (GClass12 gclass12 in Class32.list_7)
    {
      if (gclass12.socket_0.Connected)
      {
        GClass3 gclass3_1 = new GClass3((ushort) 12300);
        gclass3_1.method_31((byte) 28);
        gclass3_1.method_31((byte) 12);
        gclass3_1.method_31(byte_4);
        gclass3_1.method_41(string_9);
        gclass12.gclass4_0.method_15(gclass3_1);
        gclass12.method_5(false);
      }
    }
    if (!(Class32.string_22 != "0"))
      return;
    foreach (GClass11 gclass11 in Class32.list_8)
    {
      if (gclass11.socket_0.Connected)
      {
        GClass3 gclass3_1 = new GClass3((ushort) 12300);
        gclass3_1.method_31((byte) 28);
        gclass3_1.method_31((byte) 12);
        gclass3_1.method_31(byte_4);
        gclass3_1.method_41(string_9);
        gclass11.gclass4_0.method_15(gclass3_1);
        gclass11.method_5(false);
      }
    }
  }

  public void method_15(uint uint_9, int int_9)
  {
    GClass3 gclass3_1 = new GClass3((ushort) 12511);
    gclass3_1.method_35(uint_9);
    gclass3_1.method_44((object) int_9);
    this.gclass4_0.method_15(gclass3_1);
    this.method_5(false);
  }

  public void method_16(string string_9, int int_9)
  {
    foreach (GClass12 gclass12 in Class32.list_7)
    {
      if (gclass12.socket_0.Connected)
      {
        if (Class32.bool_13)
        {
          if ((!Class32.dictionary_1.ContainsKey(gclass12.string_1) || int_9 < Class32.dictionary_1[gclass12.string_1] ? 0 : (Class32.dictionary_1[gclass12.string_1] > 0 ? 1 : 0)) != 0)
          {
            if (!(Class32.string_50 == "none"))
            {
              gclass12.method_11(string_9, (byte) 60);
            }
            else
            {
              GClass3 gclass3_1 = new GClass3((ushort) 12326);
              gclass3_1.method_31((byte) 7);
              gclass3_1.method_41(string_9);
              gclass12.gclass4_0.method_15(gclass3_1);
              gclass12.method_5(false);
            }
          }
          else if (!Class32.dictionary_1.ContainsKey(gclass12.string_1))
          {
            if (Class32.string_50 == "none")
            {
              GClass3 gclass3_1 = new GClass3((ushort) 12326);
              gclass3_1.method_31((byte) 7);
              gclass3_1.method_41(string_9);
              gclass12.gclass4_0.method_15(gclass3_1);
              gclass12.method_5(false);
            }
            else
              gclass12.method_11(string_9, (byte) 60);
          }
        }
        else if (!(Class32.string_50 == "none"))
        {
          gclass12.method_11(string_9, (byte) 60);
        }
        else
        {
          GClass3 gclass3_1 = new GClass3((ushort) 12326);
          gclass3_1.method_31((byte) 7);
          gclass3_1.method_41(string_9);
          gclass12.gclass4_0.method_15(gclass3_1);
          gclass12.method_5(false);
        }
      }
    }
    if (!(Class32.string_22 != "0"))
      return;
    foreach (GClass11 gclass11 in Class32.list_8)
    {
      if (gclass11.socket_0.Connected)
      {
        if (!Class32.bool_13)
        {
          if (Class32.string_50 == "none")
          {
            GClass3 gclass3_1 = new GClass3((ushort) 12326);
            gclass3_1.method_31((byte) 7);
            gclass3_1.method_41(string_9);
            gclass11.gclass4_0.method_15(gclass3_1);
            gclass11.method_5(false);
          }
          else
            gclass11.method_11(string_9, (byte) 60);
        }
        else if (Class32.dictionary_1.ContainsKey(gclass11.string_1) && int_9 >= Class32.dictionary_1[gclass11.string_1] && Class32.dictionary_1[gclass11.string_1] > 0)
        {
          if (Class32.string_50 == "none")
          {
            GClass3 gclass3_1 = new GClass3((ushort) 12326);
            gclass3_1.method_31((byte) 7);
            gclass3_1.method_41(string_9);
            gclass11.gclass4_0.method_15(gclass3_1);
            gclass11.method_5(false);
          }
          else
            gclass11.method_11(string_9, (byte) 60);
        }
        else if (!Class32.dictionary_1.ContainsKey(gclass11.string_1))
        {
          if (!(Class32.string_50 == "none"))
          {
            gclass11.method_11(string_9, (byte) 60);
          }
          else
          {
            GClass3 gclass3_1 = new GClass3((ushort) 12326);
            gclass3_1.method_31((byte) 7);
            gclass3_1.method_41(string_9);
            gclass11.gclass4_0.method_15(gclass3_1);
            gclass11.method_5(false);
          }
        }
      }
    }
  }

  public void method_17(string string_9)
  {
    GClass3 gclass3_1 = new GClass3((ushort) 12326);
    gclass3_1.method_31((byte) 6);
    gclass3_1.method_41(Class32.string_31);
    gclass3_1.method_41(string_9);
    this.gclass4_0.method_15(gclass3_1);
    this.method_5(false);
  }

  public void method_18(string string_9, string string_10)
  {
    GClass3 gclass3_1 = new GClass3((ushort) 12326);
    gclass3_1.method_31((byte) 16);
    gclass3_1.method_41(string_9);
    gclass3_1.method_41(string_10);
    this.gclass4_0.method_15(gclass3_1);
    this.method_5(false);
  }

  private void method_19(GClass3 gclass3_0)
  {
    if (Class32.dictionary_2.ContainsKey(Convert.ToUInt16(gclass3_0.UInt16_0.ToString().ToLower())))
      return;
    Class31.smethod_0("IP:[" + this.string_0 + "] Opcode:[0x" + gclass3_0.UInt16_0.ToString("X") + "] [EXPLOITING].", this.string_0, "non");
    this.method_7();
  }

  private void method_20()
  {
    try
    {
      if (!(DateTime.Now > this.dateTime_20))
      {
        ++this.int_1;
        if (this.int_1 < Class32.int_40 || Class32.list_15.Contains(this.string_2))
          return;
        this.method_7();
      }
      else
      {
        this.dateTime_20 = DateTime.Now.AddSeconds(1.0);
        this.int_1 = 0;
      }
    }
    catch
    {
    }
  }

  private void method_21()
  {
    if (!Class32.bool_48)
      return;
    if (this.bool_13)
    {
      if (Class32.list_65.Contains(this.string_1))
        return;
      if (Class32.string_28 == "IP")
      {
        lock (Class32.list_64)
          Class32.list_64.Add(this.string_0);
      }
      else
      {
        lock (Class32.list_64)
          Class32.list_64.Add(this.string_6);
      }
      lock (Class32.list_65)
        Class32.list_65.Add(this.string_1);
    }
    else
    {
      if (this.bool_13 || !Class32.list_65.Contains(this.string_1))
        return;
      if (Class32.string_28 == "IP")
      {
        lock (Class32.list_64)
          Class32.list_64.Remove(this.string_0);
      }
      else
      {
        lock (Class32.list_64)
          Class32.list_64.Remove(this.string_6);
      }
      lock (Class32.list_65)
        Class32.list_65.Remove(this.string_1);
    }
  }

  private void method_22()
  {
    if (!Class32.bool_39)
      return;
    if (this.bool_7)
    {
      if (!Class32.list_67.Contains(this.string_1))
      {
        if (Class32.string_27 == "IP")
        {
          lock (Class32.list_66)
            Class32.list_66.Add(this.string_0);
        }
        else
        {
          lock (Class32.list_66)
            Class32.list_66.Add(this.string_6);
        }
        lock (Class32.list_67)
          Class32.list_67.Add(this.string_1);
      }
      if (this.bool_10 && !Class32.list_69.Contains(this.string_1))
      {
        if (Class32.string_27 == "IP")
        {
          lock (Class32.list_68)
            Class32.list_68.Add(this.string_0);
        }
        else
        {
          lock (Class32.list_68)
            Class32.list_68.Add(this.string_6);
        }
        lock (Class32.list_69)
          Class32.list_69.Add(this.string_1);
      }
      if (this.bool_18 && !Class32.list_71.Contains(this.string_1))
      {
        if (Class32.string_27 == "IP")
        {
          lock (Class32.list_70)
            Class32.list_70.Add(this.string_0);
        }
        else
        {
          lock (Class32.list_70)
            Class32.list_70.Add(this.string_6);
        }
        lock (Class32.list_71)
          Class32.list_71.Add(this.string_1);
      }
      if (!this.bool_8 || Class32.list_73.Contains(this.string_1))
        return;
      if (Class32.string_27 == "IP")
      {
        lock (Class32.list_72)
          Class32.list_72.Add(this.string_0);
      }
      else
      {
        lock (Class32.list_72)
          Class32.list_72.Add(this.string_6);
      }
      lock (Class32.list_73)
        Class32.list_73.Add(this.string_1);
    }
    else
    {
      if (this.bool_7)
        return;
      if (Class32.list_67.Contains(this.string_1))
      {
        if (Class32.string_27 == "IP")
        {
          lock (Class32.list_66)
            Class32.list_66.Remove(this.string_0);
        }
        else
        {
          lock (Class32.list_66)
            Class32.list_66.Remove(this.string_6);
        }
        lock (Class32.list_67)
          Class32.list_67.Remove(this.string_1);
      }
      if (!this.bool_10 && Class32.list_69.Contains(this.string_1))
      {
        if (Class32.string_27 == "IP")
        {
          lock (Class32.list_68)
            Class32.list_68.Remove(this.string_0);
        }
        else
        {
          lock (Class32.list_68)
            Class32.list_68.Remove(this.string_6);
        }
        lock (Class32.list_69)
          Class32.list_69.Remove(this.string_1);
      }
      if (!this.bool_18 && Class32.list_71.Contains(this.string_1))
      {
        if (Class32.string_27 == "IP")
        {
          lock (Class32.list_70)
            Class32.list_70.Remove(this.string_0);
        }
        else
        {
          lock (Class32.list_70)
            Class32.list_70.Remove(this.string_6);
        }
        lock (Class32.list_71)
          Class32.list_71.Remove(this.string_1);
      }
      if (this.bool_8 || !Class32.list_73.Contains(this.string_1))
        return;
      if (Class32.string_27 == "IP")
      {
        lock (Class32.list_72)
          Class32.list_72.Remove(this.string_0);
      }
      else
      {
        lock (Class32.list_72)
          Class32.list_72.Remove(this.string_6);
      }
      lock (Class32.list_73)
        Class32.list_73.Remove(this.string_1);
    }
  }

  private void method_23()
  {
    if ((!Class32.bool_119 || Class34.smethod_7(this.string_0) <= Class32.int_41 ? 0 : (this.string_0 != Class32.string_15 ? 1 : 0)) == 0)
      return;
    if (Class32.list_60.Contains(this.string_0))
      return;
    try
    {
      Class31.smethod_0("[Agent] IP:[" + this.string_0 + "] has been disconnected Reason:[ADV IP FLOODING].", this.string_0, "non");
      this.method_7();
    }
    catch
    {
    }
  }

  private void method_24()
  {
    lock (Class32.list_1)
      Class32.list_1.Add(this.string_0);
    lock (Class32.list_8)
      Class32.list_8.Add(this);
  }

  private void method_25()
  {
    if (!Class32.list_6.Contains(this.string_0))
      return;
    this.method_7();
  }

  private void method_26()
  {
    if (Class34.smethod_7(this.string_0) <= Class32.int_39)
      return;
    if (!(this.string_0 != Class32.string_15))
      return;
    try
    {
      Class31.smethod_0("IP: [" + this.string_0 + "] has been disconnected Reason:[FLOODING].", this.string_0, "non");
      this.method_7();
    }
    catch
    {
    }
  }

  private void method_27()
  {
    lock (Class32.list_12)
      Class32.list_12.Remove(this.uint_1);
    lock (Class32.list_13)
      Class32.list_13.Remove(this.uint_1);
    lock (Class32.list_8)
      Class32.list_8.Remove(this);
    lock (Class32.list_1)
      Class32.list_1.Remove(this.string_0);
    if (Class32.list_54.Contains(this.string_2))
    {
      lock (Class32.list_54)
        Class32.list_54.Remove(this.string_2);
    }
    if (Class32.bool_99)
    {
      lock (Class32.list_44)
        Class32.list_44.Remove(this.string_6);
    }
    if (!Class32.bool_126 || !Class32.dictionary_10.ContainsKey(this.uint_1))
      return;
    lock (Class32.dictionary_10)
      Class32.dictionary_10.Remove(this.uint_1);
  }

  private void method_28()
  {
    if (Class32.bool_122 && !string.IsNullOrEmpty(this.string_1))
      GClass7.smethod_0("EXEC [_PlayersLogging] '" + this.string_2 + "','" + this.string_0 + "','" + this.string_6 + "','" + this.string_1 + "','OFFLINE'");
    if (this.bool_25)
    {
      --Class32.int_0;
      Console.Title = "EverNow [Filter] : ONLINE PLAYER(S) [" + (object) Class32.int_0 + "]";
      Class18.smethod_1("ONLINE," + (object) Class32.int_0 ?? "");
    }
    if (!this.bool_19)
      return;
    --Class32.int_37;
    Class18.smethod_1("THIRDPART," + (object) Class32.int_37 ?? "");
  }

  private void method_29()
  {
    if (!this.bool_27)
      return;
    if (Class32.int_38 > 0)
    {
      --Class32.int_38;
      Class18.smethod_1("CLIENTLESS," + (object) Class32.int_38 ?? "");
    }
    if (!Class32.list_42.Contains(this.string_2))
      return;
    lock (Class32.list_42)
      Class32.list_42.Remove(this.string_2);
  }

  private void method_30()
  {
    if (!Class32.bool_48 || !this.bool_13 || !Class32.list_65.Contains(this.string_1))
      return;
    if (Class32.string_28 == "IP")
    {
      lock (Class32.list_64)
        Class32.list_64.Remove(this.string_0);
    }
    else
    {
      lock (Class32.list_64)
        Class32.list_64.Remove(this.string_6);
    }
    lock (Class32.list_65)
      Class32.list_65.Remove(this.string_1);
  }

  private void method_31()
  {
    if (!Class32.bool_39 || !this.bool_7)
      return;
    if (Class32.list_67.Contains(this.string_1))
    {
      if (Class32.string_27 == "IP")
      {
        lock (Class32.list_66)
          Class32.list_66.Remove(this.string_0);
      }
      else
      {
        lock (Class32.list_66)
          Class32.list_66.Remove(this.string_6);
      }
      lock (Class32.list_67)
        Class32.list_67.Remove(this.string_1);
    }
    if (this.bool_10 && Class32.list_69.Contains(this.string_1))
    {
      if (Class32.string_27 == "IP")
      {
        lock (Class32.list_68)
          Class32.list_68.Remove(this.string_0);
      }
      else
      {
        lock (Class32.list_68)
          Class32.list_68.Remove(this.string_6);
      }
      lock (Class32.list_69)
        Class32.list_69.Remove(this.string_1);
    }
    if (this.bool_18 && Class32.list_71.Contains(this.string_1))
    {
      if (Class32.string_27 == "IP")
      {
        lock (Class32.list_70)
          Class32.list_70.Remove(this.string_0);
      }
      else
      {
        lock (Class32.list_70)
          Class32.list_70.Remove(this.string_6);
      }
      lock (Class32.list_71)
        Class32.list_71.Remove(this.string_1);
    }
    if (!this.bool_8 || !Class32.list_73.Contains(this.string_1))
      return;
    if (Class32.string_27 == "IP")
    {
      lock (Class32.list_72)
        Class32.list_72.Remove(this.string_0);
    }
    else
    {
      lock (Class32.list_72)
        Class32.list_72.Remove(this.string_6);
    }
    lock (Class32.list_73)
      Class32.list_73.Remove(this.string_1);
  }

  private void method_32()
  {
    this.int_4 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',15,0");
  }

  private void method_33()
  {
    if (Class32.list_9.Contains(this.int_4))
      this.bool_13 = true;
    else
      this.bool_13 = false;
  }

  private void method_34()
  {
    if (!Class32.list_10.Contains(this.int_4))
      this.bool_5 = false;
    else
      this.bool_5 = true;
  }

  private void method_35()
  {
    if ((Class32.bool_26 || Class32.bool_22 || (Class32.bool_37 || Class32.bool_38) || Class32.bool_29 ? 1 : (Class32.bool_39 ? 1 : 0)) == 0)
      return;
    switch (GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',0,0"))
    {
      case 0:
        this.bool_7 = false;
        this.bool_8 = false;
        this.bool_18 = false;
        this.bool_10 = false;
        if (!Class32.list_13.Contains(this.uint_1))
          break;
        lock (Class32.list_13)
        {
          Class32.list_13.Remove(this.uint_1);
          break;
        }
      case 1:
        this.bool_7 = true;
        this.bool_8 = false;
        this.bool_18 = true;
        this.bool_10 = false;
        int num = !Class32.list_13.Contains(this.uint_1) ? 1 : 0;
        break;
      case 2:
        this.bool_7 = true;
        this.bool_8 = true;
        this.bool_18 = false;
        this.bool_10 = false;
        if (Class32.list_13.Contains(this.uint_1))
          break;
        lock (Class32.list_13)
        {
          Class32.list_13.Add(this.uint_1);
          break;
        }
      case 3:
        this.bool_7 = true;
        this.bool_8 = false;
        this.bool_18 = false;
        this.bool_10 = true;
        if (Class32.list_13.Contains(this.uint_1))
          break;
        lock (Class32.list_13)
        {
          Class32.list_13.Add(this.uint_1);
          break;
        }
    }
  }

  private void method_36()
  {
    if ((!Class32.bool_39 ? 0 : (this.bool_7 ? 1 : 0)) == 0)
      return;
    if (Class34.smethod_3(this.string_6, this.string_0) > Class32.int_8)
      this.bool_30 = true;
    else if (this.bool_18)
    {
      if (Class34.smethod_4(this.string_6, this.string_0) <= Class32.int_9)
        return;
      this.bool_30 = true;
    }
    else if (!this.bool_10)
    {
      if (!this.bool_8 || Class34.smethod_6(this.string_6, this.string_0) <= Class32.int_11)
        return;
      this.bool_30 = true;
    }
    else
    {
      if (Class34.smethod_5(this.string_6, this.string_0) <= Class32.int_10)
        return;
      this.bool_30 = true;
    }
  }

  private void method_37()
  {
    if (!Class32.bool_48 || Class34.smethod_2(this.string_6, this.string_0) <= Class32.int_12 || (Class32.list_15.Contains(this.string_2) || !this.bool_13))
      return;
    this.method_7();
  }

  private void method_38()
  {
    if (!Class32.bool_81 || !this.bool_15)
      return;
    this.bool_15 = false;
  }

  private void method_39()
  {
    if (Class32.bool_67 && !Class32.list_15.Contains(this.string_2) && (this.bool_19 && this.bool_7))
      this.method_7();
    else if (Class32.bool_68 && !Class32.list_15.Contains(this.string_2) && (this.bool_19 ? (this.bool_5 ? 1 : 0) : 0) != 0)
      this.method_7();
    else if (Class32.bool_65 && !Class32.list_15.Contains(this.string_2) && (!this.bool_19 ? 0 : (this.bool_13 ? 1 : 0)) != 0)
    {
      this.method_7();
    }
    else
    {
      if (!Class32.bool_73 || Class32.list_15.Contains(this.string_2) || (!this.bool_19 || !Class32.list_22.Contains(this.int_4)))
        return;
      this.method_7();
    }
  }

  private void method_40()
  {
    if (Convert.ToInt32(Class32.int_34) <= 0 || GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',8,0") != 514)
      return;
    this.bool_4 = true;
  }

  private void method_41()
  {
    if (!Class32.bool_81 || Class32.list_15.Contains(this.string_2) || !Class32.list_21.Contains(this.int_4))
      return;
    this.dateTime_15 = DateTime.Now;
  }

  private void method_42()
  {
    this.int_5 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',2,0");
  }

  private void method_43()
  {
    if (!Class32.bool_126 || !Class32.dictionary_10.ContainsKey(this.uint_1))
      return;
    lock (Class32.dictionary_10)
      Class32.dictionary_10.Remove(this.uint_1);
  }

  private void method_44()
  {
    this.int_2 = GClass7.smethod_4("EXEC [_Functions] '" + this.string_1 + "',16,0");
  }

  private void method_45()
  {
    if (this.int_4 == -32759 && !this.bool_6)
      this.bool_6 = true;
    else
      this.bool_6 = false;
  }
}
