﻿// Decompiled with JetBrains decompiler
// Type: GClass2
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Runtime.InteropServices;

public static class GClass2
{
  public static byte[] smethod_0(this object object_0)
  {
    int length = Marshal.SizeOf(object_0);
    byte[] destination = new byte[length];
    IntPtr num = Marshal.AllocHGlobal(length);
    Marshal.StructureToPtr(object_0, num, true);
    Marshal.Copy(num, destination, 0, length);
    Marshal.FreeHGlobal(num);
    return destination;
  }
}
