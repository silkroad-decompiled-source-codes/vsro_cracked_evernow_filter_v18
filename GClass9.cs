﻿// Decompiled with JetBrains decompiler
// Type: GClass9
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public sealed class GClass9
{
  private Socket socket_0 = (Socket) null;
  private Thread thread_0 = (Thread) null;
  private ManualResetEvent manualResetEvent_0 = new ManualResetEvent(false);
  private GClass9.GEnum9 genum9_0;

  public bool method_0(string string_0, int int_0, GClass9.GEnum9 genum9_1)
  {
    bool flag1 = false;
    bool flag2;
    if (this.socket_0 == null)
    {
      this.genum9_0 = genum9_1;
      this.socket_0 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      try
      {
        this.socket_0.Bind((EndPoint) new IPEndPoint(IPAddress.Parse(string_0), int_0));
        this.socket_0.Listen(5);
        this.thread_0 = new Thread(new ThreadStart(this.method_1));
        this.thread_0.Start();
        Class18.smethod_0("listening to [IP:" + string_0 + "][Port:" + (object) int_0 + "]", "Cyan");
        flag1 = true;
      }
      catch (SocketException ex)
      {
        Class18.smethod_0("Could not bind/listen/BeginAccept socket.[" + ex.ToString() + "]", "Red");
      }
      flag2 = flag1;
    }
    else
      flag2 = false;
    return flag2;
  }

  private void method_1()
  {
    while (this.socket_0 != null)
    {
      this.manualResetEvent_0.Reset();
      try
      {
        this.socket_0.BeginAccept(new AsyncCallback(this.method_2), (object) null);
      }
      catch (Exception ex)
      {
        Class18.smethod_0(ex.ToString(), "Red");
      }
      this.manualResetEvent_0.WaitOne();
    }
  }

  private void method_2(IAsyncResult iasyncResult_0)
  {
    Socket socket_2 = (Socket) null;
    this.manualResetEvent_0.Set();
    try
    {
      socket_2 = this.socket_0.EndAccept(iasyncResult_0);
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      switch (this.genum9_0)
      {
        case GClass9.GEnum9.GatewayServer:
          GClass10 gclass10 = new GClass10(socket_2, new GClass9.GDelegate0(this.method_3));
          break;
        case GClass9.GEnum9.AgentServer:
          GClass12 gclass12 = new GClass12(socket_2, new GClass9.GDelegate0(this.method_3));
          break;
        case GClass9.GEnum9.AgentServer2:
          GClass11 gclass11 = new GClass11(socket_2, new GClass9.GDelegate0(this.method_3));
          break;
      }
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
  }

  public void method_3(ref Socket socket_1, GClass9.GEnum9 genum9_1)
  {
    if (socket_1 == null)
      return;
    try
    {
      socket_1.Close();
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    socket_1 = (Socket) null;
    GC.Collect();
  }

  public enum GEnum9 : byte
  {
    GatewayServer,
    AgentServer,
    AgentServer2,
  }

  public delegate void GDelegate0(ref Socket ClientSocket, GClass9.GEnum9 HandlerType);
}
