﻿// Decompiled with JetBrains decompiler
// Type: GInterface17
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("8267BBE3-F890-491C-B7B6-2DB1EF0E5D2B")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface17
{
  [DispId(3)]
  GInterface15 GInterface15_0 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_0([MarshalAs(UnmanagedType.BStr), In] string string_0, [MarshalAs(UnmanagedType.BStr), In] string string_1, [In] bool bool_0, [In] bool bool_1);

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  bool imethod_1([MarshalAs(UnmanagedType.BStr), In] string string_0, [MarshalAs(UnmanagedType.BStr), In] string string_1);
}
