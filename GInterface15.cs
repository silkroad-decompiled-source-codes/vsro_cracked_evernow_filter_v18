﻿// Decompiled with JetBrains decompiler
// Type: GInterface15
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("9C4C6277-5027-441E-AFAE-CA1F542DA009")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface15 : IEnumerable
{
  [DispId(1)]
  int Int32_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_0([MarshalAs(UnmanagedType.Interface), In] GInterface12 ginterface12_0);

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_1([MarshalAs(UnmanagedType.BStr), In] string string_0);

  [DispId(4)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  GInterface12 imethod_2([MarshalAs(UnmanagedType.BStr), In] string string_0);

  [DispId(-4)]
  [TypeLibFunc(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.CustomMarshaler)]
  IEnumerator imethod_3();
}
