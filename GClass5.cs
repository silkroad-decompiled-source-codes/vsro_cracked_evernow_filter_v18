﻿// Decompiled with JetBrains decompiler
// Type: GClass5
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;

public class GClass5
{
  private byte[] byte_0;
  private int int_0;
  private int int_1;
  private object object_0;

  public byte[] Byte_0
  {
    get
    {
      return this.byte_0;
    }
    set
    {
      lock (this.object_0)
        this.byte_0 = value;
    }
  }

  public int Int32_0
  {
    get
    {
      return this.int_0;
    }
    set
    {
      lock (this.object_0)
        this.int_0 = value;
    }
  }

  public int Int32_1
  {
    get
    {
      return this.int_1;
    }
    set
    {
      lock (this.object_0)
        this.int_1 = value;
    }
  }

  public GClass5(GClass5 gclass5_0)
  {
    lock (gclass5_0.object_0)
    {
      this.byte_0 = new byte[gclass5_0.byte_0.Length];
      Buffer.BlockCopy((Array) gclass5_0.byte_0, 0, (Array) this.byte_0, 0, this.byte_0.Length);
      this.int_0 = gclass5_0.int_0;
      this.int_1 = gclass5_0.int_1;
      this.object_0 = new object();
    }
  }

  public GClass5()
  {
    this.byte_0 = (byte[]) null;
    this.int_0 = 0;
    this.int_1 = 0;
    this.object_0 = new object();
  }

  public GClass5(int int_2, int int_3, int int_4)
  {
    this.byte_0 = new byte[int_2];
    this.int_0 = int_3;
    this.int_1 = int_4;
    this.object_0 = new object();
  }

  public GClass5(int int_2)
  {
    this.byte_0 = new byte[int_2];
    this.int_0 = 0;
    this.int_1 = 0;
    this.object_0 = new object();
  }

  public GClass5(byte[] byte_1, int int_2, int int_3, bool bool_0)
  {
    if (bool_0)
    {
      this.byte_0 = byte_1;
    }
    else
    {
      this.byte_0 = new byte[byte_1.Length];
      Buffer.BlockCopy((Array) byte_1, 0, (Array) this.byte_0, 0, byte_1.Length);
    }
    this.int_0 = int_2;
    this.int_1 = int_3;
    this.object_0 = new object();
  }
}
