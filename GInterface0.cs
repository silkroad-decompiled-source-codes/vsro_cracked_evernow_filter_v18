﻿// Decompiled with JetBrains decompiler
// Type: GInterface0
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("B5E64FFA-C2C5-444E-A301-FB5E00018050")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface0
{
  [DispId(1)]
  string String_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(2)]
  string String_1 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(3)]
  GEnum2 GEnum2_0 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(4)]
  GEnum7 GEnum7_0 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  string String_2 { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(6)]
  bool Boolean_0 { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] set; }
}
