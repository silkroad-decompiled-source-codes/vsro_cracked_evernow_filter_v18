﻿// Decompiled with JetBrains decompiler
// Type: GInterface6
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("D46D2478-9AC9-4008-9DC7-5563CE5536CC")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface6
{
  [DispId(1)]
  GInterface10 GInterface10_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  GInterface10 imethod_0([In] GEnum5 genum5_0);
}
