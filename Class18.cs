﻿// Decompiled with JetBrains decompiler
// Type: Class18
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;

internal class Class18
{
  public static void smethod_0(string string_0, string string_1)
  {
    string string_0_1 = string_1;
    // ISSUE: reference to a compiler-generated method
    uint num = Class39.smethod_0(string_0_1);
    if (num > 2743015548U)
    {
      switch (num)
      {
        case 2840840028:
          if (string_0_1 == "Green")
          {
            Console.ForegroundColor = ConsoleColor.Green;
            break;
          }
          break;
        case 3381604954:
          if (string_0_1 == "Cyan")
          {
            Console.ForegroundColor = ConsoleColor.Cyan;
            break;
          }
          break;
        case 3608805453:
          if (string_0_1 == "DarkBlue")
          {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            break;
          }
          break;
        case 3654151273:
          if (string_0_1 == "Yellow")
          {
            Console.ForegroundColor = ConsoleColor.Yellow;
            break;
          }
          break;
        case 3923582957:
          if (string_0_1 == "Blue")
          {
            Console.ForegroundColor = ConsoleColor.Blue;
            break;
          }
          break;
        case 3966268476:
          if (string_0_1 == "DarkGreen")
          {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            break;
          }
          break;
      }
    }
    else
    {
      switch (num)
      {
        case 267312220:
          if (string_0_1 == "DarkRed")
          {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            break;
          }
          break;
        case 382078856:
          if (string_0_1 == "Magenta")
          {
            Console.ForegroundColor = ConsoleColor.Magenta;
            break;
          }
          break;
        case 986120122:
          if (string_0_1 == "Gray")
          {
            Console.ForegroundColor = ConsoleColor.Gray;
            break;
          }
          break;
        case 1920678155:
          if (string_0_1 == "Orange")
          {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            break;
          }
          break;
        case 2642369978:
          if (string_0_1 == "DarkCyan")
          {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            break;
          }
          break;
        case 2743015548:
          if (string_0_1 == "Red")
          {
            Console.ForegroundColor = ConsoleColor.Red;
            break;
          }
          break;
      }
    }
    Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + string_0);
    Console.ResetColor();
    if (string_1 == "Red")
    {
      if (!System.IO.File.Exists("logs/Error/" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt"))
      {
        try
        {
          System.IO.File.Create("logs/Error/" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt").Close();
        }
        catch
        {
        }
      }
      try
      {
        StreamWriter streamWriter = new StreamWriter("logs/Error/" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt", true);
        streamWriter.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + string_0);
        streamWriter.Close();
      }
      catch
      {
      }
    }
    else
    {
      if (!(string_1 == "Magenta"))
        return;
      if (!System.IO.File.Exists("logs/Exploits/" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt"))
      {
        try
        {
          System.IO.File.Create("logs/Exploits/" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt").Close();
        }
        catch
        {
        }
      }
      try
      {
        StreamWriter streamWriter = new StreamWriter("logs/Exploits/" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt", true);
        streamWriter.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + string_0);
        streamWriter.Close();
      }
      catch
      {
      }
    }
  }

  public static void smethod_1(string string_0)
  {
    if (!System.IO.File.Exists("Setting(s)/[Settings][Log].txt"))
    {
      try
      {
        System.IO.File.Create("Setting(s)/[Settings][Log].txt").Close();
      }
      catch
      {
      }
    }
    try
    {
      StreamWriter streamWriter = new StreamWriter("Setting(s)/[Settings][Log].txt", true);
      streamWriter.WriteLine(string_0);
      streamWriter.Close();
    }
    catch
    {
    }
  }

  public static void smethod_2(string string_0)
  {
    try
    {
      if (!Class32.bool_134)
        return;
      string string57 = Class32.string_57;
      using (WebClient webClient = new WebClient())
        webClient.UploadValues(string57, "POST", new NameValueCollection()
        {
          ["content"] = string_0
        });
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
  }
}
