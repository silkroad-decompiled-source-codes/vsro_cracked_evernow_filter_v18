﻿// Decompiled with JetBrains decompiler
// Type: GClass10
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

public class GClass10
{
  private Socket socket_0 = (Socket) null;
  private object object_0 = new object();
  private Socket socket_1 = (Socket) null;
  private byte[] byte_0 = new byte[8192];
  private byte[] byte_1 = new byte[8192];
  private GClass4 gclass4_0 = new GClass4();
  private GClass4 gclass4_1 = new GClass4();
  private ulong ulong_0 = 0;
  private DateTime dateTime_0 = DateTime.Now;
  private int int_0 = 0;
  private int int_1 = 0;
  private int int_2 = 0;
  private int int_3 = 0;
  private bool bool_0 = false;
  private bool bool_1 = false;
  private bool bool_2 = true;
  private bool bool_3 = false;
  private DateTime dateTime_1 = DateTime.Now;
  private GClass9.GDelegate0 gdelegate0_0;
  public GClass9.GEnum9 genum9_0;
  private string string_0;
  private string string_1;
  private string string_2;
  private string string_3;
  private string string_4;
  private string string_5;

  private int method_0()
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_0))
      {
        if (str == this.string_3)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  private int method_1()
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_1))
      {
        if (str == this.string_3)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  private int method_2()
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_44))
      {
        if (str == this.string_1)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public GClass10(Socket socket_2, GClass9.GDelegate0 gdelegate0_1)
  {
    try
    {
      this.gdelegate0_0 = gdelegate0_1;
      this.socket_0 = socket_2;
      this.genum9_0 = GClass9.GEnum9.GatewayServer;
      this.socket_1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      this.string_3 = ((IPEndPoint) socket_2.RemoteEndPoint).Address.ToString();
      lock (Class32.list_0)
        Class32.list_0.Add(this.string_3);
      if (Class32.bool_119 && this.method_0() > Class32.int_41 && this.string_3 != Class32.string_15)
      {
        if (!Class32.list_60.Contains(this.string_3))
        {
          try
          {
            Class31.smethod_0("[Gateway] IP:[" + this.string_3 + "] has been disconnected Reason:[ADV IP FLOODING].", this.string_3, "non");
            this.method_9();
            return;
          }
          catch
          {
          }
        }
      }
      try
      {
        this.socket_1.Connect((EndPoint) new IPEndPoint(IPAddress.Parse(Class32.string_15), Convert.ToInt32(Class32.string_16)));
        this.gclass4_0.method_13(true, true, true);
        this.method_8();
        this.method_5(false);
        if (Class32.list_6.Contains(this.string_3))
        {
          this.method_9();
        }
        else
        {
          if (this.method_0() <= Class32.int_39)
            return;
          if (!(this.string_3 != Class32.string_15))
            return;
          try
          {
            Class31.smethod_0("IP:[" + this.string_3 + "] has been disconnected Reason:[IP FLOODING].", this.string_3, "non");
            this.method_9();
          }
          catch
          {
          }
        }
      }
      catch
      {
        this.method_9();
      }
    }
    catch
    {
    }
  }

  private void method_3(IAsyncResult iasyncResult_0)
  {
    lock (this.object_0)
    {
      try
      {
        int int_1 = this.socket_0.EndReceive(iasyncResult_0);
        if ((uint) int_1 > 0U)
        {
          this.gclass4_0.method_16(this.byte_0, 0, int_1);
          List<GClass3> gclass3List = this.gclass4_0.method_19();
          if (gclass3List != null)
          {
            foreach (GClass3 gclass3_1_1 in gclass3List)
            {
              this.method_10();
              if (Class32.dictionary_2.ContainsKey(Convert.ToUInt16(gclass3_1_1.UInt16_0.ToString().ToLower())))
              {
                ushort uint160 = gclass3_1_1.UInt16_0;
                if (uint160 > (ushort) 8194)
                {
                  switch (uint160)
                  {
                    case 20480:
                    case 36864:
                      this.method_5(false);
                      continue;
                    case 24832:
                      this.int_0 = 1;
                      this.gclass4_1.method_15(gclass3_1_1);
                      this.method_5(true);
                      continue;
                    case 24833:
                      this.int_1 = 1;
                      try
                      {
                        if (!Class32.list_60.Contains(this.string_3))
                        {
                          lock (Class32.list_60)
                            Class32.list_60.Add(this.string_3);
                        }
                      }
                      catch
                      {
                      }
                      if (Class32.bool_3)
                      {
                        int num = Class32.int_0 + Convert.ToInt32(Class32.string_3);
                        if (num >= Convert.ToInt32(Class32.string_4))
                          num = Convert.ToInt32(Class32.string_4);
                        GClass3 gclass3_1_2 = new GClass3((ushort) 41217, true);
                        gclass3_1_2.method_31((byte) 1);
                        gclass3_1_2.method_31((byte) 20);
                        gclass3_1_2.method_41("SRO_Vietnam_TestLocal [F] 0");
                        gclass3_1_2.method_31((byte) 0);
                        gclass3_1_2.method_31((byte) 1);
                        gclass3_1_2.method_46((object) Class32.string_5);
                        gclass3_1_2.method_41(Class32.string_33);
                        gclass3_1_2.method_46((object) num);
                        gclass3_1_2.method_46((object) Class32.string_4);
                        gclass3_1_2.method_31((byte) 1);
                        gclass3_1_2.method_31((byte) 20);
                        gclass3_1_2.method_31((byte) 0);
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                        continue;
                      }
                      this.gclass4_1.method_15(gclass3_1_1);
                      this.method_5(true);
                      continue;
                    case 24834:
                      try
                      {
                        if (!Class32.list_60.Contains(this.string_3))
                        {
                          lock (Class32.list_60)
                            Class32.list_60.Add(this.string_3);
                        }
                      }
                      catch
                      {
                      }
                      try
                      {
                        int num1 = (int) gclass3_1_1.method_4();
                        this.string_4 = gclass3_1_1.method_14().ToLower();
                        this.string_5 = gclass3_1_1.method_14();
                        int num2 = (int) gclass3_1_1.method_6();
                        if (this.int_1 == 1 && this.int_0 == 1 || !(this.string_3 != Class32.string_15))
                        {
                          if (Class32.bool_99 && Class32.list_40.Contains(this.string_1))
                          {
                            this.method_9();
                            continue;
                          }
                          if (!Class32.list_14.Contains(this.string_4))
                          {
                            if ((this.string_4.Contains("'") ? 1 : (this.string_4.Contains("\"") ? 1 : 0)) != 0)
                            {
                              Class18.smethod_0("IP: [" + this.string_3 + "] UserID: [" + Class17.smethod_1(this.string_4) + "] attempted to SQL inject StrUserID at 0x6103", "Magenta");
                              this.method_9();
                              continue;
                            }
                            if (GClass10.smethod_0(this.string_4, Class23.smethod_0(this.string_5)))
                            {
                              if (Class32.list_53.Contains(this.string_3))
                              {
                                this.bool_1 = false;
                                if (Class32.bool_61 && Class32.list_54.Contains(this.string_4))
                                  Class32.list_54.Remove(this.string_4);
                              }
                              else
                              {
                                this.bool_1 = true;
                                if (Class32.bool_61 && !Class32.list_54.Contains(this.string_4))
                                  Class32.list_54.Add(this.string_4);
                              }
                              Class32.list_53.Remove(this.string_3);
                              if ((!Class32.bool_99 ? 0 : (this.string_3 != Class32.string_15 ? 1 : 0)) != 0)
                              {
                                try
                                {
                                  if (!this.bool_3)
                                  {
                                    if (!this.bool_2)
                                    {
                                      GClass3 gclass3_1_2 = new GClass3((ushort) 41218, false);
                                      gclass3_1_2.method_31((byte) 2);
                                      gclass3_1_2.method_31((byte) 12);
                                      this.gclass4_0.method_15(gclass3_1_2);
                                      this.method_5(false);
                                      this.method_9();
                                      continue;
                                    }
                                    if (Class32.dictionary_8.ContainsKey(this.string_4))
                                      this.string_1 = Class32.dictionary_8[this.string_4];
                                  }
                                }
                                catch
                                {
                                }
                              }
                              if ((!Class32.bool_1 || Class32.list_41.Contains(this.string_4) ? 0 : (!Class32.list_15.Contains(this.string_4) ? 1 : 0)) == 0)
                              {
                                if (Class32.bool_2 && Class32.dictionary_6.ContainsKey(this.string_4) && this.string_3 != Class32.dictionary_6[this.string_4])
                                {
                                  Class18.smethod_0("Disconnect IP:[" + this.string_3 + "] User:[" + this.string_4 + "] Reason:[try to open GM Account].", "Yellow");
                                  this.method_9();
                                  continue;
                                }
                                if (Class32.bool_98 && !Class32.list_15.Contains(this.string_4))
                                {
                                  if (!Class32.dictionary_7.ContainsKey(this.string_3))
                                  {
                                    if (this.method_1() >= Convert.ToInt32(Class32.string_44))
                                    {
                                      GClass3 gclass3_1_2 = new GClass3((ushort) 41218, false);
                                      gclass3_1_2.method_31((byte) 2);
                                      gclass3_1_2.method_31((byte) 8);
                                      this.gclass4_0.method_15(gclass3_1_2);
                                      this.method_5(false);
                                      this.method_9();
                                      continue;
                                    }
                                  }
                                  else if (this.method_1() >= Class32.dictionary_7[this.string_3])
                                  {
                                    GClass3 gclass3_1_2 = new GClass3((ushort) 41218, false);
                                    gclass3_1_2.method_31((byte) 2);
                                    gclass3_1_2.method_31((byte) 8);
                                    this.gclass4_0.method_15(gclass3_1_2);
                                    this.method_5(false);
                                    this.method_9();
                                    continue;
                                  }
                                }
                                if (Class32.bool_99 && !Class32.list_15.Contains(this.string_4) && this.string_3 != Class32.string_15)
                                {
                                  if (this.bool_3)
                                  {
                                    try
                                    {
                                      if (this.method_2() >= Convert.ToInt32(Class32.string_45))
                                      {
                                        GClass3 gclass3_1_2 = new GClass3((ushort) 41218, false);
                                        gclass3_1_2.method_31((byte) 2);
                                        gclass3_1_2.method_31((byte) 10);
                                        this.gclass4_0.method_15(gclass3_1_2);
                                        this.method_5(false);
                                        this.method_9();
                                        continue;
                                      }
                                      if (!Class32.dictionary_8.ContainsKey(this.string_4))
                                      {
                                        Class32.dictionary_8.Add(this.string_4, this.string_1);
                                      }
                                      else
                                      {
                                        Class32.dictionary_8.Remove(this.string_4);
                                        Class32.dictionary_8.Add(this.string_4, this.string_1);
                                      }
                                    }
                                    catch
                                    {
                                    }
                                  }
                                }
                                if (this.bool_2)
                                {
                                  if (!Class32.list_42.Contains(this.string_4))
                                    Class32.list_42.Add(this.string_4);
                                }
                                else if (!this.bool_2 && Class32.list_42.Contains(this.string_4))
                                  Class32.list_42.Remove(this.string_4);
                                if (Class32.bool_120 && this.bool_2 && !Class32.list_15.Contains(this.string_4))
                                {
                                  this.method_9();
                                  continue;
                                }
                                if (Class32.bool_3)
                                {
                                  if (Class32.int_0 >= int.Parse(Class32.string_4))
                                  {
                                    this.method_9();
                                    continue;
                                  }
                                }
                              }
                              else
                              {
                                this.method_9();
                                continue;
                              }
                            }
                          }
                          else
                          {
                            this.method_9();
                            continue;
                          }
                        }
                        else
                        {
                          this.method_9();
                          continue;
                        }
                      }
                      catch
                      {
                      }
                      this.gclass4_1.method_15(gclass3_1_1);
                      this.method_5(true);
                      continue;
                    case 24836:
                      if (!Class32.list_53.Contains(this.string_3))
                        Class32.list_53.Add(this.string_3);
                      this.gclass4_1.method_15(gclass3_1_1);
                      this.method_5(true);
                      continue;
                    case 24838:
                      this.bool_2 = false;
                      this.gclass4_1.method_15(gclass3_1_1);
                      this.method_5(true);
                      continue;
                    case 32769:
                      if (Class32.bool_99)
                      {
                        this.bool_3 = true;
                        this.string_1 = gclass3_1_1.method_14().Trim().ToUpper();
                        this.string_1 = Class23.smethod_0(this.string_1);
                        this.method_5(false);
                        continue;
                      }
                      continue;
                  }
                }
                else
                {
                  switch (uint160)
                  {
                    case 5200:
                      if (Class32.bool_99)
                      {
                        this.bool_3 = true;
                        this.string_2 = gclass3_1_1.method_14().Trim();
                        this.string_1 = gclass3_1_1.method_14().Trim().ToUpper();
                        this.string_0 = gclass3_1_1.method_14().Trim();
                        string str = gclass3_1_1.method_14().Trim();
                        if (this.string_1.Length != 17)
                        {
                          this.method_9();
                          continue;
                        }
                        for (int index = 0; index < 6; ++index)
                        {
                          if (this.string_1.Split('-')[index].Length != 2)
                          {
                            this.method_9();
                            break;
                          }
                        }
                        if (this.string_1.Split('-').Length != 6)
                        {
                          this.method_9();
                          continue;
                        }
                        if (str.Contains("2019"))
                        {
                          if (!(Class23.smethod_0("_ABDELRHMANEverNow_FilterBATTAWY_" + this.string_1 + str.Replace("2019", "0001")) != this.string_2))
                          {
                            this.string_1 = Class23.smethod_0(this.string_1);
                            this.method_5(false);
                            continue;
                          }
                          this.method_9();
                          continue;
                        }
                        this.method_9();
                        continue;
                      }
                      continue;
                    case 8193:
                      this.int_2 = gclass3_1_1.method_0().Length;
                      if (this.int_2 == 12)
                      {
                        this.method_7();
                        continue;
                      }
                      this.method_9();
                      continue;
                    case 8194:
                      this.int_2 = gclass3_1_1.method_0().Length;
                      if ((uint) this.int_2 <= 0U)
                      {
                        this.gclass4_1.method_15(gclass3_1_1);
                        this.method_5(true);
                        continue;
                      }
                      continue;
                  }
                }
                this.gclass4_1.method_15(gclass3_1_1);
                this.method_5(true);
              }
              else
              {
                Class31.smethod_0("IP:[" + this.string_3 + "] Opcode:[0x" + gclass3_1_1.UInt16_0.ToString("X") + "] [EXPLOITING].", this.string_3, "non");
                this.method_9();
                break;
              }
            }
          }
          this.method_8();
        }
        else
        {
          try
          {
            this.method_9();
          }
          catch
          {
          }
          this.gdelegate0_0(ref this.socket_0, this.genum9_0);
        }
      }
      catch
      {
        try
        {
          this.method_9();
        }
        catch
        {
        }
        this.gdelegate0_0(ref this.socket_0, this.genum9_0);
      }
    }
  }

  private void method_4(IAsyncResult iasyncResult_0)
  {
    lock (this.object_0)
    {
      try
      {
        int int_1 = this.socket_1.EndReceive(iasyncResult_0);
        if ((uint) int_1 <= 0U)
          return;
        this.gclass4_1.method_16(this.byte_1, 0, int_1);
        List<GClass3> gclass3List = this.gclass4_1.method_19();
        if (gclass3List != null)
        {
          foreach (GClass3 gclass3_1_1 in gclass3List)
          {
            if ((gclass3_1_1.UInt16_0 == (ushort) 20480 ? 1 : (gclass3_1_1.UInt16_0 == (ushort) 36864 ? 1 : 0)) != 0)
            {
              this.method_5(true);
            }
            else
            {
              if (gclass3_1_1.UInt16_0 == (ushort) 41218)
              {
                try
                {
                  if (!Class32.list_60.Contains(this.string_3))
                  {
                    lock (Class32.list_60)
                      Class32.list_60.Add(this.string_3);
                  }
                }
                catch
                {
                }
                byte byte_1 = gclass3_1_1.method_4();
                if (byte_1 == (byte) 1)
                {
                  uint uint_0 = gclass3_1_1.method_8();
                  string str = gclass3_1_1.method_14();
                  int num = (int) gclass3_1_1.method_6();
                  GClass3 gclass3_1_2 = new GClass3((ushort) 41218, true);
                  gclass3_1_2.method_31(byte_1);
                  gclass3_1_2.method_35(uint_0);
                  try
                  {
                    if ((!(Class32.string_21 != "0") ? 0 : (Class32.string_19 != "0" ? 1 : 0)) == 0)
                    {
                      if (!Class32.bool_117)
                      {
                        gclass3_1_2.method_41(Class32.string_15);
                        gclass3_1_2.method_46((object) Class32.string_20);
                        gclass3_1_2.method_35(0U);
                        gclass3_1_2.method_1();
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                        continue;
                      }
                      gclass3_1_2.method_41(Class32.string_48);
                      gclass3_1_2.method_46((object) Class32.string_20);
                      gclass3_1_2.method_35(0U);
                      gclass3_1_2.method_1();
                      this.gclass4_0.method_15(gclass3_1_2);
                      this.method_5(false);
                      continue;
                    }
                    if (Class32.bool_117)
                    {
                      if (str == Class32.string_15)
                      {
                        gclass3_1_2.method_41(Class32.string_48);
                        gclass3_1_2.method_46((object) Class32.string_20);
                        gclass3_1_2.method_35(0U);
                        gclass3_1_2.method_1();
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                        continue;
                      }
                      if (str == Class32.string_22)
                      {
                        gclass3_1_2.method_41(Class32.string_49);
                        gclass3_1_2.method_46((object) Class32.string_21);
                        gclass3_1_2.method_35(0U);
                        gclass3_1_2.method_1();
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                        continue;
                      }
                    }
                    else
                    {
                      if (str == Class32.string_15)
                      {
                        gclass3_1_2.method_41(Class32.string_15);
                        gclass3_1_2.method_46((object) Class32.string_20);
                        gclass3_1_2.method_35(0U);
                        gclass3_1_2.method_1();
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                        continue;
                      }
                      if (str == Class32.string_22)
                      {
                        gclass3_1_2.method_41(Class32.string_15);
                        gclass3_1_2.method_46((object) Class32.string_21);
                        gclass3_1_2.method_35(0U);
                        gclass3_1_2.method_1();
                        this.gclass4_0.method_15(gclass3_1_2);
                        this.method_5(false);
                        continue;
                      }
                    }
                  }
                  catch
                  {
                    Class18.smethod_0("This User [" + this.string_4 + "] couldn't connect to server, wrong port binding or trying to exploit?", "Red");
                    this.method_9();
                    return;
                  }
                }
              }
              else
              {
                if (gclass3_1_1.UInt16_0 == (ushort) 8994 && Class32.bool_0)
                {
                  GClass3 gclass3_1_2 = new GClass3((ushort) 25379, false);
                  gclass3_1_2.method_41(Class32.string_2);
                  this.gclass4_1.method_15(gclass3_1_2);
                  this.method_5(true);
                  continue;
                }
                if (gclass3_1_1.UInt16_0 == (ushort) 41216)
                {
                  try
                  {
                    if (!Class32.list_60.Contains(this.string_3))
                    {
                      lock (Class32.list_60)
                        Class32.list_60.Add(this.string_3);
                    }
                  }
                  catch
                  {
                  }
                }
                else if (gclass3_1_1.UInt16_0 == (ushort) 41220)
                {
                  try
                  {
                    if (!Class32.list_60.Contains(this.string_3))
                    {
                      lock (Class32.list_60)
                        Class32.list_60.Add(this.string_3);
                    }
                  }
                  catch
                  {
                  }
                }
              }
              this.gclass4_0.method_15(gclass3_1_1);
              this.method_5(false);
            }
          }
          this.method_7();
        }
        else
        {
          try
          {
            this.method_9();
          }
          catch
          {
          }
          this.gdelegate0_0(ref this.socket_0, this.genum9_0);
        }
      }
      catch
      {
        try
        {
          this.method_9();
        }
        catch
        {
        }
        this.gdelegate0_0(ref this.socket_0, this.genum9_0);
      }
    }
  }

  public void method_5(bool bool_4)
  {
    try
    {
      lock (this.object_0)
      {
        foreach (KeyValuePair<GClass5, GClass3> keyValuePair in (bool_4 ? this.gclass4_1 : this.gclass4_0).method_18())
          (bool_4 ? this.socket_1 : this.socket_0).Send(keyValuePair.Key.Byte_0);
      }
    }
    catch
    {
    }
  }

  private double method_6()
  {
    double num1 = 0.0;
    TimeSpan timeSpan = DateTime.Now - this.dateTime_0;
    if (this.ulong_0 > (ulong) int.MaxValue)
      this.ulong_0 = 0UL;
    if (this.ulong_0 > 0UL)
    {
      try
      {
        double num2 = timeSpan.TotalSeconds;
        if (timeSpan.TotalSeconds < 1.0)
          num2 = 1.0;
        num1 = Math.Round((double) this.ulong_0 / num2, 2);
      }
      catch
      {
      }
    }
    return num1;
  }

  private void method_7()
  {
    try
    {
      this.socket_1.BeginReceive(this.byte_1, 0, this.byte_1.Length, SocketFlags.None, new AsyncCallback(this.method_4), (object) null);
    }
    catch
    {
      try
      {
        this.method_9();
      }
      catch
      {
      }
      this.gdelegate0_0(ref this.socket_0, this.genum9_0);
    }
  }

  private void method_8()
  {
    try
    {
      this.socket_0.BeginReceive(this.byte_0, 0, this.byte_0.Length, SocketFlags.None, new AsyncCallback(this.method_3), (object) null);
    }
    catch
    {
      try
      {
        this.method_9();
      }
      catch
      {
      }
      this.gdelegate0_0(ref this.socket_0, this.genum9_0);
    }
  }

  private void method_9()
  {
    if (this.bool_0)
      return;
    this.bool_0 = true;
    try
    {
      if (this.socket_1 != null)
      {
        lock (Class32.list_0)
          Class32.list_0.Remove(this.string_3);
        if (this.socket_1.Connected)
          this.socket_1.Disconnect(false);
        if (this.socket_0.Connected)
          this.socket_0.Disconnect(false);
        try
        {
          this.socket_1.Close();
          this.socket_0.Close();
        }
        catch
        {
        }
      }
      this.socket_1 = (Socket) null;
    }
    catch
    {
    }
  }

  public static bool smethod_0(string string_6, string string_7)
  {
    return !string.IsNullOrEmpty(GClass7.smethod_1("SELECT TOP 1 StrUserID FROM [" + Class32.string_10 + "].[dbo].[TB_User] WHERE StrUserID = '" + string_6 + "' AND [password] = '" + string_7 + "'"));
  }

  private void method_10()
  {
    try
    {
      if (DateTime.Now > this.dateTime_1)
      {
        this.dateTime_1 = DateTime.Now.AddSeconds(1.0);
        this.int_3 = 0;
      }
      else
      {
        ++this.int_3;
        if ((this.int_3 < Class32.int_40 ? 0 : (!Class32.list_15.Contains(this.string_4) ? 1 : 0)) == 0)
          return;
        this.method_9();
      }
    }
    catch
    {
    }
  }
}
