﻿// Decompiled with JetBrains decompiler
// Type: GInterface11
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("D4BECDDF-6F73-4A83-B832-9C66874CD20E")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface11
{
  [DispId(1)]
  GEnum2 GEnum2_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(2)]
  GEnum7 GEnum7_0 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(3)]
  string String_0 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(4)]
  bool Boolean_0 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }
}
