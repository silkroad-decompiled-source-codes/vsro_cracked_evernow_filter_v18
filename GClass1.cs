﻿// Decompiled with JetBrains decompiler
// Type: GClass1
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Runtime.InteropServices;
using System.Text;

public static class GClass1
{
  public static T smethod_0<T>(this byte[] byte_0)
  {
    IntPtr num = Marshal.AllocHGlobal(byte_0.Length);
    Marshal.Copy(byte_0, 0, num, byte_0.Length);
    T structure = (T) Marshal.PtrToStructure(num, default (T).GetType());
    Marshal.FreeHGlobal(num);
    return structure;
  }

  public static string smethod_1(this byte[] byte_0)
  {
    return byte_0.smethod_2(0, byte_0.Length);
  }

  public static string smethod_2(this byte[] byte_0, int int_0, int int_1)
  {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    int num = int_1;
    if ((uint) (num % 16) > 0U)
      num += 16 - num % 16;
    for (int index = 0; index <= num; ++index)
    {
      if (index % 16 == 0)
      {
        if (index > 0)
        {
          stringBuilder1.AppendFormat("  {0}{1}", (object) stringBuilder2.ToString(), (object) Environment.NewLine);
          stringBuilder2.Clear();
        }
        if (index != num)
          stringBuilder1.AppendFormat("{0:d10}   ", (object) index);
      }
      if (index < int_1)
      {
        stringBuilder1.AppendFormat(byte_0[int_0 + index].ToString("X2") + " ");
        char c = (char) byte_0[int_0 + index];
        stringBuilder2.Append(char.IsControl(c) ? '.' : c);
      }
      else
      {
        stringBuilder1.Append("   ");
        stringBuilder2.Append('.');
      }
    }
    return stringBuilder1.ToString();
  }
}
