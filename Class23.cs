﻿// Decompiled with JetBrains decompiler
// Type: Class23
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Security.Cryptography;
using System.Text;

internal class Class23
{
  public static string smethod_0(string string_0)
  {
    byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(string_0));
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < hash.Length; ++index)
      stringBuilder.Append(hash[index].ToString("x2"));
    return stringBuilder.ToString();
  }
}
