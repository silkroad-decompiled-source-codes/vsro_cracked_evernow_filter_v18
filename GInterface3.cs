﻿// Decompiled with JetBrains decompiler
// Type: GInterface3
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("F7898AF5-CAC4-4632-A2EC-DA06E5111AF2")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface3
{
  [DispId(1)]
  GInterface6 GInterface6_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(2)]
  GEnum5 GEnum5_0 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_0();

  [DispId(4)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_1(
    [MarshalAs(UnmanagedType.BStr), In] string string_0,
    [In] GEnum2 genum2_0,
    [In] int int_0,
    [MarshalAs(UnmanagedType.BStr), In] string string_1,
    [In] GEnum1 genum1_0,
    [MarshalAs(UnmanagedType.Struct)] out object object_0,
    [MarshalAs(UnmanagedType.Struct)] out object object_1);

  [DispId(5)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_2(
    [In] GEnum2 genum2_0,
    [MarshalAs(UnmanagedType.BStr), In] string string_0,
    [In] byte byte_0,
    [MarshalAs(UnmanagedType.Struct)] out object object_0,
    [MarshalAs(UnmanagedType.Struct)] out object object_1);
}
