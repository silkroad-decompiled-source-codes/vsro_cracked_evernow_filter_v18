﻿// Decompiled with JetBrains decompiler
// Type: GInterface2
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("A6207B2E-7CDD-426A-951E-5E1CBC5AFEAD")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface2
{
  [DispId(1)]
  bool Boolean_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(2)]
  bool Boolean_1 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(3)]
  bool Boolean_2 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(4)]
  bool Boolean_3 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  bool Boolean_4 { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  bool Boolean_5 { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(7)]
  bool Boolean_6 { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(8)]
  bool Boolean_7 { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(9)]
  bool Boolean_8 { [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(10)]
  bool Boolean_9 { [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] set; }
}
