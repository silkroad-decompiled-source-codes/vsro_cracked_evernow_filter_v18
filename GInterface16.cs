﻿// Decompiled with JetBrains decompiler
// Type: GInterface16
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("79FD57C8-908E-4A36-9888-D5B3F0A444CF")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface16
{
  [DispId(1)]
  string String_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; }

  [DispId(2)]
  GEnum8 GEnum8_0 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(3)]
  bool Boolean_0 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(4)]
  GEnum2 GEnum2_0 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  GEnum7 GEnum7_0 { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  string String_1 { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(7)]
  bool Boolean_1 { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(8)]
  GInterface5 GInterface5_0 { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }
}
