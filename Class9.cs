﻿// Decompiled with JetBrains decompiler
// Type: Class9
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Runtime.InteropServices;
using System.Threading;

internal class Class9
{
  private static Class9.Delegate0 delegate0_0;

  private static void Main(string[] args)
  {
    try
    {
      Class8.smethod_0();
      Class19.smethod_0();
    }
    catch
    {
      Environment.Exit(0);
    }
    Class9.delegate0_0 = new Class9.Delegate0(Class9.smethod_0);
    Class9.SetConsoleCtrlHandler(Class9.delegate0_0, true);
    Console.Title = "EverNow [Filter] : Online Player(s) [0]";
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine("    ************************ [EverNow [Filter]]  ****************************");
    Console.WriteLine("    *            BOT Protect Server From [AntiExploit - AntiDDoS]           *");
    Console.WriteLine("    *            Version: [18.0] [Final]                                    *");
    Console.WriteLine("    *************************************************************************");
    Console.WriteLine("");
    Console.WriteLine("");
    Console.ResetColor();
    Console.ForegroundColor = ConsoleColor.Yellow;
    Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] Powered by Abdelrhman Elbattawy.");
    Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] Wait Until Loading.");
    Console.ResetColor();
    bool createdNew;
    Mutex mutex = new Mutex(true, "0X30150X3200X3019578998wseFormghfgh", out createdNew);
    if (createdNew)
    {
      Class37.smethod_0();
      Thread.Sleep(200);
      Class14.smethod_1();
      GC.KeepAlive((object) mutex);
    }
    else
    {
      Console.ForegroundColor = ConsoleColor.Red;
      Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] Tool is already running!!");
      Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] Press Any Key To Close.");
      Console.ResetColor();
      Console.ReadLine();
    }
  }

  private static bool smethod_0(int int_0)
  {
    if (int_0 == 2 && !Class13.bool_0)
    {
      Class10.smethod_0();
      Class13.bool_0 = true;
      Thread.Sleep(150);
      Class25.smethod_1();
    }
    return false;
  }

  [DllImport("kernel32.dll", SetLastError = true)]
  private static extern bool SetConsoleCtrlHandler(Class9.Delegate0 delegate0_1, bool bool_0);

  private delegate bool Delegate0(int eventType);
}
