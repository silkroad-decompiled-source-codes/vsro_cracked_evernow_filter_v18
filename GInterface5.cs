﻿// Decompiled with JetBrains decompiler
// Type: GInterface5
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("C0E9D7FA-E07E-430A-B19A-090CE82D92E2")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface5 : IEnumerable
{
  [DispId(1)]
  int Int32_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_0([MarshalAs(UnmanagedType.Interface), In] GInterface4 ginterface4_0);

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_1([In] int int_0, [In] GEnum1 genum1_0);

  [DispId(4)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  GInterface4 imethod_2([In] int int_0, [In] GEnum1 genum1_0);

  [DispId(-4)]
  [TypeLibFunc(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.CustomMarshaler)]
  IEnumerator imethod_3();
}
