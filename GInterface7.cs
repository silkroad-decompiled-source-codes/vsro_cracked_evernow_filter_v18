﻿// Decompiled with JetBrains decompiler
// Type: GInterface7
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("98325047-C671-4174-8D81-DEFCD3F03186")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface7
{
  [DispId(1)]
  int Int32_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  bool Boolean_0 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(3)]
  object Object_0 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Struct)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.Struct)] set; }

  [DispId(4)]
  bool Boolean_1 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  bool Boolean_2 { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  bool Boolean_3 { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(7)]
  GInterface15 GInterface15_0 { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(8)]
  GInterface17 GInterface17_0 { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(12)]
  GEnum0 GEnum0_0 { [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(13)]
  GEnum0 GEnum0_1 { [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(14)]
  bool Boolean_4 { [DispId(14), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(15)]
  GEnum3 GEnum3_0 { [DispId(15), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(9)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_0([In] int int_0, [MarshalAs(UnmanagedType.BStr), In] string string_0, [In] bool bool_0);

  [DispId(10)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  bool imethod_1([In] int int_0, [MarshalAs(UnmanagedType.BStr), In] string string_0);

  [DispId(11)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_2();
}
