﻿// Decompiled with JetBrains decompiler
// Type: Class38
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;

internal class Class38
{
  public static GClass6 gclass6_0 = new GClass6("Setting(s)/SqlConnection(s).ini");
  public static GClass6 gclass6_1 = new GClass6("Setting(s)/Gateway&Agent(s).ini");
  public static GClass6 gclass6_2 = new GClass6("Setting(s)/SilkPerHour.ini");
  public static GClass6 gclass6_3 = new GClass6("Setting(s)/AfkSystem.ini");
  public static GClass6 gclass6_4 = new GClass6("Setting(s)/AlchemyFeature(s).ini");
  public static GClass6 gclass6_5 = new GClass6("Setting(s)/JobSetting(s).ini");
  public static GClass6 gclass6_6 = new GClass6("Setting(s)/BattleSetting(s).ini");
  public static GClass6 gclass6_7 = new GClass6("Setting(s)/BotSetting(s).ini");
  public static GClass6 gclass6_8 = new GClass6("Setting(s)/Level&Delay(s).ini");
  public static GClass6 gclass6_9 = new GClass6("Setting(s)/AgentFeature(s).ini");
  public static GClass6 gclass6_10 = new GClass6("Setting(s)/Security.ini");
  public static GClass6 gclass6_11 = new GClass6("Setting(s)/GatewayFeature(s).ini");
  public static GClass6 gclass6_12 = new GClass6("Setting(s)/Message(s).ini");
  public static GClass6 gclass6_13 = new GClass6("Setting(s)/ProtectionSetting(s).ini");
  public static GClass6 gclass6_14 = new GClass6("Setting(s)/Discord.ini");

  public static void smethod_0()
  {
    try
    {
      Class32.string_6 = Class38.gclass6_0.method_1("Connection(s) Setting(s)", "Host");
      Class32.string_10 = Class38.gclass6_0.method_1("Connection(s) Setting(s)", "Database Acc");
      Class32.string_9 = Class38.gclass6_0.method_1("Connection(s) Setting(s)", "Database Shard");
      Class32.string_11 = Class38.gclass6_0.method_1("Connection(s) Setting(s)", "Database ShardLog");
      Class32.string_7 = Class38.gclass6_0.method_1("Connection(s) Setting(s)", "User");
      Class32.string_8 = Class38.gclass6_0.method_1("Connection(s) Setting(s)", "Passwrod");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_2 = bool.Parse(Class38.gclass6_10.method_1("GM Settings", "Login Specific IP"));
      Class32.bool_1 = bool.Parse(Class38.gclass6_10.method_1("GM Settings", "Block Users From Login"));
      Class32.bool_121 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Exploits OpCode"));
      Class32.bool_76 = bool.Parse(Class38.gclass6_10.method_1("FireWall Settings", "Enbale Block IP"));
      Class32.bool_122 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Players Information"));
      Class32.bool_100 = bool.Parse(Class38.gclass6_10.method_1("GM Settings", "Start Visible"));
      Class32.bool_101 = bool.Parse(Class38.gclass6_10.method_1("GM Settings", "GM Commands Allow"));
      Class32.bool_102 = bool.Parse(Class38.gclass6_10.method_1("GM Settings", "MOBKill Commands"));
      Class32.bool_103 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [All]"));
      Class32.bool_104 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [GM]"));
      Class32.bool_105 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [PT]"));
      Class32.bool_106 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [Guild]"));
      Class32.bool_107 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Global(s) Chat(s)"));
      Class32.bool_108 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [Union]"));
      Class32.bool_109 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Alchemy Plus"));
      Class32.bool_110 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [PM]"));
      Class32.bool_111 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Chat [Academy]"));
      Class32.bool_133 = bool.Parse(Class38.gclass6_10.method_1("Log Service", "Notice"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.string_2 = Class38.gclass6_11.method_1("Captcha Setting(s)", "Captcha Text");
      Class32.bool_0 = bool.Parse(Class38.gclass6_11.method_1("Captcha Setting(s)", "Enable"));
      Class32.string_44 = Class38.gclass6_11.method_1("Limits Settings", "IP Limit");
      Class32.bool_98 = bool.Parse(Class38.gclass6_11.method_1("Limits Settings", "Enable IP Limit"));
      Class32.string_45 = Class38.gclass6_11.method_1("Limits Settings", "HWID Limit");
      Class32.bool_99 = bool.Parse(Class38.gclass6_11.method_1("Limits Settings", "Enable HWID Limit"));
      Class32.bool_3 = bool.Parse(Class38.gclass6_11.method_1("Fake Players", "Enbale System"));
      Class32.string_3 = Class38.gclass6_11.method_1("Fake Players", "Fake Players");
      Class32.string_4 = Class38.gclass6_11.method_1("Fake Players", "Max Player");
      Class32.string_5 = Class38.gclass6_11.method_1("Fake Players", "ShardID");
      Class32.string_33 = Class38.gclass6_11.method_1("Fake Players", "Server Name");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.string_15 = Class38.gclass6_1.method_1("Server Setting(s)", "Server IP");
      Class32.string_16 = Class38.gclass6_1.method_1("Server Setting(s)", "Gateway Real Port");
      Class32.string_17 = Class38.gclass6_1.method_1("Server Setting(s)", "Gateway Fake Port");
      Class32.string_18 = Class38.gclass6_1.method_1("Server Setting(s)", "Agent Real Port");
      Class32.string_20 = Class38.gclass6_1.method_1("Server Setting(s)", "Agent Fake Port");
      Class32.string_22 = Class38.gclass6_1.method_1("Server Setting(s)", "Agent2 Server IP");
      Class32.string_19 = Class38.gclass6_1.method_1("Server Setting(s)", "Agent2 Real Port");
      Class32.string_21 = Class38.gclass6_1.method_1("Server Setting(s)", "Agent2 Fake Port");
      Class32.bool_117 = bool.Parse(Class38.gclass6_1.method_1("Proxy Setting(s)", "Enable"));
      Class32.string_47 = Class38.gclass6_1.method_1("Proxy Setting(s)", "GatewayIP");
      Class32.string_48 = Class38.gclass6_1.method_1("Proxy Setting(s)", "AgentIP1");
      Class32.string_49 = Class38.gclass6_1.method_1("Proxy Setting(s)", "AgentIP2");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_4 = bool.Parse(Class38.gclass6_2.method_1("Settings", "Enable System"));
      Class32.string_14 = Class38.gclass6_2.method_1("Settings", "Silk Type");
      Class32.int_1 = int.Parse(Class38.gclass6_2.method_1("Settings", "Silk Level"));
      Class32.uint_0 = uint.Parse(Class38.gclass6_2.method_1("Settings", "Silk Amounts"));
      Class32.int_2 = int.Parse(Class38.gclass6_2.method_1("Settings", "Silk Delay"));
      Class32.bool_6 = bool.Parse(Class38.gclass6_2.method_1("Settings", "AFK System"));
      Class32.bool_5 = bool.Parse(Class38.gclass6_2.method_1("Settings", "Notice MSG"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_7 = bool.Parse(Class38.gclass6_3.method_1("Setting(s)", "Enable System"));
      Class32.int_3 = int.Parse(Class38.gclass6_3.method_1("Setting(s)", "Delay Time"));
      Class32.string_23 = Class38.gclass6_3.method_1("Setting(s)", "Type Normal");
      Class32.bool_9 = bool.Parse(Class38.gclass6_3.method_1("Setting(s)", "Arena Enable"));
      Class32.int_5 = int.Parse(Class38.gclass6_3.method_1("Setting(s)", "Arena Delay"));
      Class32.bool_8 = bool.Parse(Class38.gclass6_3.method_1("Setting(s)", "Fortress Enable"));
      Class32.int_4 = int.Parse(Class38.gclass6_3.method_1("Setting(s)", "Fortress Delay"));
      Class32.bool_114 = bool.Parse(Class38.gclass6_3.method_1("Setting(s)", "CTF Enable"));
      Class32.string_46 = Class38.gclass6_3.method_1("Setting(s)", "CTF Delay");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_10 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Enable Plus Notice"));
      Class32.bool_11 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Enable Adv Notice"));
      Class32.int_6 = int.Parse(Class38.gclass6_4.method_1("Setting(s)", "Start Plus"));
      Class32.bool_12 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Adv Specific Notice"));
      Class32.bool_13 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "ExtraNotice"));
      Class32.string_24 = Class38.gclass6_4.method_1("Setting(s)", "ExtraNotice Key Start");
      Class32.string_25 = Class38.gclass6_4.method_1("Setting(s)", "ExtraNotice Key Exec");
      Class32.string_26 = Class38.gclass6_4.method_1("Setting(s)", "ExtraNotice Key Cancel");
      Class32.int_7 = int.Parse(Class38.gclass6_4.method_1("Setting(s)", "Plus Limit"));
      Class32.bool_14 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Disable Adv Elixir"));
      Class32.bool_15 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Disable Elixir"));
      Class32.bool_16 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Alchemy Limit"));
      Class32.bool_17 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Disalbe Stones"));
      Class32.bool_18 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Disable Destory Item"));
      Class32.bool_19 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Avatar Blues"));
      Class32.bool_20 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Record Adv Elixir"));
      Class32.bool_115 = bool.Parse(Class38.gclass6_4.method_1("Setting(s)", "Disable Alchemy Specfic"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_21 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Trace"));
      Class32.bool_22 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Ress"));
      Class32.bool_23 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Enable Teleport Trader"));
      Class32.bool_24 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Enable Teleport Hunter"));
      Class32.bool_25 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Enable Teleport Thieves"));
      Class32.bool_26 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Reverse"));
      Class32.bool_27 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Exchange"));
      Class32.bool_28 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Fellow(s) Pet"));
      Class32.bool_29 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Return in Town"));
      Class32.bool_30 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Drop Item in Town"));
      Class32.bool_31 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Drop Item in AllArea"));
      Class32.bool_32 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Skin Char"));
      Class32.bool_33 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Zerk"));
      Class32.bool_34 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Berserk"));
      Class32.bool_35 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Skills"));
      Class32.bool_36 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Items"));
      Class32.bool_37 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Anti Thief Cheat"));
      Class32.bool_38 = bool.Parse(Class38.gclass6_5.method_1("Setting(s)", "Disable Pet in Town"));
      Class32.bool_39 = bool.Parse(Class38.gclass6_5.method_1("Limit Setting(s)", "Enable Limit Function(s)"));
      Class32.string_27 = Class38.gclass6_5.method_1("Limit Setting(s)", "Limit Type");
      Class32.int_8 = int.Parse(Class38.gclass6_5.method_1("Limit Setting(s)", "Job Counts"));
      Class32.int_9 = int.Parse(Class38.gclass6_5.method_1("Limit Setting(s)", "Trader Counts"));
      Class32.int_10 = int.Parse(Class38.gclass6_5.method_1("Limit Setting(s)", "Hunter Counts"));
      Class32.int_11 = int.Parse(Class38.gclass6_5.method_1("Limit Setting(s)", "Thieve Counts"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_40 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Disable Zerk"));
      Class32.bool_41 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Disable Berserk Pot"));
      Class32.bool_42 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Enable Blocked Skills"));
      Class32.bool_43 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Disable Trace"));
      Class32.bool_44 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Disable Resurrection"));
      Class32.bool_47 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Disable Fellow Pet Ride"));
      Class32.bool_48 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Enable Limit Function(s)"));
      Class32.string_28 = Class38.gclass6_6.method_1("FW Setting(s)", "Limit Type");
      Class32.int_12 = int.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Limit Counts"));
      Class32.bool_116 = bool.Parse(Class38.gclass6_6.method_1("FW Setting(s)", "Disable Change Tax"));
      Class32.bool_49 = bool.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Disable Zerk"));
      Class32.bool_50 = bool.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Enable Blocked Skills"));
      Class32.bool_51 = bool.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Disable Exchange"));
      Class32.bool_52 = bool.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Disable Stall"));
      Class32.bool_53 = bool.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Disable Trace"));
      Class32.bool_54 = bool.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Enable Limit Function(s)"));
      Class32.string_29 = Class38.gclass6_6.method_1("Arena Setting(s)", "Limit Type");
      Class32.int_13 = int.Parse(Class38.gclass6_6.method_1("Arena Setting(s)", "Limit Counts"));
      Class32.string_55 = Class38.gclass6_6.method_1("Arena Setting(s)", "Time Each Round");
      Class32.bool_55 = bool.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Disable Zerk"));
      Class32.bool_56 = bool.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Enable Blocked Skills"));
      Class32.bool_57 = bool.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Disable Exchange"));
      Class32.bool_58 = bool.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Disable Stall"));
      Class32.bool_59 = bool.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Disable Trace"));
      Class32.bool_60 = bool.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Enable Limit Function(s)"));
      Class32.string_30 = Class38.gclass6_6.method_1("CTF Setting(s)", "Limit Type");
      Class32.int_14 = int.Parse(Class38.gclass6_6.method_1("CTF Setting(s)", "Limit Counts"));
      Class32.string_56 = Class38.gclass6_6.method_1("CTF Setting(s)", "Time Each Round");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_61 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Enable BOT Detection"));
      Class32.bool_62 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Pvp Cap"));
      Class32.bool_63 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Trace Mode"));
      Class32.bool_64 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Exchange"));
      Class32.bool_65 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Enter FW"));
      Class32.bool_66 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Alchemy"));
      Class32.bool_67 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Job Mode"));
      Class32.bool_68 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Enter Arena"));
      Class32.bool_69 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Stall"));
      Class32.bool_70 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Party"));
      Class32.bool_71 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Water Temple"));
      Class32.bool_72 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable Stones"));
      Class32.bool_73 = bool.Parse(Class38.gclass6_7.method_1("Third Part Setting(s)", "Disable ByRegion"));
      Class32.bool_120 = bool.Parse(Class38.gclass6_7.method_1("Clientless Setting(s)", "Disable Clientless Login"));
      Class32.bool_127 = bool.Parse(Class38.gclass6_7.method_1("Clientless Setting(s)", "Disable Arena Register"));
      Class32.bool_128 = bool.Parse(Class38.gclass6_7.method_1("Clientless Setting(s)", "Disable CTF Register"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.int_15 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Arena Register"));
      Class32.int_16 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "CTF Register"));
      Class32.int_17 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Global"));
      Class32.int_18 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Stall"));
      Class32.int_19 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Zerk"));
      Class32.int_20 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Reverse"));
      Class32.int_21 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Resurrection"));
      Class32.int_22 = int.Parse(Class38.gclass6_8.method_1("Level Setting(s)", "Exchange"));
      Class32.int_23 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Exchange Delay"));
      Class32.int_24 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Stall Delay"));
      Class32.int_25 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Global Delay"));
      Class32.int_26 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Reverse Delay"));
      Class32.int_27 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Restart Delay"));
      Class32.int_28 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Exit Delay"));
      Class32.int_29 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Zerk Delay"));
      Class32.int_30 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Resurrection Delay"));
      Class32.int_31 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Teleport Delay"));
      Class32.int_32 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Picking Pet Delay"));
      Class32.int_33 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Horses Delay"));
      Class32.int_34 = int.Parse(Class38.gclass6_8.method_1("Delay Setting(s)", "Attack Delay"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.string_31 = Class38.gclass6_9.method_1("Main Setting(s)", "Filter Name");
      Class32.bool_74 = bool.Parse(Class38.gclass6_9.method_1("Main Setting(s)", "Enable Welcome Message"));
      Class32.string_32 = Class38.gclass6_9.method_1("Main Setting(s)", "MSG");
      Class32.bool_77 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Region By Skills"));
      Class32.bool_78 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Region By Zerk"));
      Class32.bool_79 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Region By PvpCape"));
      Class32.bool_80 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Region By Scroll"));
      Class32.bool_81 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Wear PvpCape ByRegion"));
      Class32.bool_82 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Region By Party"));
      Class32.bool_83 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Zerk On Pet"));
      Class32.bool_84 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Exchange at Pvp"));
      Class32.bool_85 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Exchange outside"));
      Class32.bool_86 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Enable Character Lock"));
      Class32.bool_87 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Academy"));
      Class32.bool_88 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Academy Invite"));
      Class32.bool_89 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Zerk at Pvp"));
      Class32.bool_90 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Berserk at Pvp"));
      Class32.bool_91 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Restart"));
      Class32.bool_92 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable Stall outside"));
      Class32.bool_112 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "Disable ReverseByRegion"));
      Class32.bool_113 = bool.Parse(Class38.gclass6_9.method_1("Common Function(s)", "EnableBadWord"));
      Class32.bool_93 = bool.Parse(Class38.gclass6_9.method_1("BlockChat By Team", "Enable System"));
      Class32.string_34 = Class38.gclass6_9.method_1("BlockChat By Team", "Key Exec");
      Class32.string_35 = Class38.gclass6_9.method_1("BlockChat By Team", "Key Cancel");
      Class32.string_36 = Class38.gclass6_9.method_1("BlockChat By Team", "Key Hour(s)");
      Class32.string_37 = Class38.gclass6_9.method_1("BlockChat By Team", "Key Day(s)");
      Class32.string_38 = Class38.gclass6_9.method_1("BlockChat By Team", "Key Month(s)");
      Class32.string_39 = Class38.gclass6_9.method_1("BlockChat By Team", "Key Year(s)");
      Class32.bool_94 = bool.Parse(Class38.gclass6_9.method_1("EverNow MultiTool(s)", "Enable System"));
      Class32.string_40 = Class38.gclass6_9.method_1("EverNow MultiTool(s)", "CharName[BOT]");
      Class32.string_41 = Class38.gclass6_9.method_1("EverNow MultiTool(s)", "CharName[Helper]");
      Class32.string_42 = Class38.gclass6_9.method_1("EverNow MultiTool(s)", "Key System(s)");
      Class32.string_43 = Class38.gclass6_9.method_1("EverNow MultiTool(s)", "Key Helper");
      Class32.bool_95 = bool.Parse(Class38.gclass6_9.method_1("EverNow MultiTool(s)", "Auto Information"));
      Class32.bool_96 = bool.Parse(Class38.gclass6_9.method_1("EverNow MultiTool(s)", "Auto Trade Info"));
      Class32.int_36 = int.Parse(Class38.gclass6_9.method_1("Limit Setting(s)", "Guild Member Limit"));
      Class32.int_35 = int.Parse(Class38.gclass6_9.method_1("Limit Setting(s)", "Union Limit"));
      Class32.bool_123 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "Advanced Switch"));
      Class32.bool_124 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "Enable Color Dll"));
      Class32.bool_125 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "Enable Luxury Globals"));
      Class32.int_42 = int.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "ID Item [Blue]"));
      Class32.int_43 = int.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "ID Item [Green]"));
      Class32.int_44 = int.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "ID Item [Orange]"));
      Class32.int_45 = int.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "ID Item [Purple]"));
      Class32.int_46 = int.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "ID Item [Red]"));
      Class32.int_47 = int.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "ID Item [Olive]"));
      Class32.bool_126 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "Enable Title(s) System"));
      Class32.string_50 = Class38.gclass6_9.method_1("Advanced Setting(s)", "Notify Type");
      Class32.bool_130 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "Enable Commands"));
      Class32.string_51 = Class38.gclass6_9.method_1("Advanced Setting(s)", "Key Commands");
      Class32.bool_131 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "EnableAutoEvent"));
      Class32.string_52 = Class38.gclass6_9.method_1("Advanced Setting(s)", "AutoEventCharName");
      Class32.string_53 = Class38.gclass6_9.method_1("Advanced Setting(s)", "AutoEventNotifyType");
      Class32.bool_132 = bool.Parse(Class38.gclass6_9.method_1("Advanced Setting(s)", "AutoEventWinner"));
      Class32.string_54 = Class38.gclass6_9.method_1("Advanced Setting(s)", "AutoEventWinnerType");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class33.string_0 = Class38.gclass6_12.method_1("Message(s)", "AutoInfo [Helper]");
      Class33.string_1 = Class38.gclass6_12.method_1("Message(s)", "AutoInfo [BOT]");
      Class33.string_2 = Class38.gclass6_12.method_1("Message(s)", "AutoInfo [Trade]");
      Class33.string_3 = Class38.gclass6_12.method_1("Message(s)", "PCLimit");
      Class33.string_4 = Class38.gclass6_12.method_1("Message(s)", "IPLimit");
      Class33.string_5 = Class38.gclass6_12.method_1("Message(s)", "JobCountLimit");
      Class33.string_6 = Class38.gclass6_12.method_1("Message(s)", "TraderCountLimit");
      Class33.string_7 = Class38.gclass6_12.method_1("Message(s)", "HunterCountLimit");
      Class33.string_8 = Class38.gclass6_12.method_1("Message(s)", "ThieveCountLimit");
      Class33.string_9 = Class38.gclass6_12.method_1("Message(s)", "ArenaLimit");
      Class33.string_10 = Class38.gclass6_12.method_1("Message(s)", "FWLimit");
      Class33.string_11 = Class38.gclass6_12.method_1("Message(s)", "JobBOT");
      Class33.string_12 = Class38.gclass6_12.method_1("Message(s)", "ArenaBOT");
      Class33.string_13 = Class38.gclass6_12.method_1("Message(s)", "FWBOT");
      Class33.string_14 = Class38.gclass6_12.method_1("Message(s)", "RegionsBOT");
      Class33.string_15 = Class38.gclass6_12.method_1("Message(s)", "CannotOpenPvP");
      Class33.string_16 = Class38.gclass6_12.method_1("Message(s)", "ExchangeLevel");
      Class33.string_17 = Class38.gclass6_12.method_1("Message(s)", "ExchangeDelay");
      Class33.string_18 = Class38.gclass6_12.method_1("Message(s)", "ExchangeJob");
      Class33.string_19 = Class38.gclass6_12.method_1("Message(s)", "ExchangePvP");
      Class33.string_20 = Class38.gclass6_12.method_1("Message(s)", "CharLock");
      Class33.string_21 = Class38.gclass6_12.method_1("Message(s)", "ExchangeArena");
      Class33.string_22 = Class38.gclass6_12.method_1("Message(s)", "ExchangeOutSide");
      Class33.string_23 = Class38.gclass6_12.method_1("Message(s)", "ExchangeBOT");
      Class33.string_24 = Class38.gclass6_12.method_1("Message(s)", "StallTeleport");
      Class33.string_25 = Class38.gclass6_12.method_1("Message(s)", "StallLevel");
      Class33.string_26 = Class38.gclass6_12.method_1("Message(s)", "StallDelay");
      Class33.string_27 = Class38.gclass6_12.method_1("Message(s)", "StallBadWord");
      Class33.string_28 = Class38.gclass6_12.method_1("Message(s)", "StallInArena");
      Class33.string_29 = Class38.gclass6_12.method_1("Message(s)", "StallOutSide");
      Class33.string_30 = Class38.gclass6_12.method_1("Message(s)", "StallBOT");
      Class33.string_31 = Class38.gclass6_12.method_1("Message(s)", "ZerkLevel");
      Class33.string_32 = Class38.gclass6_12.method_1("Message(s)", "ZerkDelay");
      Class33.string_33 = Class38.gclass6_12.method_1("Message(s)", "ZerkRegions");
      Class33.string_34 = Class38.gclass6_12.method_1("Message(s)", "ZerkJob");
      Class33.string_35 = Class38.gclass6_12.method_1("Message(s)", "ZerkFW");
      Class33.string_36 = Class38.gclass6_12.method_1("Message(s)", "ZerkPvp");
      Class33.string_37 = Class38.gclass6_12.method_1("Message(s)", "ZerkArena");
      Class33.string_38 = Class38.gclass6_12.method_1("Message(s)", "AvatarBlue");
      Class33.string_39 = Class38.gclass6_12.method_1("Message(s)", "ItemRegions");
      Class33.string_40 = Class38.gclass6_12.method_1("Message(s)", "ItemJob");
      Class33.string_41 = Class38.gclass6_12.method_1("Message(s)", "BerserkPvp");
      Class33.string_42 = Class38.gclass6_12.method_1("Message(s)", "BerserkFW");
      Class33.string_43 = Class38.gclass6_12.method_1("Message(s)", "BerserkJob");
      Class33.string_44 = Class38.gclass6_12.method_1("Message(s)", "JobRoom");
      Class33.string_45 = Class38.gclass6_12.method_1("Message(s)", "ReverseJob");
      Class33.string_46 = Class38.gclass6_12.method_1("Message(s)", "ReverseLevel");
      Class33.string_47 = Class38.gclass6_12.method_1("Message(s)", "ReverseDelay");
      Class33.string_48 = Class38.gclass6_12.method_1("Message(s)", "ChatBlock");
      Class33.string_49 = Class38.gclass6_12.method_1("Message(s)", "GlobalLevel");
      Class33.string_50 = Class38.gclass6_12.method_1("Message(s)", "GlobalDelay");
      Class33.string_51 = Class38.gclass6_12.method_1("Message(s)", "GlobalBadWord");
      Class33.string_52 = Class38.gclass6_12.method_1("Message(s)", "RessJob");
      Class33.string_53 = Class38.gclass6_12.method_1("Message(s)", "RessLevel");
      Class33.string_54 = Class38.gclass6_12.method_1("Message(s)", "RessDelay");
      Class33.string_55 = Class38.gclass6_12.method_1("Message(s)", "RessFW");
      Class33.string_56 = Class38.gclass6_12.method_1("Message(s)", "RessArena");
      Class33.string_57 = Class38.gclass6_12.method_1("Message(s)", "RessCTF");
      Class33.string_58 = Class38.gclass6_12.method_1("Message(s)", "Trans_Spawn");
      Class33.string_59 = Class38.gclass6_12.method_1("Message(s)", "TransDelay");
      Class33.string_60 = Class38.gclass6_12.method_1("Message(s)", "PetDelay");
      Class33.string_61 = Class38.gclass6_12.method_1("Message(s)", "ThieveScroll");
      Class33.string_62 = Class38.gclass6_12.method_1("Message(s)", "ChangeSkin");
      Class33.string_63 = Class38.gclass6_12.method_1("Message(s)", "CTFLevel");
      Class33.string_64 = Class38.gclass6_12.method_1("Message(s)", "ArenaLevel");
      Class33.string_65 = Class38.gclass6_12.method_1("Message(s)", "AntiDropItem");
      Class33.string_66 = Class38.gclass6_12.method_1("Message(s)", "HWTBOT");
      Class33.string_67 = Class38.gclass6_12.method_1("Message(s)", "TeleportDelay");
      Class33.string_68 = Class38.gclass6_12.method_1("Message(s)", "AlchemyBOT");
      Class33.string_69 = Class38.gclass6_12.method_1("Message(s)", "ElixirDisable");
      Class33.string_70 = Class38.gclass6_12.method_1("Message(s)", "PlusLimit");
      Class33.string_71 = Class38.gclass6_12.method_1("Message(s)", "PlusMax");
      Class33.string_72 = Class38.gclass6_12.method_1("Message(s)", "AdvPlus");
      Class33.string_73 = Class38.gclass6_12.method_1("Message(s)", "ExcedPlus");
      Class33.string_74 = Class38.gclass6_12.method_1("Message(s)", "AlchemyBOT1");
      Class33.string_75 = Class38.gclass6_12.method_1("Message(s)", "AlchemyStones");
      Class33.string_76 = Class38.gclass6_12.method_1("Message(s)", "ExitDelay");
      Class33.string_77 = Class38.gclass6_12.method_1("Message(s)", "RestartDelay");
      Class33.string_78 = Class38.gclass6_12.method_1("Message(s)", "RestartDiasble");
      Class33.string_79 = Class38.gclass6_12.method_1("Message(s)", "GuildLimit");
      Class33.string_80 = Class38.gclass6_12.method_1("Message(s)", "UnionLimit");
      Class33.string_81 = Class38.gclass6_12.method_1("Message(s)", "Pickitem");
      Class33.string_82 = Class38.gclass6_12.method_1("Message(s)", "TraceJob");
      Class33.string_83 = Class38.gclass6_12.method_1("Message(s)", "TraceArena");
      Class33.string_84 = Class38.gclass6_12.method_1("Message(s)", "TraceFW");
      Class33.string_85 = Class38.gclass6_12.method_1("Message(s)", "TraceBot");
      Class33.string_86 = Class38.gclass6_12.method_1("Message(s)", "BlockSkills");
      Class33.string_87 = Class38.gclass6_12.method_1("Message(s)", "BlockSkillsArena");
      Class33.string_88 = Class38.gclass6_12.method_1("Message(s)", "BlockSkillsJob");
      Class33.string_89 = Class38.gclass6_12.method_1("Message(s)", "BlockSkillsRegions");
      Class33.string_90 = Class38.gclass6_12.method_1("Message(s)", "BlockSkillsFW");
      Class33.string_91 = Class38.gclass6_12.method_1("Message(s)", "PartymatchingBadWord");
      Class33.string_92 = Class38.gclass6_12.method_1("Message(s)", "RegionParty");
      Class33.string_93 = Class38.gclass6_12.method_1("Message(s)", "BadWord");
      Class33.string_94 = Class38.gclass6_12.method_1("Message(s)", "LockSucc");
      Class33.string_95 = Class38.gclass6_12.method_1("Message(s)", "LockMaxium");
      Class33.string_96 = Class38.gclass6_12.method_1("Message(s)", "LockAlready");
      Class33.string_97 = Class38.gclass6_12.method_1("Message(s)", "LockIncorrect");
      Class33.string_98 = Class38.gclass6_12.method_1("Message(s)", "unLockSucc");
      Class33.string_99 = Class38.gclass6_12.method_1("Message(s)", "unLockMaxium");
      Class33.string_100 = Class38.gclass6_12.method_1("Message(s)", "unLockAlready");
      Class33.string_101 = Class38.gclass6_12.method_1("Message(s)", "unLockIncorrect");
      Class33.string_102 = Class38.gclass6_12.method_1("Message(s)", "unLockIncorrectPass");
      Class33.string_103 = Class38.gclass6_12.method_1("Message(s)", "AcademyDisable");
      Class33.string_104 = Class38.gclass6_12.method_1("Message(s)", "AcademyInvite");
      Class33.string_105 = Class38.gclass6_12.method_1("Message(s)", "MovePvp");
      Class33.string_106 = Class38.gclass6_12.method_1("Message(s)", "PetJob");
      Class33.string_107 = Class38.gclass6_12.method_1("Message(s)", "RegionParty1");
      Class33.string_108 = Class38.gclass6_12.method_1("Message(s)", "RegionBOT1");
      Class33.string_109 = Class38.gclass6_12.method_1("Message(s)", "DestoryItem");
      Class33.string_110 = Class38.gclass6_12.method_1("Message(s)", "CannotDestory");
      Class33.string_111 = Class38.gclass6_12.method_1("Message(s)", "ZerkPet");
      Class33.string_112 = Class38.gclass6_12.method_1("Message(s)", "PetFW");
      Class33.string_113 = Class38.gclass6_12.method_1("Message(s)", "Premium");
      Class33.string_114 = Class38.gclass6_12.method_1("Message(s)", "SilkSystem");
      Class33.string_115 = Class38.gclass6_12.method_1("Message(s)", "CTFLimit");
      Class33.string_116 = Class38.gclass6_12.method_1("Message(s)", "ExchangeCTF");
      Class33.string_117 = Class38.gclass6_12.method_1("Message(s)", "StallInCTF");
      Class33.string_118 = Class38.gclass6_12.method_1("Message(s)", "ZerkCTF");
      Class33.string_119 = Class38.gclass6_12.method_1("Message(s)", "TraceCTF");
      Class33.string_120 = Class38.gclass6_12.method_1("Message(s)", "BlockSkillsCTF");
      Class33.string_121 = Class38.gclass6_12.method_1("Message(s)", "SpecificItem");
      Class33.string_122 = Class38.gclass6_12.method_1("Message(s)", "TeleportByTime");
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.int_39 = int.Parse(Class38.gclass6_13.method_1("Flood Setting(s)", "FLOOD Limit"));
      Class32.int_40 = int.Parse(Class38.gclass6_13.method_1("Flood Setting(s)", "Packet Count"));
      Class32.bool_119 = bool.Parse(Class38.gclass6_13.method_1("Advanced Flood", "Enable"));
      Class32.int_41 = int.Parse(Class38.gclass6_13.method_1("Advanced Flood", "Flood"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      Class32.bool_134 = bool.Parse(Class38.gclass6_14.method_1("Setting(s)", "Enable System"));
      Class32.string_57 = Class38.gclass6_14.method_1("Setting(s)", "WebHook Url");
      Class32.bool_135 = bool.Parse(Class38.gclass6_14.method_1("Setting(s)", "Send Globals"));
      Class32.bool_136 = bool.Parse(Class38.gclass6_14.method_1("Setting(s)", "Send Notice"));
      Class32.bool_137 = bool.Parse(Class38.gclass6_14.method_1("Setting(s)", "Send Unique Appear"));
      Class32.bool_138 = bool.Parse(Class38.gclass6_14.method_1("Setting(s)", "Send Unique Kills"));
      Class32.bool_139 = bool.Parse(Class38.gclass6_14.method_1("AutoEvent(s)", "Enable Function"));
      Class32.string_58 = Class38.gclass6_14.method_1("AutoEvent(s)", "CharName");
      Class32.bool_140 = bool.Parse(Class38.gclass6_14.method_1("AutoEvent(s)", "Send Globals"));
      Class32.bool_141 = bool.Parse(Class38.gclass6_14.method_1("AutoEvent(s)", "Send Notice"));
    }
    catch (Exception ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
  }
}
