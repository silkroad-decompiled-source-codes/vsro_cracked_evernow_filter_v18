﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: Extension]
[assembly: AssemblyTitle("EverNow [Filter]")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alkayan Groups")]
[assembly: AssemblyProduct("EverNow [Filter]")]
[assembly: AssemblyCopyright("Copyright to Abdelrhman Elbattawy ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("3e82e84e-2e09-4f7a-9422-c39588fbedc6")]
[assembly: AssemblyFileVersion("18.0.0.0")]
[assembly: AssemblyVersion("18.0.0.0")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
