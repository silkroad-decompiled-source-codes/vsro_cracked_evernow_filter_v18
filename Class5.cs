﻿// Decompiled with JetBrains decompiler
// Type: Class5
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.IO;

internal class Class5 : BinaryReader
{
  private byte[] byte_0;

  public Class5(byte[] byte_1)
    : base((Stream) new MemoryStream(byte_1, false))
  {
    this.byte_0 = byte_1;
  }

  public Class5(byte[] byte_1, int int_0, int int_1)
    : base((Stream) new MemoryStream(byte_1, int_0, int_1, false))
  {
    this.byte_0 = byte_1;
  }
}
