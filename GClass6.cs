﻿// Decompiled with JetBrains decompiler
// Type: GClass6
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.InteropServices;
using System.Text;

public class GClass6
{
  public string string_0;

  [DllImport("kernel32")]
  private static extern long WritePrivateProfileString(
    string string_1,
    string string_2,
    string string_3,
    string string_4);

  [DllImport("kernel32")]
  private static extern int GetPrivateProfileString(
    string string_1,
    string string_2,
    string string_3,
    StringBuilder stringBuilder_0,
    int int_0,
    string string_4);

  public GClass6(string string_1)
  {
    try
    {
      this.string_0 = string_1;
    }
    catch
    {
    }
  }

  public void method_0(string string_1, string string_2, string string_3)
  {
    try
    {
      GClass6.WritePrivateProfileString(string_1, string_2, string_3, this.string_0);
    }
    catch
    {
    }
  }

  public string method_1(string string_1, string string_2)
  {
    string empty;
    try
    {
      StringBuilder stringBuilder_0 = new StringBuilder((int) byte.MaxValue);
      GClass6.GetPrivateProfileString(string_1, string_2, "", stringBuilder_0, (int) byte.MaxValue, this.string_0);
      empty = stringBuilder_0.ToString();
      goto label_4;
    }
    catch
    {
    }
    empty = string.Empty;
label_4:
    return empty;
  }
}
