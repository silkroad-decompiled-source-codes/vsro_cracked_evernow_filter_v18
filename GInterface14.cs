﻿// Decompiled with JetBrains decompiler
// Type: GInterface14
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("B21563FF-D696-4222-AB46-4E89B73AB34A")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface14 : GInterface12, GInterface13
{
  [DispId(1)]
  string String_22 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(2)]
  string String_23 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(3)]
  string String_24 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(4)]
  string String_25 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(5)]
  int Int32_5 { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  string String_26 { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(7)]
  string String_27 { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(8)]
  string String_28 { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(9)]
  string String_29 { [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(10)]
  string String_30 { [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(11)]
  GEnum6 GEnum6_2 { [DispId(11), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(11), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(12)]
  object Object_2 { [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Struct)] get; [DispId(12), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.Struct)] set; }

  [DispId(13)]
  string String_31 { [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(13), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(14)]
  bool Boolean_4 { [DispId(14), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(14), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(15)]
  string String_32 { [DispId(15), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(15), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(16)]
  int Int32_6 { [DispId(16), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(16), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(17)]
  bool Boolean_5 { [DispId(17), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(17), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(18)]
  GEnum0 GEnum0_2 { [DispId(18), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(18), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(19)]
  int Int32_7 { [DispId(19), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(19), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(20)]
  string String_33 { [DispId(20), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(20), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(21)]
  string String_34 { [DispId(21), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(21), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(22)]
  string String_35 { [DispId(22), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(22), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(23)]
  string String_36 { [DispId(23), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(23), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(24)]
  string String_37 { [DispId(24), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(24), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(25)]
  int Int32_8 { [DispId(25), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(25), MethodImpl(MethodImplOptions.InternalCall)] set; }
}
