﻿// Decompiled with JetBrains decompiler
// Type: Class24
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;

internal class Class24
{
  public static void smethod_0()
  {
    try
    {
      File.WriteAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Setting(s)\\[Filter][Log].txt", string.Empty);
    }
    catch
    {
    }
    try
    {
      Class16.smethod_0();
    }
    catch
    {
      Environment.Exit(0);
    }
    try
    {
      Class25.smethod_2();
    }
    catch
    {
      Environment.Exit(0);
    }
    try
    {
      Class25.smethod_3();
    }
    catch
    {
      Environment.Exit(0);
    }
    try
    {
      Class38.smethod_0();
      Class36.smethod_0();
      Class35.smethod_0();
      Class18.smethod_0("Setting(s) has been loaded successfully.", "Blue");
    }
    catch
    {
      Class18.smethod_0("Cannot load Setting(s) check it.", "Red");
    }
    try
    {
      GClass7.sqlConnection_0 = new SqlConnection("Data Source=" + Class32.string_6 + ";Initial Catalog=EverNow_Filter;Integrated Security=false; User ID = " + Class32.string_7 + "; Password = " + Class32.string_8 + ";");
      GClass7.sqlConnection_0.Open();
      Class18.smethod_0("Connection(s) [SQL] has been opened successfully.", "Green");
      try
      {
        Class21.smethod_1();
        Class21.smethod_0();
        GClass7.smethod_0("UPDATE [_LogPlayers] SET Status = 'OFFLINE'");
      }
      catch
      {
        Class18.smethod_0("Cannot load Table(s).", "Red");
      }
    }
    catch (SqlException ex)
    {
      Class18.smethod_0(ex.ToString(), "Red");
    }
    try
    {
      if (Class32.bool_117)
      {
        new GClass9().method_0(Class32.string_47, Convert.ToInt32(Class32.string_17), GClass9.GEnum9.GatewayServer);
        new GClass9().method_0(Class32.string_48, Convert.ToInt32(Class32.string_20), GClass9.GEnum9.AgentServer);
        if (!(Class32.string_22 != "0"))
          return;
        new GClass9().method_0(Class32.string_49, Convert.ToInt32(Class32.string_21), GClass9.GEnum9.AgentServer2);
      }
      else
      {
        new GClass9().method_0(Class32.string_15, Convert.ToInt32(Class32.string_17), GClass9.GEnum9.GatewayServer);
        new GClass9().method_0(Class32.string_15, Convert.ToInt32(Class32.string_20), GClass9.GEnum9.AgentServer);
        if (!(Class32.string_22 != "0"))
          return;
        new GClass9().method_0(Class32.string_15, Convert.ToInt32(Class32.string_21), GClass9.GEnum9.AgentServer2);
      }
    }
    catch
    {
      Class18.smethod_0("Guard Cannot open check [IP Server & Port(s)].", "Red");
    }
  }
}
