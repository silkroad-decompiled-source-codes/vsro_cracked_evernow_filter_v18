﻿// Decompiled with JetBrains decompiler
// Type: GClass3
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.IO;
using System.Text;

public class GClass3
{
  private ushort ushort_0;
  private Class6 class6_0;
  private Class5 class5_0;
  private bool bool_0;
  private bool bool_1;
  private bool bool_2;
  private byte[] byte_0;
  private object object_0;

  public ushort UInt16_0
  {
    get
    {
      return this.ushort_0;
    }
  }

  public bool Boolean_0
  {
    get
    {
      return this.bool_0;
    }
  }

  public bool Boolean_1
  {
    get
    {
      return this.bool_1;
    }
  }

  public GClass3(GClass3 gclass3_0)
  {
    lock (gclass3_0.object_0)
    {
      this.object_0 = new object();
      this.ushort_0 = gclass3_0.ushort_0;
      this.bool_0 = gclass3_0.bool_0;
      this.bool_1 = gclass3_0.bool_1;
      this.bool_2 = gclass3_0.bool_2;
      if (!this.bool_2)
      {
        this.class6_0 = new Class6();
        this.class5_0 = (Class5) null;
        this.byte_0 = (byte[]) null;
        this.class6_0.Write(gclass3_0.class6_0.method_0());
      }
      else
      {
        this.class6_0 = (Class6) null;
        this.byte_0 = gclass3_0.byte_0;
        this.class5_0 = new Class5(this.byte_0);
      }
    }
  }

  public GClass3(ushort ushort_1)
  {
    this.object_0 = new object();
    this.ushort_0 = ushort_1;
    this.bool_0 = false;
    this.bool_1 = false;
    this.class6_0 = new Class6();
    this.class5_0 = (Class5) null;
    this.byte_0 = (byte[]) null;
  }

  public GClass3(ushort ushort_1, bool bool_3)
  {
    this.object_0 = new object();
    this.ushort_0 = ushort_1;
    this.bool_0 = bool_3;
    this.bool_1 = false;
    this.class6_0 = new Class6();
    this.class5_0 = (Class5) null;
    this.byte_0 = (byte[]) null;
  }

  public GClass3(ushort ushort_1, bool bool_3, bool bool_4)
  {
    if (bool_3 & bool_4)
      throw new Exception("[Packet::Packet] Packets cannot both be massive and encrypted!");
    this.object_0 = new object();
    this.ushort_0 = ushort_1;
    this.bool_0 = bool_3;
    this.bool_1 = bool_4;
    this.class6_0 = new Class6();
    this.class5_0 = (Class5) null;
    this.byte_0 = (byte[]) null;
  }

  public GClass3(ushort ushort_1, bool bool_3, bool bool_4, byte[] byte_1)
  {
    if (bool_3 & bool_4)
      throw new Exception("[Packet::Packet] Packets cannot both be massive and encrypted!");
    this.object_0 = new object();
    this.ushort_0 = ushort_1;
    this.bool_0 = bool_3;
    this.bool_1 = bool_4;
    this.class6_0 = new Class6();
    this.class6_0.Write(byte_1);
    this.class5_0 = (Class5) null;
    this.byte_0 = (byte[]) null;
  }

  public GClass3(ushort ushort_1, bool bool_3, bool bool_4, byte[] byte_1, int int_0, int int_1)
  {
    if (bool_3 & bool_4)
      throw new Exception("[Packet::Packet] Packets cannot both be massive and encrypted!");
    this.object_0 = new object();
    this.ushort_0 = ushort_1;
    this.bool_0 = bool_3;
    this.bool_1 = bool_4;
    this.class6_0 = new Class6();
    this.class6_0.Write(byte_1, int_0, int_1);
    this.class5_0 = (Class5) null;
    this.byte_0 = (byte[]) null;
  }

  public byte[] method_0()
  {
    lock (this.object_0)
      return !this.bool_2 ? this.class6_0.method_0() : this.byte_0;
  }

  public void method_1()
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        return;
      this.byte_0 = this.class6_0.method_0();
      this.class5_0 = new Class5(this.byte_0);
      this.class6_0.Close();
      this.class6_0 = (Class6) null;
      this.bool_2 = true;
    }
  }

  public long method_2(long long_0, SeekOrigin seekOrigin_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot SeekRead on an unlocked Packet.");
      return this.class5_0.BaseStream.Seek(long_0, seekOrigin_0);
    }
  }

  public int method_3()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot SeekRead on an unlocked Packet.");
      return (int) (this.class5_0.BaseStream.Length - this.class5_0.BaseStream.Position);
    }
  }

  public byte method_4()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadByte();
    }
  }

  public sbyte method_5()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadSByte();
    }
  }

  public ushort method_6()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadUInt16();
    }
  }

  public short method_7()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadInt16();
    }
  }

  public uint method_8()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadUInt32();
    }
  }

  public int method_9()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadInt32();
    }
  }

  public ulong method_10()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadUInt64();
    }
  }

  public long method_11()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadInt64();
    }
  }

  public float method_12()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadSingle();
    }
  }

  public double method_13()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return this.class5_0.ReadDouble();
    }
  }

  public string method_14()
  {
    return this.method_15(1252);
  }

  public string method_15(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      byte[] bytes = this.class5_0.ReadBytes((int) this.class5_0.ReadUInt16());
      return Encoding.GetEncoding(int_0).GetString(bytes);
    }
  }

  public string method_16()
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      return Encoding.Unicode.GetString(this.class5_0.ReadBytes((int) this.class5_0.ReadUInt16() * 2));
    }
  }

  public byte[] method_17(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      byte[] numArray = new byte[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadByte();
      return numArray;
    }
  }

  public sbyte[] method_18(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      sbyte[] numArray = new sbyte[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadSByte();
      return numArray;
    }
  }

  public ushort[] method_19(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      ushort[] numArray = new ushort[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadUInt16();
      return numArray;
    }
  }

  public short[] method_20(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      short[] numArray = new short[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadInt16();
      return numArray;
    }
  }

  public uint[] method_21(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      uint[] numArray = new uint[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadUInt32();
      return numArray;
    }
  }

  public int[] method_22(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      int[] numArray = new int[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadInt32();
      return numArray;
    }
  }

  public ulong[] method_23(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      ulong[] numArray = new ulong[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadUInt64();
      return numArray;
    }
  }

  public long[] method_24(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      long[] numArray = new long[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadInt64();
      return numArray;
    }
  }

  public float[] method_25(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      float[] numArray = new float[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadSingle();
      return numArray;
    }
  }

  public double[] method_26(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      double[] numArray = new double[int_0];
      for (int index = 0; index < int_0; ++index)
        numArray[index] = this.class5_0.ReadDouble();
      return numArray;
    }
  }

  public string[] method_27(int int_0)
  {
    return this.method_27(1252);
  }

  public string[] method_28(int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      string[] strArray = new string[int_1];
      for (int index = 0; index < int_1; ++index)
      {
        byte[] bytes = this.class5_0.ReadBytes((int) this.class5_0.ReadUInt16());
        strArray[index] = Encoding.UTF7.GetString(bytes);
      }
      return strArray;
    }
  }

  public string[] method_29(int int_0)
  {
    lock (this.object_0)
    {
      if (!this.bool_2)
        throw new Exception("Cannot Read from an unlocked Packet.");
      string[] strArray = new string[int_0];
      for (int index = 0; index < int_0; ++index)
      {
        byte[] bytes = this.class5_0.ReadBytes((int) this.class5_0.ReadUInt16() * 2);
        strArray[index] = Encoding.Unicode.GetString(bytes);
      }
      return strArray;
    }
  }

  public long method_30(long long_0, SeekOrigin seekOrigin_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot SeekWrite on a locked Packet.");
      return this.class6_0.BaseStream.Seek(long_0, seekOrigin_0);
    }
  }

  public void method_31(byte byte_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(byte_1);
    }
  }

  public void method_32(sbyte sbyte_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(sbyte_0);
    }
  }

  public void method_33(ushort ushort_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(ushort_1);
    }
  }

  public void method_34(short short_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(short_0);
    }
  }

  public void method_35(uint uint_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(uint_0);
    }
  }

  public void method_36(int int_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(int_0);
    }
  }

  public void method_37(ulong ulong_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(ulong_0);
    }
  }

  public void method_38(long long_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(long_0);
    }
  }

  public void method_39(float float_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(float_0);
    }
  }

  public void method_40(double double_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(double_0);
    }
  }

  public void method_41(string string_0)
  {
    this.method_42(string_0, 1252);
  }

  public void method_42(string string_0, int int_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      byte[] bytes = Encoding.Default.GetBytes(Encoding.UTF7.GetString(Encoding.GetEncoding(int_0).GetBytes(string_0)));
      this.class6_0.Write((ushort) bytes.Length);
      this.class6_0.Write(bytes);
    }
  }

  public void method_43(string string_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      byte[] bytes = Encoding.Unicode.GetBytes(string_0);
      this.class6_0.Write((ushort) string_0.ToString().Length);
      this.class6_0.Write(bytes);
    }
  }

  public void method_44(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write((byte) (Convert.ToUInt64(object_1) & (ulong) byte.MaxValue));
    }
  }

  public void method_45(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write((sbyte) (Convert.ToInt64(object_1) & (long) byte.MaxValue));
    }
  }

  public void method_46(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write((ushort) (Convert.ToUInt64(object_1) & (ulong) ushort.MaxValue));
    }
  }

  public void method_47(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write((ushort) (Convert.ToInt64(object_1) & (long) ushort.MaxValue));
    }
  }

  public void method_48(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write((uint) (Convert.ToUInt64(object_1) & (ulong) uint.MaxValue));
    }
  }

  public void method_49(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write((int) (Convert.ToInt64(object_1) & (long) uint.MaxValue));
    }
  }

  public void method_50(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(Convert.ToUInt64(object_1));
    }
  }

  public void method_51(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(Convert.ToInt64(object_1));
    }
  }

  public void method_52(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(Convert.ToSingle(object_1));
    }
  }

  public void method_53(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      this.class6_0.Write(Convert.ToDouble(object_1));
    }
  }

  public void method_54(object object_1)
  {
    this.method_55(object_1, 1252);
  }

  public void method_55(object object_1, int int_0)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      byte[] bytes = Encoding.Default.GetBytes(Encoding.UTF7.GetString(Encoding.GetEncoding(int_0).GetBytes(object_1.ToString())));
      this.class6_0.Write((ushort) bytes.Length);
      this.class6_0.Write(bytes);
    }
  }

  public void method_56(object object_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      byte[] bytes = Encoding.Unicode.GetBytes(object_1.ToString());
      this.class6_0.Write((ushort) object_1.ToString().Length);
      this.class6_0.Write(bytes);
    }
  }

  public void method_57(byte[] byte_1)
  {
    if (this.bool_2)
      throw new Exception("Cannot Write to a locked Packet.");
    this.class6_0.Write(byte_1);
  }

  public void method_58(byte[] byte_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(byte_1[index]);
    }
  }

  public void method_59(ushort[] ushort_1)
  {
    this.method_60(ushort_1, 0, ushort_1.Length);
  }

  public void method_60(ushort[] ushort_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(ushort_1[index]);
    }
  }

  public void method_61(short[] short_0)
  {
    this.method_62(short_0, 0, short_0.Length);
  }

  public void method_62(short[] short_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(short_0[index]);
    }
  }

  public void method_63(uint[] uint_0)
  {
    this.method_64(uint_0, 0, uint_0.Length);
  }

  public void method_64(uint[] uint_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(uint_0[index]);
    }
  }

  public void method_65(int[] int_0)
  {
    this.method_66(int_0, 0, int_0.Length);
  }

  public void method_66(int[] int_0, int int_1, int int_2)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_1; index < int_1 + int_2; ++index)
        this.class6_0.Write(int_0[index]);
    }
  }

  public void method_67(ulong[] ulong_0)
  {
    this.method_68(ulong_0, 0, ulong_0.Length);
  }

  public void method_68(ulong[] ulong_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(ulong_0[index]);
    }
  }

  public void method_69(long[] long_0)
  {
    this.method_70(long_0, 0, long_0.Length);
  }

  public void method_70(long[] long_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(long_0[index]);
    }
  }

  public void method_71(float[] float_0)
  {
    this.method_72(float_0, 0, float_0.Length);
  }

  public void method_72(float[] float_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(float_0[index]);
    }
  }

  public void method_73(double[] double_0)
  {
    this.method_74(double_0, 0, double_0.Length);
  }

  public void method_74(double[] double_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.class6_0.Write(double_0[index]);
    }
  }

  public void method_75(string[] string_0, int int_0)
  {
    this.method_76(string_0, 0, string_0.Length, int_0);
  }

  public void method_76(string[] string_0, int int_0, int int_1, int int_2)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_42(string_0[index], int_2);
    }
  }

  public void method_77(string[] string_0)
  {
    this.method_76(string_0, 0, string_0.Length, 1252);
  }

  public void method_78(string[] string_0, int int_0, int int_1)
  {
    this.method_76(string_0, int_0, int_1, 1252);
  }

  public void method_79(string[] string_0)
  {
    this.method_80(string_0, 0, string_0.Length);
  }

  public void method_80(string[] string_0, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_43(string_0[index]);
    }
  }

  public void method_81(object[] object_1)
  {
    this.method_82(object_1, 0, object_1.Length);
  }

  public void method_82(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_44(object_1[index]);
    }
  }

  public void method_83(object[] object_1)
  {
    this.method_84(object_1, 0, object_1.Length);
  }

  public void method_84(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_45(object_1[index]);
    }
  }

  public void method_85(object[] object_1)
  {
    this.method_86(object_1, 0, object_1.Length);
  }

  public void method_86(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_46(object_1[index]);
    }
  }

  public void method_87(object[] object_1)
  {
    this.method_88(object_1, 0, object_1.Length);
  }

  public void method_88(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_47(object_1[index]);
    }
  }

  public void method_89(object[] object_1)
  {
    this.method_90(object_1, 0, object_1.Length);
  }

  public void method_90(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_48(object_1[index]);
    }
  }

  public void method_91(object[] object_1)
  {
    this.method_92(object_1, 0, object_1.Length);
  }

  public void method_92(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_49(object_1[index]);
    }
  }

  public void method_93(object[] object_1)
  {
    this.method_94(object_1, 0, object_1.Length);
  }

  public void method_94(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_50(object_1[index]);
    }
  }

  public void method_95(object[] object_1)
  {
    this.method_96(object_1, 0, object_1.Length);
  }

  public void method_96(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_51(object_1[index]);
    }
  }

  public void method_97(object[] object_1)
  {
    this.method_98(object_1, 0, object_1.Length);
  }

  public void method_98(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_52(object_1[index]);
    }
  }

  public void method_99(object[] object_1)
  {
    this.method_100(object_1, 0, object_1.Length);
  }

  public void method_100(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_53(object_1[index]);
    }
  }

  public void method_101(object[] object_1, int int_0)
  {
    this.method_102(object_1, 0, object_1.Length, int_0);
  }

  public void method_102(object[] object_1, int int_0, int int_1, int int_2)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_42(object_1[index].ToString(), int_2);
    }
  }

  public void method_103(object[] object_1)
  {
    this.method_102(object_1, 0, object_1.Length, 1252);
  }

  public void method_104(object[] object_1, int int_0, int int_1)
  {
    this.method_102(object_1, int_0, int_1, 1252);
  }

  public void method_105(object[] object_1)
  {
    this.method_106(object_1, 0, object_1.Length);
  }

  public void method_106(object[] object_1, int int_0, int int_1)
  {
    lock (this.object_0)
    {
      if (this.bool_2)
        throw new Exception("Cannot Write to a locked Packet.");
      for (int index = int_0; index < int_0 + int_1; ++index)
        this.method_43(object_1[index].ToString());
    }
  }
}
