﻿// Decompiled with JetBrains decompiler
// Type: Class34
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Collections.Generic;

internal class Class34
{
  private static object object_0 = new object();

  public static int smethod_0(string string_0)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_44))
      {
        if (str == string_0)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_1(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_61))
      {
        if (Class32.string_29 == "PC")
        {
          if (str == string_0)
            ++num;
        }
        else if (str == string_1)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_2(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_64))
      {
        if (Class32.string_28 == "PC")
        {
          if (str == string_0)
            ++num;
        }
        else if (str == string_1)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_3(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_66))
      {
        if (!(Class32.string_27 == "PC"))
        {
          if (str == string_1)
            ++num;
        }
        else if (str == string_0)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_4(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_70))
      {
        if (Class32.string_27 == "PC")
        {
          if (str == string_0)
            ++num;
        }
        else if (str == string_1)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_5(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_68))
      {
        if (Class32.string_27 == "PC")
        {
          if (str == string_0)
            ++num;
        }
        else if (str == string_1)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_6(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_72))
      {
        if (Class32.string_27 == "PC")
        {
          if (str == string_0)
            ++num;
        }
        else if (str == string_1)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_7(string string_0)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_1))
      {
        if (str == string_0)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static int smethod_8(string string_0, string string_1)
  {
    int num = 0;
    try
    {
      foreach (string str in new List<string>((IEnumerable<string>) Class32.list_2))
      {
        if (!(Class32.string_30 == "PC"))
        {
          if (str == string_1)
            ++num;
        }
        else if (str == string_0)
          ++num;
      }
    }
    catch
    {
    }
    return num;
  }

  public static bool smethod_9(string string_0)
  {
    lock (Class34.object_0)
    {
      try
      {
        foreach (string str in Class32.list_3)
        {
          if (string_0.Contains(str))
            return true;
        }
        return false;
      }
      catch
      {
        return false;
      }
    }
  }

  public static bool smethod_10(string string_0)
  {
    lock (Class34.object_0)
    {
      try
      {
        foreach (string str in Class32.list_5)
        {
          if (string_0.Contains(str))
            return true;
        }
        return false;
      }
      catch
      {
        return false;
      }
    }
  }

  public static bool smethod_11(string string_0)
  {
    lock (Class34.object_0)
    {
      try
      {
        foreach (string str in Class32.list_4)
        {
          if (string_0.Contains(str))
            return true;
        }
        return false;
      }
      catch
      {
        return false;
      }
    }
  }
}
