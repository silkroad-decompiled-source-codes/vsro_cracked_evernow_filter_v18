﻿// Decompiled with JetBrains decompiler
// Type: GInterface18
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("79649BB4-903E-421B-94C9-79848E79F6EE")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface18 : IEnumerable
{
  [DispId(1)]
  int Int32_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  GInterface16 imethod_0([In] GEnum8 genum8_0);

  [DispId(-4)]
  [TypeLibFunc(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.CustomMarshaler)]
  IEnumerator imethod_1();
}
