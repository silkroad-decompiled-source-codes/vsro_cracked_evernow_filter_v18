﻿// Decompiled with JetBrains decompiler
// Type: GInterface10
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("174A0DDA-E9F9-449D-993B-21AB667CA456")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface10
{
  [DispId(1)]
  GEnum5 GEnum5_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  bool Boolean_0 { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(3)]
  bool Boolean_1 { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(4)]
  bool Boolean_2 { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  bool Boolean_3 { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  GInterface11 GInterface11_0 { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(7)]
  GInterface2 GInterface2_0 { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(8)]
  GInterface5 GInterface5_0 { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(9)]
  GInterface18 GInterface18_0 { [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(10)]
  GInterface1 GInterface1_0 { [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }
}
