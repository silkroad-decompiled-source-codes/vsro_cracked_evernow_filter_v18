﻿// Decompiled with JetBrains decompiler
// Type: GInterface1
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("644EFD52-CCF9-486C-97A2-39F352570B30")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface1 : IEnumerable
{
  [DispId(1)]
  int Int32_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_0([MarshalAs(UnmanagedType.Interface), In] GInterface0 ginterface0_0);

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void imethod_1([MarshalAs(UnmanagedType.BStr), In] string string_0);

  [DispId(4)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  GInterface0 imethod_2([MarshalAs(UnmanagedType.BStr), In] string string_0);

  [DispId(-4)]
  [TypeLibFunc(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.CustomMarshaler)]
  IEnumerator imethod_3();
}
