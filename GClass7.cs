﻿// Decompiled with JetBrains decompiler
// Type: GClass7
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Data;
using System.Data.SqlClient;

public static class GClass7
{
  public static SqlConnection sqlConnection_0;

  public static void smethod_0(string string_0)
  {
    using (SqlConnection connection = new SqlConnection("Data Source=" + Class32.string_6 + ";Initial Catalog=EverNow_Filter;Integrated Security=false; User ID = " + Class32.string_7 + "; Password = " + Class32.string_8 + ";"))
    {
      try
      {
        connection.Open();
        new SqlCommand(string_0, connection).ExecuteNonQuery();
      }
      catch (SqlException ex)
      {
        Class18.smethod_0("[" + string_0 + "][" + ex.ToString() + "]", "Red");
      }
    }
  }

  public static string smethod_1(string string_0)
  {
    using (SqlConnection connection = new SqlConnection("Data Source=" + Class32.string_6 + ";Initial Catalog=EverNow_Filter;Integrated Security=false; User ID = " + Class32.string_7 + "; Password = " + Class32.string_8 + ";"))
    {
      string str = (string) null;
      try
      {
        SqlCommand sqlCommand = new SqlCommand(string_0, connection);
        connection.Open();
        sqlCommand.CommandType = CommandType.Text;
        if (!string.IsNullOrEmpty(Convert.ToString(sqlCommand.ExecuteScalar())))
          str = (string) sqlCommand.ExecuteScalar();
      }
      catch (SqlException ex)
      {
        Class18.smethod_0("[" + string_0 + "][" + ex.ToString() + "]", "Red");
      }
      return str;
    }
  }

  public static int smethod_2(string string_0)
  {
    using (SqlConnection connection = new SqlConnection("Data Source=" + Class32.string_6 + ";Initial Catalog=EverNow_Filter;Integrated Security=false; User ID = " + Class32.string_7 + "; Password = " + Class32.string_8 + ";"))
    {
      int num = 0;
      try
      {
        SqlCommand sqlCommand = new SqlCommand(string_0, connection);
        connection.Open();
        sqlCommand.CommandType = CommandType.Text;
        if (!string.IsNullOrEmpty(Convert.ToString(sqlCommand.ExecuteScalar())))
        {
          try
          {
            num = Convert.ToInt32(sqlCommand.ExecuteScalar());
          }
          catch
          {
            Class18.smethod_0("[SQL][Read INT] This Query [" + string_0 + "] has somthing wrong.", "Red");
          }
        }
      }
      catch (SqlException ex)
      {
        Class18.smethod_0("[" + string_0 + "][" + ex.ToString() + "]", "Red");
      }
      return num;
    }
  }

  public static string smethod_3(string string_0)
  {
    using (SqlConnection connection = new SqlConnection("Data Source=" + Class32.string_6 + ";Initial Catalog=EverNow_Filter;Integrated Security=false; User ID = " + Class32.string_7 + "; Password = " + Class32.string_8 + ";"))
    {
      string str = (string) null;
      connection.Open();
      using (SqlDataReader sqlDataReader = new SqlCommand(string_0, connection).ExecuteReader())
      {
        try
        {
          sqlDataReader.Read();
          if (string.IsNullOrEmpty(Convert.ToString(sqlDataReader[0])))
            return (string) null;
          str = Convert.ToString(sqlDataReader["Result"]);
          sqlDataReader.Close();
        }
        catch (SqlException ex)
        {
          Class18.smethod_0("[" + string_0 + "][" + ex.ToString() + "]", "Red");
          str = (string) null;
        }
      }
      return str;
    }
  }

  public static int smethod_4(string string_0)
  {
    using (SqlConnection connection = new SqlConnection("Data Source=" + Class32.string_6 + ";Initial Catalog=EverNow_Filter;Integrated Security=false; User ID = " + Class32.string_7 + "; Password = " + Class32.string_8 + ";"))
    {
      int num = 0;
      connection.Open();
      using (SqlDataReader sqlDataReader = new SqlCommand(string_0, connection).ExecuteReader())
      {
        try
        {
          sqlDataReader.Read();
          if (string.IsNullOrEmpty(Convert.ToString(sqlDataReader[0])))
            return 0;
          num = Convert.ToInt32(sqlDataReader["Result"]);
          sqlDataReader.Close();
        }
        catch (SqlException ex)
        {
          Class18.smethod_0("[" + string_0 + "][" + ex.ToString() + "]", "Red");
          num = 0;
        }
      }
      return num;
    }
  }
}
