﻿// Decompiled with JetBrains decompiler
// Type: GInterface9
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("39EB36E0-2097-40BD-8AF2-63A13B525362")]
[TypeLibType(4160)]
[ComImport]
public interface GInterface9 : IEnumerable
{
  [DispId(1)]
  int Int32_0 { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.IUnknown)]
  object imethod_0([MarshalAs(UnmanagedType.Interface), In] GInterface8 ginterface8_0);

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  GInterface8 imethod_1([In] int int_0);

  [DispId(-4)]
  [TypeLibFunc(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.CustomMarshaler)]
  IEnumerator imethod_2();
}
