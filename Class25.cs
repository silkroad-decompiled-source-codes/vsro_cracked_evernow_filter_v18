﻿// Decompiled with JetBrains decompiler
// Type: Class25
// Assembly: EverNow [Filter], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 10658295-669D-4540-8E2D-84892EBF7371
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter].exe

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

internal class Class25
{
  private static Timer timer_0;
  private static Timer timer_1;

  private static void smethod_0()
  {
    try
    {
      Process currentProcess = Process.GetCurrentProcess();
      currentProcess.MaxWorkingSet = currentProcess.MaxWorkingSet;
    }
    catch
    {
    }
  }

  public static void smethod_1()
  {
    GClass7.smethod_0("TRUNCATE TABLE [_SecretCodes]");
    foreach (KeyValuePair<string, string> keyValuePair in Class32.dictionary_4)
      GClass7.smethod_0("EXEC [_StoreSecretCode] '" + keyValuePair.Key + "','" + keyValuePair.Value + "'");
  }

  public static void smethod_2()
  {
    Class25.timer_0 = new Timer(new TimerCallback(Class25.smethod_5), (object) null, 0, 20000);
  }

  public static void smethod_3()
  {
    Class25.timer_1 = new Timer(new TimerCallback(Class25.smethod_4), (object) null, 0, 1000);
  }

  private static void smethod_4(object object_0)
  {
    if (Class32.bool_60)
    {
      try
      {
        foreach (object obj in Class32.list_76)
        {
          string str = obj.ToString().Replace(" , EveryDay(s)", "".ToString());
          if (!(str == DateTime.Now.ToString("hh:mm:ss tt")))
          {
            if (str == DateTime.Now.ToString("hh:mm:ss tt , dddd"))
            {
              Class32.int_49 = 1;
              Class18.smethod_0("[CTF System] Limit count has been opened.", "Orange");
            }
          }
          else
          {
            Class32.int_49 = 1;
            Class18.smethod_0("[CTF System] Limit count has been opened.", "Orange");
          }
        }
      }
      catch
      {
      }
      try
      {
        foreach (object obj in Class32.list_77)
        {
          string str = obj.ToString().Replace(" , EveryDay(s)", "".ToString());
          if (!(str == DateTime.Now.ToString("hh:mm:ss tt")))
          {
            if (str == DateTime.Now.ToString("hh:mm:ss tt , dddd"))
            {
              Class32.int_49 = 0;
              Class32.list_63.Clear();
              Class32.list_2.Clear();
              Class18.smethod_0("[CTF System] Limit count has been closed.", "Orange");
            }
          }
          else
          {
            Class32.int_49 = 0;
            Class32.list_63.Clear();
            Class32.list_2.Clear();
            Class18.smethod_0("[CTF System] Limit count has been closed.", "Orange");
          }
        }
      }
      catch
      {
      }
    }
    if (!Class32.bool_54)
      return;
    try
    {
      foreach (object obj in Class32.list_75)
      {
        string str = obj.ToString().Replace(" , EveryDay(s)", "".ToString());
        if (str == DateTime.Now.ToString("hh:mm:ss tt"))
        {
          Class32.list_62.Clear();
          Class32.list_61.Clear();
          Class18.smethod_0("[Arena System] Limit count has been closed.", "Orange");
        }
        else if (str == DateTime.Now.ToString("hh:mm:ss tt , dddd"))
        {
          Class32.list_62.Clear();
          Class32.list_61.Clear();
          Class18.smethod_0("[Arena System] Limit count has been closed.", "Orange");
        }
      }
    }
    catch
    {
    }
  }

  public static void smethod_5(object object_0)
  {
    try
    {
      Class25.smethod_0();
      if (File.Exists("Setting(s)/[Filter][Log].txt"))
      {
        foreach (string readAllLine in File.ReadAllLines("Setting(s)/[Filter][Log].txt"))
        {
          char[] chArray = new char[1]{ ',' };
          string[] strArray = readAllLine.Split(chArray);
          string str = strArray[0];
          if (str == "LOADCONFIG")
          {
            try
            {
              Class38.smethod_0();
              Class36.smethod_0();
              Class35.smethod_0();
              Class18.smethod_0("Setting(s) has been loaded successfully.", "Blue");
            }
            catch
            {
            }
          }
          else if (str == "LOADSQLTABLE")
          {
            try
            {
              if (Class32.int_48 == 0)
              {
                Class21.smethod_0();
                Class18.smethod_0("SqlTable(s) has been loaded successfully.", "Blue");
              }
            }
            catch
            {
            }
          }
          else if (str == "LOADOPCODES")
          {
            try
            {
              Class21.smethod_1();
              Class18.smethod_0("OpCode(s) has been loaded successfully.", "Blue");
            }
            catch
            {
            }
          }
          else if (str == "LOADPLAYERS")
          {
            try
            {
              Class18.smethod_1("ONLINE," + (object) Class32.int_0 ?? "");
              Class18.smethod_1("CLIENTLESS," + (object) Class32.int_38 ?? "");
              Class18.smethod_1("THIRDPART," + (object) Class32.int_37 ?? "");
            }
            catch
            {
            }
          }
          else if (str == "LOADTELEPORTTIME")
          {
            try
            {
              Class36.smethod_0();
            }
            catch
            {
            }
          }
          else if (str == "DISCONNECT")
          {
            try
            {
              Class27.smethod_0(strArray[1]);
              Class18.smethod_0("CharName [" + strArray[1] + "] has been disconnected from server.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SENDNOTICE")
          {
            try
            {
              Class30.smethod_0(strArray[1]);
              Class18.smethod_0("[Notice] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SENDGLOBAL")
          {
            try
            {
              Class29.smethod_0(strArray[1]);
              Class18.smethod_0("[Global] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SENDPRIVATEMESSAGE")
          {
            try
            {
              Class28.smethod_0(strArray[1]);
              Class18.smethod_0("[Private Message] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SENDACADEMYMESSAGE")
          {
            try
            {
              Class26.smethod_0(strArray[1]);
              Class18.smethod_0("[Academy Message] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SPECIFICSENDNOTICE")
          {
            try
            {
              Class30.smethod_1(strArray[1], strArray[2]);
              Class18.smethod_0("[Notice] to [" + strArray[1] + "] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SPECIFICSENDGLOBAL")
          {
            try
            {
              Class29.smethod_1(strArray[1], strArray[2]);
              Class18.smethod_0("[Global] to [" + strArray[1] + "] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SPECIFICSENDPRIVATEMESSAGE")
          {
            try
            {
              Class28.smethod_1(strArray[1], strArray[2]);
              Class18.smethod_0("[Private Message] to [" + strArray[1] + "] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SPECIFICSENDACADEMYMESSAGE")
          {
            try
            {
              Class26.smethod_1(strArray[1], strArray[2]);
              Class18.smethod_0("[Academy Message] to [" + strArray[1] + "] Message has been sent.", "Green");
            }
            catch
            {
            }
          }
          else if (str == "SECRETCODE")
          {
            try
            {
              string key = strArray[1];
              if (!Class32.dictionary_4.ContainsKey(key))
                Class18.smethod_1("SECRETCODE,NULL");
              else
                Class18.smethod_1("SECRETCODE," + Class32.dictionary_4[key] ?? "");
            }
            catch
            {
            }
          }
        }
        try
        {
          File.WriteAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Setting(s)\\[Filter][Log].txt", string.Empty);
        }
        catch
        {
        }
      }
      else
        Class18.smethod_0("Error loading [Filter][Log].txt", "Red");
    }
    catch
    {
    }
  }
}
